/*-----------------------------------------------------------------
    Copyright by Freefly Systems Inc 2015
		
    Contains proprietary algorithms and source code
    Copyright by SM Webb and JG Ellison 2010-2012
		
    Filename: "QBX_Protocol_AppInterface.c"
-----------------------------------------------------------------*/

//****************************************************************************
// Headers
//****************************************************************************
#include <stdlib.h>		// for Standard Data Types
#include <stdint.h>		// for Standard Data Types
#include "QBX_Protocol.h"
#include "QBX_Protocol_AppInterface.h"
#include "QBX_Parsing_Functions.h"
#include "rc4.h"

#include <stdbool.h>

//****************************************************************************
// Data variables - needs cleanup
//****************************************************************************

IMU_type imu;
accel_type accel;
config_type config;
commonconfig_type commonconfig;
gyrotest_type gyrotest;
cal_type cal;
pc_date_type pc_date;
Compass_type compass;


remote_type remote = { .q = { 1, 0, 0, 0 },
    .q_delta = { 1, 0, 0, 0 },
    .new_reading = 0,
    .timer = 500,
    .interval = 0,
    .last_time = 0,
    .freq = 0.0f };

int32_t man_motor[8];
float motor[3] = { 0.0f, 0.0f, 0.0f };
int16_t motor_commands[3];
bool motor_kills[3];
float motor_to_percent;
float joint[3] = { 0.0f, 0.0f, 0.0f };
int32_t joint_raw[3] = { 0, 0, 0 };
float jointrads[3];                            // joint angles in radians, re-ordered roll,pitch,yaw
int32_t uber_clamp = 25000;
float motor_to_amps = 10.0f / 25000.0f;
float motor_ambient[3] = { 25.0f, 25.0f, 25.0f };
float joint_rate[3];
float estimated_temp[3] = { 25.0f, 25.0f, 25.0f };
uint32_t cell_count = 0;
float battery_voltage = 14.8f;
float battery_current = 0.0f;
float tilt_flip = 0.0f;
float charge_used = 0.0f;
uint32_t drive_led_status[3] = { 0, 0, 0 };
uint32_t use_puck_ptat = false;
uint8_t bat_raw[3] = { 0, 0, 0 };
uint32_t bat_mon = 0;
uint32_t drive_response_times[3] = { 0, 0, 0 };

// These are nominal values required by the VB calibration code
int32_t lsb_per_deg_per_sec; // LSB per gyro deg/sec
int32_t lsb_per_g; // LSB per 1g
int32_t lsb_per_t; // LSB per earth field unit

float * att;
float * e;

float c_live_offset[3];// = { 0.0f, 0.0f, 0.0f };
float * a_pred;
float * rate_offset;
float ptat;
float * rate;
float * rate_filt;
float h_correction;
float * rate_raw;
float * rate_cmd_error;
float * rate_control;
float * rate_cmd;
float climb_control;
float * vr_cmd_error;
float * att_cmd_error;
float * ar_cmd_error;
float * att_cmd;
float * ar_cmd;
float * xr_cmd_error;
float * rate_cmd_error;
float * vr_cmd;
float * cal_mode;
float * debugvar;
uint32_t  gyro_orientation;
uint32_t  accel_orientation;
uint32_t compass_orientation;
uint8_t config_page;
uint32_t autotune_enable;// = 0;
uint32_t autotune_progress;// = 0;

//volatile uint16_t machine_fail = 0x0000;
//uint32_t status = 0;
float timeNC = 0.0f;
float occupancy = 0.0f;
int32_t alarm_led = 0;

uint32_t time_idle_entry;
uint32_t time_idle_entry_last;
uint32_t time_idle_exit;
uint32_t time_in_idle;
uint32_t period_idle;
float idle_percent;

Cal_control_type cal_control;
puck_type puck;
uint32_t RWD_lic(uint32_t);

uint32_t Check_licence_date(void);
uint32_t Obscurify(uint32_t);
uint32_t Unobscurify(uint32_t);

uint32_t RWD_puck(uint32_t);

Gimbal_control_type gimbal_control;

lic_type lic;

//****************************************************************************
// Private Global Vars
//****************************************************************************
uint8_t protocol_key[20];

// otherwise undefined symbols
float ar[3] = { 0.0f, 0.0f, 0.0f };       // reference frame accelerations
float vr[3] = { 0.0f, 0.0f, 0.0f };       // reference frame velocities
float xr[3] = { 0.0f, 0.0f, 0.0f };       // reference frame positions
float vr_delay[3];                  // Delayed velocity
float xr_delay[3];                  // Delayed position
float ar_filt[3] = { 0.0f, 0.0f, 0.0f };  // LSF filtered reference plane ar and vr
GPS_type gps;
uint32_t gui_shutter = false;
radio_type radio;
husq_type husq;
Pressure_type pressure;
target_type target;


// BLE
QBX_BLE_Datalog_t QBX_BLE_Datalog;

//****************************************************************************
// Private Function Prototypes
//****************************************************************************
void AddCameraStatusWordToPacket(void);


//****************************************************************************
// Functions
//****************************************************************************

//----------------------------------------------------------------------------
// Service the QBX Comms Ports. Check for RX Messages, TX Periodic Messages
void QBX_Service_Comms(void)
{
//	static uint32_t localTick = 0;
//	
//	HSL_UART_Refresh_DMA(&USER_UART_HANDLE);
//	
//	QBX_SetSendProtocol(1);
//	
//	// UART Charting/Streaming
//	if(localTick < 16)    // Max 16 parameters
//	{
//		// UART 1
//		if(QBX_CommsPorts[QBX_COMMS_PORT_UART1].TeleStream[localTick * 2 + 1] != 0)
//		{
//				if(++QBX_CommsPorts[QBX_COMMS_PORT_UART1].StreamTicker[localTick] == QBX_CommsPorts[QBX_COMMS_PORT_UART1].TeleStream[localTick * 2 + 1])
//				{
//						QBX_SendPacketStd(QBX_CommsPorts[QBX_COMMS_PORT_UART1].TeleStream[localTick * 2], QBX_COMMS_PORT_UART1, QBX_MSG_TYPE_CURVAL_PERIODIC);
//						QBX_CommsPorts[QBX_COMMS_PORT_UART1].StreamTicker[localTick] = 0;
//				}
//		}
//		
//		// UART 2
//		if(QBX_CommsPorts[QBX_COMMS_PORT_UART2].TeleStream[localTick * 2 + 1] != 0)
//		{
//				if(++QBX_CommsPorts[QBX_COMMS_PORT_UART2].StreamTicker[localTick] == QBX_CommsPorts[QBX_COMMS_PORT_UART2].TeleStream[localTick * 2 + 1])
//				{
//						QBX_SendPacketStd(QBX_CommsPorts[QBX_COMMS_PORT_UART2].TeleStream[localTick * 2], QBX_COMMS_PORT_UART2, QBX_MSG_TYPE_CURVAL_PERIODIC);
//						QBX_CommsPorts[QBX_COMMS_PORT_UART2].StreamTicker[localTick] = 0;
//				}
//		}
//		
//		// BLE Charting/Streaming TEST - OVERRIDE RATE!!!
//		if(QBX_CommsPorts[QBX_COMMS_PORT_BLE_UART].TeleStream[localTick * 2] == 1){
//			if(++QBX_CommsPorts[QBX_COMMS_PORT_BLE_UART].StreamTicker[localTick] == 10)
//			{
//				QBX_SendPacketStd(1, QBX_COMMS_PORT_BLE_UART, QBX_MSG_TYPE_CURVAL_PERIODIC);
//				QBX_CommsPorts[QBX_COMMS_PORT_BLE_UART].StreamTicker[localTick] = 0;
//			}
//		}
//			
//		if(QBX_CommsPorts[QBX_COMMS_PORT_BLE_UART].TeleStream[localTick * 2] == 9){
//			if(++QBX_CommsPorts[QBX_COMMS_PORT_BLE_UART].StreamTicker[localTick] == 4)
//			{
//				QBX_SendPacketStd(9, QBX_COMMS_PORT_BLE_UART, QBX_MSG_TYPE_CURVAL_PERIODIC);
//				QBX_CommsPorts[QBX_COMMS_PORT_BLE_UART].StreamTicker[localTick] = 0;
//			}
//		}
//			
//		if(QBX_CommsPorts[QBX_COMMS_PORT_BLE_UART].TeleStream[localTick * 2] == 22){
//			if(++QBX_CommsPorts[QBX_COMMS_PORT_BLE_UART].StreamTicker[localTick] == 4)
//			{
//				QBX_SendPacketStd(22, QBX_COMMS_PORT_BLE_UART, QBX_MSG_TYPE_CURVAL_PERIODIC);
//				QBX_CommsPorts[QBX_COMMS_PORT_BLE_UART].StreamTicker[localTick] = 0;
//			}
//		}
//	}
//	
//	// BLE Notify Type Charting/Streaming
//	
//	// BLE Datalog (Split Bandwidth of single Characteristic Connection Interval (Rate))
//	if (localTick % COMMS_SERVICE_RATE / 100 == 0){		// Update Attributes Alternating at 100 HZ
////		for (int i=0; i<4; i++){
////			if (QBX_BLE_Datalog.Attrib[QBX_BLE_Datalog.SendIdx] != 0){
////				QBX_SendPacketDataLog(QBX_BLE_Datalog.Attrib[i], QBX_COMMS_PORT_BLE_DLOG);
////				break;
////			}
////			if (++QBX_BLE_Datalog.SendIdx >= 4) QBX_BLE_Datalog.SendIdx = 0;
////		}
//	}
//	
//	if(++localTick == (COMMS_SERVICE_RATE / 20)) localTick = 0; // 20Hz max rate
//	
//	// Check for RX Messages
//	uint8_t rxbyte, count;
//	
//	// UART 1
//	count = 0;
//	while(count++ < 20){		// Service 20 characters at a time to avoid blocking the main loop if the buffer is stuffed
//		if(RemoveUART1Rx(&rxbyte) == 0) break;    // If no characters in buffer then skip
//		QBX_StreamRxCharSM(QBX_COMMS_PORT_UART1, rxbyte);
//	}
//	
//	// UART 2
//	count = 0;
//	while(count++ < 20){		// Service 20 characters at a time to avoid blocking the main loop if the buffer is stuffed
//		if(RemoveUART2Rx(&rxbyte) == 0) break;    // If no characters in buffer then skip
//		QBX_StreamRxCharSM(QBX_COMMS_PORT_UART2, rxbyte);
//	}
//	
//	// BLE UART
//	count = 0;
//	while(count++ < 20){		// Service 20 characters at a time to avoid blocking the main loop if the buffer is stuffed
//		if(Ble_UART_BufRemove(&Ble_UART_QX.RX, &rxbyte) == 0) break;    // If no characters in buffer then skip
//		QBX_StreamRxCharSM(QBX_COMMS_PORT_BLE_UART, rxbyte);
//	}
	
	// BLE DIRECT
	
	// BLE DATALOG - Sends Message if Needed
		
		
}

//----------------------------------------------------------------------------
// Parse Packet Server - Application Callback
// This Call Back Function is the primary point of exchange of comms/application data.
// This primairly handles the "Server/Slave" Role, Accepting Requests and Sending Responses
uint8_t *QBX_ParsePacket_Srv_CB(QBX_Msg_t *Msg_p){

}

//----------------------------------------------------------------------------
// Parse Packet Client - Application Callback
// This primairly handles the "Client/Master" Role, Sending Requests and Accepting Responses
uint8_t *QBX_ParsePacket_Cli_CB(QBX_Msg_t *Msg_p){
    int j;
    
    uint32_t i;
    int32_t reserved = 0;
    int32_t temp = 0;
    uint32_t coded;
    static uint32_t last_radio_time = 0;
    //static uint32_t last_target_time = 0;
    uint32_t t;
    uint8_t attrib71_digital = 0;
    int32_t minimum_kp;
    static int32_t last_kp[3] = { 0, 0, 0 };
    
    // Temp variables
    uint8_t invertGyro[3] = { 0, 0, 1 }; // 1 = sense inverted
    uint8_t invertAccel[3] = { 1, 1, 0 }; // 1 = axis inverted
    uint8_t invertCompass[3] = { 1, 0, 1 }; // 1 = axis inverted
    
    // Handle specific packet type
    switch(Msg_p->Header.Attrib)
    {
            
            // Status packet as follows:
            // NB OnStation is only 2 bit, 6 bits spare for future use
            // | BatV    | BatV    | GPSSats | Temp | Occ  | STATUS  | STATUS  | Time      |
            // | Time    | Pres    | Pres    | Rate | Rate | CAMSTAT | CAMSTAT | OnStation |
            // | SysTick | SysTick |
        case 1:
            PARSE_FL_AS_SS(&battery_voltage, 1, 0, 0, 100.0f);
            PARSE_UC_AS_UC(&gps.sats, 1, 0, 0);
            PARSE_FL_AS_UC(&pressure.T, 1, 0, 0, 3.0f);
            PARSE_FL_AS_UC(&occupancy, 1, 0, 0, 1.0f);
            //				AddStatusWordToPacket();
            PARSE_FL_AS_SS(&timeNC, 1, 0, 0, 1.0f);
            PARSE_FL_AS_SS(&pressure.P, 1, 0, 0, 0.1f);
            PARSE_FL_AS_SS(&imu.rate, 1, 0, 0, 1.0f);
            //				AddCameraStatusWordToPacket();
            temp = 0;
            PARSE_SL_AS_UC(&temp, 1, 0, 0);
            PARSE_SL_AS_SS(&imu.latency, 1, 0, 0);
            PARSE_FL_AS_SS(&battery_current, 1, 0, 0, 100.0f);
            PARSE_FL_AS_SS(&charge_used, 1, 0, 0, 1.0f);
            break;
            
        case 2:        // Roll, pitch and yaw
            PARSE_FL_AS_SS(att, 3, 0, 0, 100.0f);
            break;
            
        case 3:        // Height and ROC
            PARSE_FL_AS_SS(&xr[UP], 1, 0, 0, 10.0f);
            PARSE_FL_AS_SS(&vr[UP], 1, 0, 0, 100.0f);
            break;
            
        case 4:        // GPS
            PARSE_SL_AS_SL(&gps.longitude, 1, 0, 0);
            PARSE_SL_AS_SL(&gps.latitude, 1, 0, 0);
            PARSE_FL_AS_SL(&gps.xr[UP], 1, 0, 0, 1000.0f);    //SS -> SL, 10.0 -> 1000.0
            PARSE_FL_AS_SS(&gps.gspeed, 1, 0, 0, 100.0f);
            PARSE_FL_AS_SS(&gps.heading, 1, 0, 0, 10.0f);
            PARSE_FL_AS_SS(&gps.hacc, 1, 0, 0, 100.0f);        //10.0 -> 100.0
            PARSE_FL_AS_SS(&gps.vacc, 1, 0, 0, 100.0f);        //10.0 -> 100.0
            PARSE_FL_AS_SS(&gps.sacc, 1, 0, 0, 100.0f);
            //PARSE_SL_AS_SS(&reserved, 1, 0, 0);
            //PARSE_SL_AS_SS(&reserved, 1, 0, 0);
            break;
            
        case 6:        // Quaternion
            PARSE_FL_AS_SS(e, 4, 0, 0, 1000.0f);
            PARSE_FL_AS_SS(a_pred,3, 0, 0, 1000.0f);
            break;
            
        case 7:        // Joint angles
            PARSE_FL_AS_SS(jointrads, 3, 0, 0, 100.0f * RADTODEG);
            break;
            
        case 8:        // Radio
            PARSE_SL_AS_SS(radio.ch, 10, 0, 0);
            break;
            
        case 9:        // Motors and temperatures
            PARSE_FL_AS_SS(motor, 3, 0, 0, 100.0f * motor_to_amps);
            PARSE_SL_AS_SS(&reserved, 1, 0, 0);
            PARSE_FL_AS_SS(motor, 3, 0, 0, motor_to_percent);
            PARSE_SL_AS_SS(&reserved, 1, 0, 0);
            PARSE_FL_AS_SS(estimated_temp, 3, 0, 0, 10.0f);
            break;
            
        case 10:    // Gyros
            PARSE_FL_AS_SL(rate_offset, 3, 0, 0, 10000.0f);
            PARSE_FL_AS_SL(&ptat, 1, 0, 0, 10000.0f);
            PARSE_FL_AS_SL(rate, 3, 0, 0, RADTODEG * 100.0f);
            PARSE_FL_AS_SL(rate_filt, 3, 0, 0, RADTODEG * 100.0f);
            break;
            
        case 11:    // Accelerometer
            PARSE_FL_AS_SS(accel.vec, 3, 0, 0, 1000.0f);
            PARSE_FL_AS_SS(accel.vec_offset, 3, 0, 0, 1000.0f);
            PARSE_FL_AS_SS(&accel.magnitude, 1, 0, 0, 1000.0f);
            PARSE_FL_AS_SS(&accel.freq, 1, 0, 0, 100.0f);
            break;
            
        case 12:    // Magnetometer
            PARSE_FL_AS_SS(compass.vec, 3, 0, 0, 1000.0f);
            PARSE_FL_AS_SS(&compass.magnitude, 1, 0, 0, 100.0f);
            PARSE_FL_AS_SS(&compass.freq, 1, 0, 0, 100.0f);
            PARSE_FL_AS_SS(c_live_offset, 3, 0, 0, 1000.0f);
            PARSE_FL_AS_SS(&h_correction, 3, 0, 0, 10.0f * RADTODEG);
            break;
            
        case 13:    // Raw Gyros
            PARSE_FL_AS_SL(rate_raw, 3, 0, 0, 1000.0f);
            break;
            
        case 14:    // Raw Accelerometer
            PARSE_SL_AS_SL(accel.raw, 3, 0, 0);        // 3x oversampling in gyro.c means we overflow a int16_t
            break;
            
        case 15:     // Raw magnetometer
            PARSE_SS_AS_SS(compass.raw, 3, 0, 0);
            break;
            
        case 18:    // Bonehead attitude
            PARSE_FL_AS_SS(att, 3, 0, 0, 1.0f);
            break;
            
        case 19:    // GPS diagnostic #2
            PARSE_SS_AS_SS((int16_t *)&gps.agc, 1, 0, 0);
            PARSE_UC_AS_UC(&gps.jam_indicator, 1, 0, 0);
            PARSE_UC_AS_UC(&gps.fix, 1, 0, 0);
            PARSE_UC_AS_UC(&gps.first_fix, 1, 0, 0);
            PARSE_UC_AS_UC(&gps.sbas_prn, 1, 0, 0);
            PARSE_UC_AS_UC(&gps.sbas_mode, 1, 0, 0);
            PARSE_SC_AS_SC(&gps.sbas_system, 1, 0, 0);
            PARSE_SS_AS_SS(&gps.year, 1, 0, 0);
            PARSE_UC_AS_UC(&gps.month, 1, 0, 0);
            PARSE_UC_AS_UC(&gps.day, 1, 0, 0);
            PARSE_UC_AS_UC(&gps.hour, 1, 0, 0);
            PARSE_UC_AS_UC(&gps.min, 1, 0, 0);
            PARSE_UC_AS_UC(&gps.sec, 1, 0, 0);
            PARSE_UC_AS_UC(&gps.sats, 1, 0, 0);
            PARSE_FL_AS_SS(&gps.freq, 1, 0, 0, 100.0f);
            PARSE_SS_AS_SS(&gps.crc, 1, 0, 0);
            break;
            
        case 20:    // Spektrum diagnostic
            PARSE_UC_AS_UC(&radio.data_source, 1, 0, 0);
            PARSE_SL_AS_SS(&radio.a_bad_frames, 1, 0, 0);
            PARSE_FL_AS_UC(&radio.a_freq, 1, 0, 0, 1.0f);
            PARSE_SL_AS_UC(&radio.a_dropouts, 1, 0, 0);
            PARSE_UC_AS_UC(&radio.a_los, 1, 0, 0);
            PARSE_SL_AS_SS(&radio.b_bad_frames, 1, 0, 0);
            PARSE_FL_AS_UC(&radio.b_freq, 1, 0, 0, 1.0f);
            PARSE_SL_AS_UC(&radio.b_dropouts, 1, 0, 0);
            PARSE_UC_AS_UC(&radio.b_los, 1, 0, 0);
            break;
            
        case 21:    // Rate diagnostic
            PARSE_FL_AS_SS(rate_filt, 3, 0, 0, RADTODEG * 10.0f);
            PARSE_FL_AS_SS(rate_cmd_error, 3, 0, 0, RADTODEG * 10.0f);
            PARSE_FL_AS_SS(rate_control, 3, 0, 0, 0.1f);
            break;
            
        case 22:    // Attitude diagnostic
            PARSE_FL_AS_SS(att, 3, 0, 0, 100.0f);
            PARSE_FL_AS_SS(att_cmd_error, 3, 0, 0, 10.0f);
            PARSE_FL_AS_SS(rate_cmd, 3, 0, 0, RADTODEG * 10.0f);
            break;
            
        case 23:    // Acceleration diagnostic
            PARSE_FL_AS_SS(ar, 3, 0, 0, 100.0f);
            PARSE_FL_AS_SS(ar_filt, 3, 0, 0, 100.0f);
            PARSE_FL_AS_SS(gps.ar, 3, 0, 0, 980.665f);
            PARSE_FL_AS_SS(ar_cmd_error, 3, 0, 0, 100.0f);
            PARSE_FL_AS_SS(att_cmd, 2, 0, 0, 100.0f);
            PARSE_FL_AS_SS(&climb_control, 1, 0, 0, 0.1f);
            break;
            
        case 24:    // Velocity diagnostic
            PARSE_FL_AS_SS(vr_delay, 3, 0, 0, 100.0f);
            PARSE_FL_AS_SS(gps.vr, 3, 0, 0, 100.0f);
            PARSE_FL_AS_SS(vr_cmd_error, 3, 0, 0, 100.0f);
            PARSE_FL_AS_SS(ar_cmd, 3, 0, 0, 100.0f);
            break;
            
        case 25:    // Position diagnostic #1
            PARSE_FL_AS_SL(xr_delay, 3, 0, 0, 1000.0f);                // SS -> SL, 10.0 -> 1000.0
            PARSE_FL_AS_SL(gps.xr, 2, 0, 0, 1000.0f);            // SS -> SL, 10.0 -> 1000.0
            PARSE_FL_AS_SL(&pressure.h_corr, 1, 0, 0, 1000.0f);    // SS -> SL, 10.0 -> 1000.0
            PARSE_FL_AS_SS(xr_cmd_error, 3, 0, 0, 100.0f);        // 10.0 -> 100.0
            PARSE_FL_AS_SS(vr_cmd, 3, 0, 0, 100.0f);
            break;
            
        case 26:    // Position diagnostic #2
            PARSE_FL_AS_SL(xr, 3, 0, 0, 1000.0f);
            PARSE_FL_AS_SL(gps.xr, 2, 0, 0, 1000.0f);
            PARSE_FL_AS_SL(&pressure.h_corr, 1, 0, 0, 1000.0f);
            PARSE_FL_AS_SS(&gps.hacc, 1, 0, 0, 100.0f);
            break;
            
        case 27:    // Target position
            PARSE_FL_AS_SL(target.range, 3, 0, 0, 1000.0f);
            PARSE_FL_AS_SS(&target.freq, 1, 0, 0, 100.0f);
            break;
            
        case 28:    // Accel, magnetometer and gyro axis inversion
            for(i = ROLL; i <= YAW; i++)
            {
                if(gyro_orientation & (1 << i))
                {
                    invertGyro[i] = 1;
                } else {
                    invertGyro[i] = 0;
                }
                if(accel_orientation & (1 << i))
                {
                    invertAccel[i] = 1;
                } else {
                    invertAccel[i] = 0;
                }
                if(compass_orientation & (1 << i))
                {
                    invertCompass[i] = 1;
                } else {
                    invertCompass[i] = 0;
                }
            }
            PARSE_UC_AS_UC(invertAccel, 3, 0, 0);
            PARSE_UC_AS_UC(invertCompass, 3, 0, 0);
            PARSE_UC_AS_UC(invertGyro, 3, 0, 0);
            break;
            
        case 29:    // Accel, magnetometer and gyro internal scaling
            PARSE_SL_AS_SL(&lsb_per_g, 1, 0, 0);
            PARSE_SL_AS_SL(&lsb_per_t, 1, 0, 0);
            PARSE_SL_AS_SL(&lsb_per_deg_per_sec, 1, 0, 0);
            temp = ACCEL_PRESCALE;
            PARSE_SL_AS_SL(&temp, 1, 0, 0);
            temp = COMPASS_PRESCALE;
            PARSE_SL_AS_SL(&temp, 1, 0, 0);
            break;
            
        case 31: // Machine failure report
            //				PARSE_SS_AS_SS(&machine_fail, 1, 0, 0);
            break;
            
        case 48:    // gps home position
            PARSE_SL_AS_SL(&gps.longitude_home, 1, 0, 0);
            PARSE_SL_AS_SL(&gps.latitude_home, 1, 0, 0);
            break;
            
        case 49:    // gps home altitude
            //#warning GPS home altitude not done
            break;
            
        case 50:    // gps home position and altitude
            break;
            
        case 64:    // Calibration control
            //				if(cal_mode == 0xAA)    //Engineering cal mode
            //				{
            //						PARSE_SL_AS_UC(&cal_control.calibration_select, 1, CALIBRATION_LAST_ENG, 0);
            //				}
            //				else                    //Customer cal mode
            //				{
            PARSE_SL_AS_UC(&cal_control.calibration_select, 1, CALIBRATION_LAST_CUST, 0);
            //				}
            PARSE_SL_AS_UC(&cal_control.calibration_start, 1, 1, 0);
            //				if((cal_mode != 0xAA) && (cal_mode != 0xAB)) cal_control.calibration_start = 0;
            //				temp = cal_control.calibration_progress;
            PARSE_SL_AS_UC(&temp, 1, 0, 0);
            //				temp = cal_control.calibration_total_progress;
            PARSE_SL_AS_UC(&temp, 1, 0, 0);
            PARSE_FL_AS_UC(&config.cal_temp_step, 1, 5.0f, 0.1f, 10.0f);
            PARSE_SL_AS_UC(&config.cal_temp_points, 1, 20, 3);
            break;
            
        case 69:	// RC4 Encryption Key Exchange
        case 690:
            if (Msg_p->Header.Type == QBX_MSG_TYPE_CURVAL_EVENT){		// Key Request Event Response (Send to Client)
                Msg_p->Header.Target_Addr = Msg_p->Header.Source_Addr;
                Msg_p->Header.Source_Addr = QBXSourceAddress;	// ID = 0x00 for GCU
                *Msg_p->MsgBuf_p++ = 'g';					// give
                *Msg_p->MsgBuf_p++ = 'i';					// it
                *Msg_p->MsgBuf_p++ = 'm';					// me
                *Msg_p->MsgBuf_p++ = (uint8_t)~'g';		// complement
                *Msg_p->MsgBuf_p++ = (uint8_t)~'i';
                *Msg_p->MsgBuf_p++ = (uint8_t)~'m';
                
            } else if (Msg_p->Header.Type == QBX_MSG_TYPE_WRITE_ABS){		// Recieve Encryption Key (Recv from Client)
                for(j = 0; j < Msg_p->MsgDataLength; j++) protocol_key[j] = *Msg_p->MsgBuf_p++;
                EnDecode(protocol_key, Msg_p->MsgDataLength, (uint8_t *)"GwM1OO-01%1");		// Decode the public Message Key
                Prime_PRBS((uint8_t *)"Fgg.@B.3O7.CLAD$2");								// private password for codebook generation
                DoKSA((uint8_t *)&protocol_key, Msg_p->MsgDataLength);		// public password for codebook generation
                Copy_codebook();
                QBX_CommsPorts->QBX_Enc_Key_Stat = QBX_ENC_KEY_STAT_VALID;
                //UART2Send((uint8_t *)"Key received!\r");
            }
            break;
            
        case 70:    // Control quaternion
            if(rw == READ)
            {
                PARSE_FL_AS_SS(remote.q, 4, 1.0f, 0.0f, 10000.0f);
                
                // Added padding to allow FIZ to be tacked on
                PARSE_SL_AS_SS(&reserved, 1, 65535, 0);
                PARSE_SL_AS_SS(&reserved, 1, 65535, 0);
                PARSE_SL_AS_SS(&reserved, 1, 65535, 0);
                PARSE_SL_AS_SS(&reserved, 1, 65535, 0);
                PARSE_SL_AS_SS(&reserved, 1, 65535, 0);
                PARSE_SL_AS_UC(&reserved, 1, 255, 0);   // Digital
                // End of added padding
            }
            else
            {
                if(Msg_p->EncKeyStat == QBX_ENC_KEY_STAT_VALID)
                {
                    //								t = get_time_coarse();
                    PARSE_FL_AS_SS(remote.q, 4, 1.0f, -1.0f, 10000.0f);
                    remote.new_reading = TRUE;
                    remote.timer = 500;
                    remote.interval = t - remote.last_time;
                    remote.last_time = t;
                    debugvar[5]++;
                }
                else
                {
                    remote.q_delta[0] = 1.0f;
                    remote.q_delta[1] = 0.0f;
                    remote.q_delta[2] = 0.0f;
                    remote.q_delta[3] = 0.0f;
                }
            }
            break;
            
        case 71:    // FTX controller attribute
            if(rw == READ) // A read from this attribute will just report dummy values, but needs to be here so that message lengths are computed correctly at initialisation
            {
                reserved = 0;
                PARSE_SL_AS_SS(&reserved, 1, 65535, 0);
                PARSE_SL_AS_SS(&reserved, 1, 65535, 0);
                PARSE_SL_AS_SS(&reserved, 1, 65535, 0);
                PARSE_SL_AS_SS(&reserved, 1, 65535, 0);
                PARSE_SL_AS_SS(&reserved, 1, 65535, 0);
                PARSE_SL_AS_SS(&reserved, 1, 65535, 0);
                PARSE_SL_AS_SS(&reserved, 1, 65535, 0);
                PARSE_SL_AS_SS(&reserved, 1, 65535, 0);
                PARSE_SL_AS_UC(&reserved, 1, 255, 0);   // Digital
            }
            else
            {
                if((Msg_p->EncKeyStat == QBX_ENC_KEY_STAT_VALID) && (commonconfig.radioType == FTX))
                {
                    //								t = get_time_coarse();
                    // 1000 - 2000 legacy control.  10000 - 20000 high res control
                    PARSE_SL_AS_SS(&temp, 1, 20000, 1000);
                    if((500 <= temp) && (temp <= 2500)) radio.ch[ROLL_CMD] = temp;
                    if((5000 <= temp) && (temp <= 25000)) radio.ch[ROLL_CMD] = temp * 0.1f;
                    PARSE_SL_AS_SS(&temp, 1, 20000, 1000);
                    if((500 <= temp) && (temp <= 2500)) radio.ch[PITCH_CMD] = temp;
                    if((5000 <= temp) && (temp <= 25000)) radio.ch[PITCH_CMD] = temp * 0.1f;
                    PARSE_SL_AS_SS(&temp, 1, 20000, 1000);
                    if((500 <= temp) && (temp <= 2500)) radio.ch[YAW_CMD] = temp;
                    if((5000 <= temp) && (temp <= 25000)) radio.ch[YAW_CMD] = temp * 0.1f;
                    PARSE_SL_AS_SS(&reserved, 1, 2000, 1000);   // Focus
                    PARSE_SL_AS_SS(&reserved, 1, 2000, 1000);   // Iris
                    PARSE_SL_AS_SS(&reserved, 1, 2000, 1000);   // Zoom
                    PARSE_SL_AS_SS(&reserved, 1, 65535, 0);   // Aux 1
                    PARSE_SL_AS_SS(&reserved, 1, 65535, 0);   // Aux 2
                    PARSE_SL_AS_UC(&attrib71_digital, 1, 255, 0);   // Digital
                    radio.new_data = 1;
                    radio.freq = 1000.0f / (float)(t - last_radio_time);
                    //								LPF(LPF_RADIO_FREQ, &radio.freq, 0.05f);
                    radio.a_freq = radio.freq;  //For GUI
                    last_radio_time = t;
                    radio.los = 0;
                    radio.a_los = radio.los;    //For GUI
                    husq.los = 0;
                    husq.last_time = t;
                    radio.ch[TILT_CLAMP_CMD] = 2000;    // Force max rate
                    radio.ch[PAN_CLAMP_CMD] = 2000;
                    
                    switch(attrib71_digital & 3)        // Convert digital to equivalent radio mode switch
                    {
                        case 0:
                            radio.ch[MODE_CMD] = 1000;
                            break;
                            
                        case 1:
                            radio.ch[MODE_CMD] = 1500;
                            break;
                            
                        case 2:
                            radio.ch[MODE_CMD] = 2000;
                            break;
                    }
                    if(attrib71_digital & 4)
                    {
                        radio.ch[SHUTTER_CMD] = 2000;
                    }
                    else
                    {
                        radio.ch[SHUTTER_CMD] = 1000;
                    }
                }
            }
            break;
            
        case 73:    // Look at
            PARSE_SL_AS_SL(&temp, 1, 0, 0);
            PARSE_FL_AS_SL(&target.altitude, 1, 100000000, -100000000, 1000.0f);    // * 1e3
            PARSE_SL_AS_SL(&target.latitude, 1, 900000000, -900000000);             // * 1e7
            PARSE_SL_AS_SL(&target.longitude, 1, 1800000000, -1800000000);          // * 1e7
            //				if(rw != READ)
            //				{
            //						t = get_time_coarse();  // I don't think this used?
            //						target.new_reading = 1;
            //						target.timer = 2500;    //   10 second timeout
            //				}
            break;
            
        case 78:    // Gyro test results
            PARSE_FL_AS_SS(gyrotest.ft_delta_puck, 3, 0, 0, 10.0f);
            PARSE_FL_AS_SS(gyrotest.ft_delta_gcu, 3, 0, 0, 10.0f);
            break;
            
        case 79:    // Debug
            PARSE_SS_AS_SS(debugvar, 8, 0, 0);
            break;
            
        case 80:    // Unlock calibration
            break;
            
        case 81:    // Flash commands - report back status
            //			if(rw == READ){
            //				PARSE_UC_AS_UC(&flash_status, 1, 0xFF, 0);	// Add Status to Packet
            //				if(QBX_CommsPorts[Msg_p->CommPort].BlockComms == 0) flash_status = 0;    // Don't reset flash status when computing message lengths
            //				break;
            //			} else if (rw == WRITEABS) {
            //				//if(FLYING <= status) break;
            //				flash_status = 0;    // Flash_status is set to 1 if a write operation is successful (only for CAL)
            //				machine_fail &= ~CONFIGFAIL;
            //				machine_fail &= ~CALFAIL;
            //				uint8_t FlashCmd;
            //				PARSE_UC_AS_UC(&FlashCmd, 1, 0xFF, 0);	// Get Command from Packet
            //				switch(FlashCmd)
            //				{
            //						case 0:    // Read config flash
            //								machine_fail |= RWD_config(READ);
            //								break;
            //
            //						case 1:    // Write config flash
            //								machine_fail |= RWD_config(WRITE);
            //								break;
            //
            //						case 2:    // read default config
            //								machine_fail |= RWD_config(DEFAULT);
            //								break;
            //
            //						case 3:    // reset flight controller
            //								//Reset_controller();
            //								break;
            //
            //						case 4:    // bootstrap processor
            //								osDelay(2000);
            //								config_page = 0xAA;            // Special value to bootstrap bootloader
            //								Config_RW(WRITE);
            //								osDelay(200);
            //								HAL_NVIC_SystemReset();                // reset processor
            //								break;
            //
            //						case 8:    // Read stored controller cal
            //								machine_fail |= RWD_cal(READ);
            //								break;
            //
            //						case 9:    // Write controller cal
            //								 machine_fail |= RWD_cal(WRITE);
            //								break;
            //
            //						case 10: // Read controller factory cal
            //								machine_fail |= RWD_cal(FACTORY_READ);
            //								break;
            //
            //						case 11: // Write controller factory cal
            //								machine_fail |= RWD_cal(FACTORY_WRITE);
            //								break;
            //
            //						case 12: // Default controller cal
            //								machine_fail |= RWD_cal(DEFAULT);
            //								break;
            //
            //						case 13: // Read factory puck cal
            //								machine_fail |= RWD_puck(READ);
            //								break;
            //
            //						case 14: // Write factory puck cal
            //								machine_fail |= RWD_puck(WRITE);
            //								break;
            //
            //						case 15: // Default puck cal
            //								machine_fail |= RWD_puck(DEFAULT);
            //								break;
            //
            //						case 32: // Spektrum bind
            //								//Set_bind_next_power(TRUE);
            //								break;
            //
            //						case 0x7F: // Indicates that an engineering gui is connected
            //								QBX_CommsPorts[Msg_p->CommPort].EngGUIAccess = TRUE;
            //								break;
            //
            //						case 0xAA: // Engineering cal mode
            //								cal_mode = 0xAA;
            //								flash_status = 0xAA;
            //								break;
            //
            //						case 0xAB: // Customer cal mode
            //								cal_mode = 0xAB;
            //								flash_status = 0xAB;
            //								break;
            //				}
            //			}
            break;
            
        case 82:    // Attitude control loops constants
            PARSE_SL_AS_SS(&config.tilt_angle_kp, 1, 500, 0);
            PARSE_SL_AS_SS(&config.tilt_rate_kp, 1, 500, 0);
            PARSE_SL_AS_SS(&config.tilt_rate_ki, 1, 500, 0);
            PARSE_SL_AS_SS(&config.tilt_rate_kd, 1, 500, -500);
            PARSE_SL_AS_SS(&config.roll_angle_kp, 1, 500, 0);
            PARSE_SL_AS_SS(&config.roll_rate_kp, 1, 500, 0);
            PARSE_SL_AS_SS(&config.roll_rate_ki, 1, 500, 0);
            PARSE_SL_AS_SS(&config.roll_rate_kd, 1, 500, -500);
            PARSE_SL_AS_SS(&config.yaw_angle_kp, 1, 500, 0);
            PARSE_SL_AS_SS(&config.yaw_rate_kp, 1, 500, 0);
            PARSE_SL_AS_SS(&config.yaw_rate_ki, 1, 500, 0);
            PARSE_SL_AS_SS(&config.yaw_rate_kd, 1, 500, -500);
            PARSE_SL_AS_UC(&config.gyro_filter, 1, LSF_MAX_LENGTH, 1);
            
            printf("Processed 82 with result [Pan Stiffness aka config.yaw_rate_kd = %i\n", config.yaw_rate_kd);
            
            break;
            
        case 83:    // Attitude control loops clamps
            PARSE_FL_AS_SS(&config.max_pr_rate, 1, 500.0f, 25.0f, 1.0f);
            if(config.max_pr_rate < config.rc_rate_scale) config.rc_rate_scale = config.max_pr_rate;
            PARSE_FL_AS_SS(&config.max_y_rate, 1, 500.0f, 25.0f, 1.0f);
            PARSE_FL_AS_SS(&config.max_tilt_angle, 1, 90.0f, 10.0f, 1.0f);
            PARSE_FL_AS_SS(&config.min_tilt_angle, 1, -10.0f, -170.0f, 1.0f);
            PARSE_FL_AS_SS(&config.max_roll_angle, 1, 90.0f, 0.0f, 1.0f);
            //				if(rw != READ) PID_init(1);
            break;
            
        case 84:    // Kalman attitude constants
            PARSE_FL_AS_SS(&config.k1accel, 1, 1.0f, 0.0001f, 10000.0f);
            PARSE_FL_AS_SS(&config.k2accel, 1, 10.0f, 0.01f, 100.0f);
            PARSE_FL_AS_SS(&config.k1magnet, 1, 1.0f, 0.0001f, 10000.0f);
            PARSE_FL_AS_SS(&config.k2magnet, 1, 10.0f, 0.01f, 100.0f);
            PARSE_SL_AS_SS(&reserved, 1, 0, 0);
            PARSE_SL_AS_SS(&reserved, 1, 0, 0);
            break;
            
        case 87:    // Height Kalman
            PARSE_FL_AS_SS(&config.baro_accel_k1, 1, 100.0f, 0.0f, 10.0f);
            PARSE_FL_AS_SS(&config.baro_velocity_k2, 1, 1.0f, 0.0f, 1000.0f);
            PARSE_FL_AS_SS(&config.baro_height_k3, 1, 1.0f, 0.0f, 1000.0f);
            break;
            
        case 91:    // Battery
            //if(rw != READ) { if(FLYING <= status) break; }
            PARSE_SL_AS_SC(&config.cells, 1, 6, 0);
            PARSE_FL_AS_SC(&config.battery_alarm, 1, 4.0f, 2.5f, 10.0f);
            PARSE_FL_AS_SC(&config.battery_dead, 1, 4.0f, 2.5f, 10.0f);
            PARSE_FL_AS_SS(&cal.voltage_cal, 1, 150.0f, 5.0f, 10.0f);
            temp = cell_count;
            PARSE_SL_AS_SC(&temp, 1, 0, 0);
            //				if(rw != READ) Calc_gyro_cal(gyro_orientation);
            break;
            
        case 92:    // Motors - manual control
            PARSE_SL_AS_SC(man_motor, 8, 100, 0);
            break;
            
        case 93:    // Motor config
            PARSE_SL_AS_SS(&config.motor_rate, 1, 500, 20);
            PARSE_SL_AS_SC(&config.output_filter, 1, LSF_MAX_LENGTH, 1);
            PARSE_FL_AS_SS(&config.max_motor_current, 1, 100.0f, 0.1f, 100.0f);
            PARSE_UC_AS_UC(config.motor_addr, 8, 255, 0);
            //				if(rw != READ)
            //				{
            //						Calc_motor_const();
            //						for(i = LSF_M0; i <= LSF_M2; i++) LSF_init(i);
            //				}
            break;
            
        case 94:    // Manual shutter (might expand to other camera functions - zoom, exposure etc.)
            PARSE_SL_AS_UC(&gui_shutter, 1, 1, 0);
            break;
            
        case 96:    // Radio mapping
            PARSE_SL_AS_SC(&commonconfig.map[1], 9, 12, -12);
            PARSE_SL_AS_SC(&commonconfig.radioType, 1, MAXRADIOTYPE, 0);
            //if(rw != READ) Radio_config();
            break;
            
        case 97:    // Radio config
            PARSE_SL_AS_UC(&config.pr_stick_window, 1, 100, 0);
            PARSE_SL_AS_UC(&config.yaw_stick_window, 1, 100, 0);
            PARSE_SL_AS_SC(&config.los_delay, 1, 60, 1);
            PARSE_SL_AS_SS(&config.rc_rate_scale, 1, config.max_pr_rate, 1);
            PARSE_SL_AS_SC(&config.pan_expo, 1, 100, 0);
            PARSE_SL_AS_SC(&config.tilt_expo, 1, 100, 0);
            PARSE_SL_AS_SC(&config.rc_lp_filter, 1, 6, 0);
            PARSE_SL_AS_UC(&config.tilt_s_curve, 1, 30, 0);
            PARSE_SL_AS_UC(&config.pan_s_curve, 1, 30, 0);
            PARSE_SL_AS_UC(&config.tilt_control_mode, 1, 1, 0);
            PARSE_SL_AS_UC(&config.roll_control_mode, 1, 1, 0);
            PARSE_SL_AS_UC(&config.pan_control_mode, 1, 1, 0);
            //				if(rw != READ)
            //				{
            //						for(i = LSF_TILT_SERVO; i <= LSF_PAN_SERVO; i++) LSF_init(i);
            //						PID_init(1);        // update outer gain versus RC rate scale
            //				}
            break;
            
        case 98:    // Joint cal
            PARSE_SL_AS_SL(cal.joint_offset, 3, 2147483647LL, -2147483648LL);
            break;
            
        case 99:    // Motor cal
            //if(rw != READ) { if(status >= FLYING) break; }
            PARSE_FL_AS_SS(&cal.motor_available_current, 1, 100.0f, 0.1f, 100.0f);
            //				if(rw != READ) Calc_motor_const();
            break;
            
        case 100:    // Sensor cal - gyros
            //if(rw != READ) { if(FLYING <= status) break; }
            PARSE_FL_AS_SS(puck.gyro_scale, 3, 10.0f, 0.1f, 100.0f);
            PARSE_FL_AS_SS(puck.gyro_ptat_comp, 3, 100.0f, -100.0f, 100.0f);
            PARSE_FL_AS_SS(&puck.gyro_ptat_ref, 1, 60.0f, 0.0f, 10.0f);
            PARSE_FL_AS_SS(&puck.gyro_yaw_induced_roll, 1, 1.0f, -1.0f, 1000.0f);
            PARSE_FL_AS_SS(&puck.gyro_yaw_induced_pitch, 1, 1.0f, -1.0f, 1000.0f);
            PARSE_FL_AS_SS(&puck.gyro_yaw_induced_roll_inv, 1, 1.0f, -1.0f, 1000.0f);
            PARSE_FL_AS_SS(&puck.gyro_yaw_induced_pitch_inv, 1, 1.0f, -1.0f, 1000.0f);
            //				if(rw != READ) Calc_gyro_cal(gyro_orientation);
            break;
            
        case 101:    // Sensor cal - accelerometers
            PARSE_SS_AS_SS(puck.a_max, 3, 32767, -32767);
            PARSE_SS_AS_SS(puck.a_min, 3, 32767, -32767);
            PARSE_FL_AS_SS(puck.accel_offset_tempco, 3, 300.0f, -300.0f, 100.0f);
            PARSE_FL_AS_SS(puck.accel_scale_tempco, 3, 0.01f, -0.01f, 100000.0f);
            PARSE_FL_AS_SS(&puck.accel_temp_ref, 1, 60.0f, 0.0f, 10.0f);
            //				if(rw != READ) Calc_accel_cal(accel_orientation);
            break;
            
        case 102:    // Sensor cal - magnetometers
            PARSE_FL_AS_SS(cal.c_max, 3, 32767.0f, -32767.0f, 1.0f);
            PARSE_FL_AS_SS(cal.c_min, 3, 32767.0f, -32767.0f, 1.0f);
            //				if(rw != READ) Calc_compass_cal(compass_orientation);
            break;
            
        case 103:    // Airframe attitude trim
            PARSE_FL_AS_SS(config.trim, 2, 10.0f, -10.0f, 10.0f);
            PARSE_FL_AS_SS(&config.trim[2], 1, 180.0f, -180.0f, 10.0f);
            break;
            
        case 104:    // Compass config
            PARSE_SL_AS_SC(&config.declination, 1, 45, -45);
            PARSE_SL_AS_UC(&reserved, 1, 1, 0);
            PARSE_SL_AS_UC(&config.auto_heading_k, 1, 100, 0);
            PARSE_SL_AS_UC(&config.enable_auto_mag_offset, 1, 1, 0);
            PARSE_SL_AS_UC(&config.auto_mag_offset_k, 1, 100, 0);
            PARSE_SL_AS_UC(&config.heading_mode, 1, 3, 0);
            PARSE_SL_AS_UC(&compass.start_cal, 1, 1, 0);
            //				if(status <= BOOT) compass.start_cal = 0;
            //				Set_correction();
            break;
            
        case 105:    // GPS Kalman and config, IMU offset
            PARSE_FL_AS_SS(&config.gps_accel_k1, 1, 1.0f, 0.0f, 10000.0f);
            PARSE_SL_AS_SS(&reserved, 1, 0, 0); // Don't use k2 any more
            PARSE_FL_AS_SS(&config.gps_velocity_k3, 1, 1.0f, 0.0f, 1000.0f);
            PARSE_FL_AS_SS(&config.gps_position_k4, 1, 1.0f, 0.0f, 1000.0f);
            PARSE_FL_AS_SS(config.imu_offset, 3, 0.2f, -0.2f, 1000.0f);
            PARSE_FL_AS_SS(&config.maxhacc, 1, 50.0f, 0.1f, 10.0f);
            PARSE_SL_AS_UC(&config.accelerometer_filter, 1, LSF_MAX_LENGTH, 2);
            PARSE_SL_AS_UC(&config.gps_velocity_filter, 1, 10, 2);
            PARSE_UC_AS_UC(&config.acceleration_comp_enable, 1, 1, 0);
            PARSE_FL_AS_SS(&config.gps_accel_threshold, 1, 0.5f, 0.0f, 1000.0f);
            //				if(rw != READ)
            //				{
            //						for(i = LSF_AR_NORTH; i <= LSF_AR_UP; i++) LSF_init(i);
            //						for(i = LSF_GPS_VR_NORTH; i <= LSF_GPS_VR_UP; i++) LSF_init(i);
            //				}
            break;
            
        case 106:    // Modes and features
            PARSE_SL_AS_UC(&config.mode, 1, 1, 0);
            PARSE_FL_AS_SS(&config.pan_majestic_curve, 1, 100, 1, 1.0f);
            PARSE_FL_AS_SS(&config.pan_majestic_window, 1, 90, 0, 1.0f);
            PARSE_FL_AS_SS(&config.pan_majestic_span, 1, 180, 30, 1.0f);
            PARSE_FL_AS_SS(&config.tilt_majestic_curve, 1, 100, 1, 1.0f);
            PARSE_FL_AS_SS(&config.tilt_majestic_window, 1, 90, 0, 1.0f);
            PARSE_FL_AS_SS(&config.tilt_majestic_span, 1, 180, 30, 1.0f);
            PARSE_SL_AS_UC(&config.cat_distance, 1, 100, 0);
            PARSE_SL_AS_UC(&config.cat_speed, 1, 100, 0);
            PARSE_SL_AS_UC(&config.split_rate, 1, 1, 0);
            PARSE_SL_AS_UC(config.axis_mode, 3, 4, 0);
            PARSE_SL_AS_UC(&config.application, 1, 1, 0);
            break;
            
        case 107:    // Config page
            PARSE_UC_AS_UC(&config_page, 1, 2, 0);
            //				if(rw != READ)
            //				{
            //						Config_RW(1);
            //						RWD_config(READ);
            //						Config_variables();
            //				}
            break;
            
        case 108:    // GPS Antenna position
            PARSE_FL_AS_SS(config.gps_ant1_offset, 2, 2.0f, -2.0f, 1000.0f);
            break;
            
        case 109:    // Expert features
            PARSE_SL_AS_SS(&config.shakey_pan, 1, 100, -100);
            PARSE_SL_AS_SS(&config.shakey_tilt, 1, 100, -100);
            PARSE_SL_AS_UC(&config.stiffy_setdown, 1, 1, 0);
            PARSE_SL_AS_SS(&config.gain_scheduling, 1, 1, 0);
            PARSE_SL_AS_SS(&config.roll_notch, 1, 1, 0);
            PARSE_SL_AS_SS(&config.tilt_notch, 1, 1, 0);
            PARSE_SL_AS_UC(&config.boot_mode, 1, 1, 0);
            PARSE_SL_AS_UC(&autotune_enable, 1, 1, 0);
            PARSE_FL_AS_UC(&config.autotune_backoff, 1, 100.0f, 10.0f, 1.0f);
            PARSE_SL_AS_UC(&autotune_progress, 1, 100, 0);
            PARSE_SL_AS_UC(&config.jolt_rejection, 1, 100, 0);
            //				if(status != FLYING) autotune_enable = 0;
            break;
            
        case 110: // Shutter features
            PARSE_FL_AS_SS(&config.sensor_width, 1, 100.0f, 1.0f, 10.0f);
            PARSE_FL_AS_SS(&config.sensor_height, 1, 100.0f, 1.0f, 10.0f);
            PARSE_FL_AS_SS(&config.zoom, 1, 3000.0f, 1.0f, 10.0f);   // 3000mm zoom lens is pretty awesome - John bragging
            PARSE_FL_AS_SS(&config.overlap, 1, 100.0f, 0.0f, 10.0f);
            PARSE_FL_AS_SS(&config.shutter_period, 1, 600.0f, 0.1f, 10.0f); // Max 60 seconds
            PARSE_FL_AS_SS(&config.shutter_duration, 1, 600.0f, 0.1f, 10.0f); // Max 60 seconds
            PARSE_SL_AS_UC(&config.shutter_mode, 1, 14, 0);
            PARSE_SL_AS_UC(&config.shutter_release, 1, 2, 0);
            PARSE_FL_AS_SS(&config.timelapse_tilt_rate, 1, 32.0f, -32.0f, 100.0f);
            PARSE_FL_AS_SS(&config.timelapse_pan_rate, 1, 32.0f, -32.0f, 100.0f);
            PARSE_FL_AS_UC(&config.pointing_accuracy, 1, 5.0f, 0.1f, 10.0f);
            PARSE_SL_AS_UC(&config.aux_function, 1, 1, 0);
            //Shutter_init();
            break;
            
        case 111:   // Targetting options
            PARSE_SL_AS_UC(&config.target_mode, 1, 1, 0);
            PARSE_SL_AS_UC(&config.target_linger, 1, 1, 0);
            PARSE_FL_AS_SS(&config.target_height_offset, 1, 320.0f, -320.0f, 100.0f);
            PARSE_SL_AS_UC(&target.set_current, 1, 1, 0);
            //				if(rw != READ) // If options are changed then trigger a target recalculation
            //				{
            //						target.new_reading = 1;
            //						target.timer = 2500;    // 10 second timeout
            //				}
            break;
            
        case 112: // Logging
            //				if(QBX_CommsPorts[Msg_p->CommPort].EngGUIAccess == TRUE)
            //				{
            //						PARSE_SL_AS_UC(&config.log_rate_index, 1, MAX_LOG_RATES - 1, 0);
            //				}
            //				else
            //				{
            //						PARSE_SL_AS_UC(&config.log_rate_index, 1, ENGINEERING_LOG_RATE_INDEX - 1, 0);
            //				}
            PARSE_SL_AS_UC(&config.record, 1, 1, 0);
            break;
            
        case 118: // Board revision
            if(rw == READ)
            {
                //						coded = 0;//HWREG(FLASH_USERREG1);
                PARSE_SL_AS_SL(&coded, 1, 2147483647LL, -2147483648LL);
            }
            break;
            
        case 119:    // Puck serial number, hardware revision and cal date (RO)
            if(rw == READ)
            {
                //						coded = Obscurify(puck.serial_number);
                PARSE_SL_AS_SL(&coded, 1, 2147483647LL, -2147483648LL);
                PARSE_SL_AS_SL(&puck.hardware_rev, 1, 2147483647LL, -2147483648LL);
                PARSE_SL_AS_SS(&puck.year, 1, 0, 0);
                PARSE_UC_AS_UC(&puck.month, 1, 0, 0);
                PARSE_UC_AS_UC(&puck.day, 1, 0, 0);
            }
            if(rw == WRITEABS)
            {
                PARSE_SL_AS_SL(&coded, 1, 2147483647LL, -2147483648LL);
                //						puck.serial_number = Unobscurify(coded);
                PARSE_SL_AS_SL(&puck.hardware_rev, 1, 2147483647LL, -2147483648LL);
                PARSE_SL_AS_SS(&reserved, 1, 0, 0);
                PARSE_SL_AS_UC(&reserved, 1, 0, 0);
                PARSE_SL_AS_UC(&reserved, 1, 0, 0);
            }
            break;
            
        case 120: // Licence details
            if(rw == READ)
            {
                PARSE_SL_AS_SS(&lic.activate_year, 1, 0, 0);
                PARSE_UC_AS_UC(&lic.activate_month, 1, 0, 0);
                PARSE_UC_AS_UC(&lic.activate_day, 1, 0, 0);
                PARSE_SL_AS_SS(&lic.expire_year, 1, 0, 0);
                PARSE_UC_AS_UC(&lic.expire_month, 1, 0, 0);
                PARSE_UC_AS_UC(&lic.expire_day, 1, 0, 0);
                PARSE_SL_AS_SS(&lic.max_altitude, 1, 0.0f, 0.0f);
                PARSE_SL_AS_SS(&lic.max_range, 1, 0.0f, 0.0f);
                PARSE_SL_AS_UC(&lic.licence_type, 1, 0, 0);
                PARSE_SL_AS_UC(&lic.control_functionality, 1, 0, 0);
                PARSE_SL_AS_UC(&lic.airframe_functionality, 1, 0, 0);
                PARSE_SL_AS_UC(&lic.waypoint_functionality, 1, 0, 0);
                PARSE_SL_AS_UC(&lic.camera_functionality, 1, 0, 0);
            }
            break;
            
        case 121:    // Calibration & hardware details
            if(rw == READ)
            {
                //						coded = Obscurify(cal.serial_number);
                PARSE_SL_AS_SL(&coded, 1, 0, 0);
                //PARSE_SL_AS_SL(&cal.serial_number, 1, 0, 0);
                PARSE_SL_AS_SL(&cal.hardware_type, 1, 0, 0);
                PARSE_SL_AS_SS(&cal.year, 1, 0, 0);
                PARSE_UC_AS_UC(&cal.month, 1, 0, 0);
                PARSE_UC_AS_UC(&cal.day, 1, 0, 0);
                PARSE_UC_AS_UC(cal.rn41_mac, 12, 0, 0);
                //PARSE_UC_AS_UC(dongle.mac, 12, 0, 0);
                PARSE_SL_AS_UC(&config.software_rev_major, 1, 0, 0);
                PARSE_SL_AS_UC(&config.software_rev_minor, 1, 0, 0);
                PARSE_UC_AS_UC(&config_page, 1, 0, 0);
            }
            break;
            
        case 122:    // Sensor internal cal data
            if(rw == READ)
            {
                PARSE_SL_AS_SL(pressure.cal, 8, 0, 0);    // NB pressure cal is actually 16 bit unsigned in datasheet, although we are passing a int32_t here
                PARSE_UC_AS_UC(accel.cal, 8, 0, 0);
            }
            break;
            
        case 123:    // PC date
            PARSE_SS_AS_SS(&pc_date.year, 1, 3000, 0);
            PARSE_UC_AS_UC(&pc_date.month, 1, 12, 1);
            PARSE_UC_AS_UC(&pc_date.day, 1, 31, 1);
            break;
            
        case 124:   // Debug
            PARSE_SL_AS_SS(config.parameter, 8, 32767, -32767);
            break;
            
        case 126:    // stream settings
            if(rw == READ){ 
                for (i = 0; i < 32; i++) PARSE_UC_AS_UC(&QBX_CommsPorts[Msg_p->CommPort].TeleStream[i], 1, 0xFF, 0x00);
                
            } else {	// Write/Configure
                uint8_t StreamByte;
                for (j = 0; j < 32; j++) {
                    PARSE_UC_AS_UC(&StreamByte, 1, 0xFF, 0x00);
                    QBX_CommsPorts[Msg_p->CommPort].TeleStream[j] = StreamByte;
                }
                for (j = 0; j < 16; j++) {
                    QBX_CommsPorts[Msg_p->CommPort].StreamTicker[j] = 0;
                }
            }
            break;
            
            // START BLE Attributes
            
        case 20000:	// Setup BLE Datalog Characteristics
            PARSE_SL_AS_SL(&QBX_BLE_Datalog.Attrib[0], 1, 2147483647LL, 2147483648LL);
            PARSE_SL_AS_SL(&QBX_BLE_Datalog.Attrib[1], 1, 2147483647LL, 2147483648LL);
            PARSE_SL_AS_SL(&QBX_BLE_Datalog.Attrib[2], 1, 2147483647LL, 2147483648LL);
            PARSE_SL_AS_SL(&QBX_BLE_Datalog.Attrib[3], 1, 2147483647LL, 2147483648LL);
            break;
            
        default:
            break;
    }
    
    return QBX_Parser_GetMessagePointer();
}

//----------------------------------------------------------------------------
// Called by the protocol when a message has failed decryption
// This is intended to allow the app to request an encryption key
void QBX_DecryptFail_CB(QBX_Msg_t *Msg_p){
	
	//UART2Send((uint8_t *)"x");
	
	// Select the Type of Key Request to Send based on the Recieved Message's Protocol Type (QB/QBX)
	if (Msg_p->QB_Legacy == 1){
		if (QBX_CommsPorts[Msg_p->CommPort].EncKeyFailedCntr++ >= 50){		// Send a Key Request after 50 Failed Decrypts.
			QBX_CommsPorts[Msg_p->CommPort].EncKeyFailedCntr = 0;
			QBX_SendPacket(69, Msg_p->CommPort, QBX_MSG_TYPE_CURVAL_EVENT, 0, 0, 0, 0, Msg_p->Header.Source_Addr);	// QB Request is a "QB Read" type packet
		}
		
	} else {	// QBX
		if (QBX_CommsPorts[Msg_p->CommPort].EncKeyFailedCntr++ >= 50){		// Send a Key Request after 50 Failed Decrypts.
			QBX_CommsPorts[Msg_p->CommPort].EncKeyFailedCntr = 0;
			QBX_SendPacket(690, Msg_p->CommPort, QBX_MSG_TYPE_WRITE_ABS, 0, 0, 0, 0, Msg_p->Header.Source_Addr);		// Write QBX Encryption Key Request Packet
		}
	}
	
}

//----------------------------------------------------------------------------
// Send Tx Message to Comms Port
// This is an application specific function that directs the message to the appropriate communications port buffer.
// UART: AddUART(x)Tx(), BLE: aci_gatt_update_char_value(), etc
void QBX_SendMsg2CommsPort_CB(QBX_Msg_t *TxMsg_p){
	int i;
	
	if (QBX_CommsPorts[TxMsg_p->CommPort].BlockComms){		// If Comms are Blocked, don't actually send the message to the interface (used for finding std message size)
		return;
	}
	
	// Send Message to Appropriate Port:
	switch (TxMsg_p->CommPort){
		
		case QBX_COMMS_PORT_DEBUG:

            TxMsg_p->MsgBuf_p = &TxMsg_p->MsgBuf[0];
            for(i = 0; i < TxMsg_p->MsgBuf_MsgLen; i++){
                int i = *TxMsg_p->MsgBuf_p++;
                printf("b[%#02x]", i);
            }
            
			break;
		
		case QBX_COMMS_PORT_UART1:
			TxMsg_p->MsgBuf_p = &TxMsg_p->MsgBuf[0];
			for(i = 0; i < TxMsg_p->MsgBuf_MsgLen; i++){
//					AddUART1Tx(*TxMsg_p->MsgBuf_p++);
			}
//			UART1TX_empty_buffer();
			break;
		
		case QBX_COMMS_PORT_UART2:
			TxMsg_p->MsgBuf_p = &TxMsg_p->MsgBuf[0];
			for(i = 0; i < TxMsg_p->MsgBuf_MsgLen; i++){
//					AddUART2Tx(*TxMsg_p->MsgBuf_p++);
			}
//			UART2TX_empty_buffer();
			break;
		
		case QBX_COMMS_PORT_UART3:
			break;
		
		case QBX_COMMS_PORT_BLE_DIRECT_ACK:
			break;
		
		case QBX_COMMS_PORT_BLE_DIRECT_NAK0:
			break;
		
		case QBX_COMMS_PORT_BLE_DIRECT_NAK1:
			break;
		
		case QBX_COMMS_PORT_BLE_UART:
			TxMsg_p->MsgBuf_p = &TxMsg_p->MsgBuf[0];
			for(i = 0; i < TxMsg_p->MsgBuf_MsgLen; i++){
//				Ble_UART_BufAdd(&Ble_UART_QX.TX, *TxMsg_p->MsgBuf_p++);
			}
			break;
		
		default:
			break;
	}
}

//----------------------------------------------------------------------------
// | 7           | 6           | 5           | 4           | 3           | 2           | 1           | 0
// | IMU         | IMU         | Drive       | Drive       | TrackMode  | TrackMode    | TrackMode   | NULL
//
// | 7           | 6           | 5           | 4           | 3           | 2           | 1           | 0
// | STATUS      | STATUS      | STATUS      | MachineFail | Kalman lock | RadioLOS    | GPSLOS      | CompassBad
//
void AddStatusWordToPacket()
{
  
}

//----------------------------------------------------------------------------
// | 7           | 6           | 5           | 4           | 3           | 2           | 1           | 0
// | CamState    | CamState    | CamState    | CamState    | SeqCommand  | SeqCommand  | SeqCommand  | SeqCommand
//
// | 7           | 6           | 5           | 4           | 3           | 2           | 1           | 0
// | CmdIndex    | CmdIndex    | CmdIndex    | CmdIndex    | CmdIndex    | CmdIndex    | CmdIndex    | CmdIndex
//
void AddCameraStatusWordToPacket(void)
{
	uint8_t statusByte = 0;
//    statusByte = statusByte << 4;
//    statusByte |= 0;
//    *msgPtr++ = statusByte;
//    
//    statusByte =  0;
//    *msgPtr++ = statusByte;
	
	// NOTE: This code above made no sense... must be left over...
	// Shortcut for now: Add two bytes
	PARSE_SL_AS_UC(&statusByte, 1, 0xFF, 0x00);
	PARSE_SL_AS_UC(&statusByte, 1, 0xFF, 0x00);
}

//----------------------------------------------------------------------------
// Check if the option byte is used (std or special) in the write attribute
uint8_t QBX_QBLegacy_ChkUseOptByte(uint8_t att){

	if (att < 64){		// All Attributes less than 64 don't use an option byte!
		return 0;
	} else if ((att == 78) || (att == 79) || (att == 81) || (att == 120)|| (att == 121)|| (att == 122)|| (att == 126)){		// Special Attributes
		return 0;
	} else {	// ALL OTHER ATTRIBUTES Use Option Byte
		return 1;
	}
}

//----------------------------------------------------------------------------
// Take Mutex for RX Protection
void QBX_TakeMutex_Rx(void){
	
}

//----------------------------------------------------------------------------
// Give Mutex for RX Protection
void QBX_GiveMutex_Rx(void){
	
}

//----------------------------------------------------------------------------
// Take Mutex for TX Protection
void QBX_TakeMutex_Tx(void){
	
}

//----------------------------------------------------------------------------
// Give Mutex for TX Protection
void QBX_GiveMutex_Tx(void){
	
}


//----------------------------------------------------------------------------
void SendPacket(uint32_t attrib) {
    
    printf("Senidngn attr = %u \n",attrib);// should be 82
    
    QBX_SendPacketStd(attrib, QBX_COMMS_PORT_DEBUG , QBX_MSG_TYPE_WRITE_DELTA);
}





