/*-----------------------------------------------------------------
    Copyright by Freefly Systems Inc 2015
		
    Contains proprietary algorithms and source code
    Copyright by SM Webb and JG Ellison 2010-2012
		
    Filename: "QBX_Protocol.h"
		
		Description:
-----------------------------------------------------------------*/

#ifndef QBX_PROTOCOL_H
#define QBX_PROTOCOL_H

//****************************************************************************
// Headers
//****************************************************************************
#include <stdlib.h>		// for Standard Data Types
#include <stdint.h>		// for Standard Data Types
#include "QBX_Protocol_AppInterface.h"		// This contains the application specific interface functions

//****************************************************************************
// Definitions
//****************************************************************************


//****************************************************************************
// Data Types
//****************************************************************************

// Encryption Key Flag
typedef enum {
	QBX_ENC_KEY_STAT_INVALID,
	QBX_ENC_KEY_STAT_VALID,
} QBX_Enc_Key_Stat_Type_e;

// DEVICE IDs (Used in Source/Destination IDs)
typedef enum {
	QBX_DEV_ID_BROADCAST,
	QBX_DEV_ID_GENERAL_CONFIG_GUI,
	QBX_DEV_ID_MOBILE_CONFIG_GUI,
	QBX_DEV_ID_ENGINEERING_CONFIG_GUI,
	QBX_DEV_ID_MOVI_CONTROLLER,
	QBX_DEV_ID_FIZ_CAM1,
	QBX_DEV_ID_FIZ_CAM2,
	QBX_DEV_ID_FIZ_3D,
	QBX_DEV_ID_GIMBAL,
	QBX_DEV_ID_FLIGHT_CONTROLLER_CORE1,
	QBX_DEV_ID_FLIGHT_CONTROLLER_CORE2,
	QBX_DEV_ID_GPS_COMPASS_UNIT,
	QBX_DEV_ID_FLOGGER,
	QBX_DEV_ID_LENS_CONTROLLER,
	QBX_DEV_ID_
} QBX_DevId_e;

// QBX Message Types
typedef enum {
	QBX_MSG_TYPE_CURVAL_EVENT,			// These Message Types Contain the Current Value of the Attribute
	QBX_MSG_TYPE_CURVAL_PERIODIC,		// Attribute value sent periodically
	QBX_MSG_TYPE_CURVAL_WRITERESP,	
	QBX_MSG_TYPE_CURVAL_READRESP,		
	QBX_MSG_TYPE_READ,							// Read Request (Cur Val is Response)
//	QBX_MSG_TYPE_PERIODIC_CFG_WRITE,	// Request the attribute to be sent at a given rate.
//	QBX_MSG_TYPE_PERIODIC_CFG_READ,		// Check the 
	QBX_MSG_TYPE_WRITE_ABS,					// Writes the Absolute Values to the Application Variables
	QBX_MSG_TYPE_WRITE_DELTA,				// Writes the Delta (Change in) Values to the Application Variables
	QBX_MSG_TYPE_WRITE_BITMASK_SET,
	QBX_MSG_TYPE_WRITE_BITMASK_CLR,
	QBX_MSG_TYPE_WRITE_BITMASK_TOG,
	QBX_MSG_TYPE_RESERVED_11,
	QBX_MSG_TYPE_RESERVED_12,
	QBX_MSG_TYPE_RESERVED_13,
	QBX_MSG_TYPE_RESERVED_14,
	QBX_MSG_TYPE_RESERVED_15,
	QBX_MSG_TYPE_EXTENDED						// Last Type is an Extended Type
} QBX_Msg_Type_e;

// QBX Message Classes
typedef enum {
	QBX_MSG_CLASS_OTHER,	
	QBX_MSG_CLASS_READ,
	QBX_MSG_CLASS_WRITE
} QBX_Msg_Class_e;

// QBX Send Message Types
typedef enum {
	QBX_MSG_SENDTYPE_CURVAL,	// Message is a Current Value Status Update
	QBX_MSG_SENDTYPE_WRITE		// Message is a write command
} QBX_Msg_SendType_e;

// QBX Message Parse Type
typedef enum {
	QBX_MSG_PARSETYPE_RECV_WRITE,	// Recieve Data from a source (MSG->APPDATA)
	QBX_MSG_PARSETYPE_SEND_RESP,	// Send a Message with Current Values (APPDATA->MSG)
	QBX_MSG_PARSETYPE_RECV_RESP,	// Recieve a response message (MSG->APPDATA)
	QBX_MSG_PARSETYPE_SEND_WRITE	// Write a Message to a target (APPDATA->MSG)
} QBX_Msg_ParseType_e;

// QBX Message Parse Direction
typedef enum {
	QBX_MSG_PARSE_DIR_RECV,
	QBX_MSG_PARSE_DIR_SEND
} QBX_Msg_ParseDir_e;

// QBX Status Enum
typedef enum {
	QBX_STAT_OK,
	QBX_STAT_ERROR,
	QBX_STAT_ERROR_MSG_TYPE_NOT_SUPPORTED,
	QBX_STAT_ERROR_ENCRYPTED_NO_DATA,	// The packet doesn't contain data, but is marked as encrypted
	QBX_STAT_ERROR_DECRYPT_FAILED,
	QBX_STAT_ERROR_BAD_ENC_KEY
} QBX_Stat_e;

// Message Frame Header Data
typedef struct {
	
	uint16_t MsgLength;			// Message Length Field
	
	uint32_t Attrib;				// 32 bit Attribute Number. QB Attributes are the first 7 bits worth of values, the rest are QBX
	
	QBX_Msg_Type_e Type;		// Specific Message Type (enum)
	
	// Miscellaneous Flag Bits
	uint8_t Silent;					// Dont Send a Response if 1
	uint8_t VariableLen;		// Data Payload is varible length (Recieved Length should not be checked against the stored attribute length).
	uint8_t AddOptionByte1;	// Adds Option byte 1
	
	// Encryption
	uint8_t Encrypted;			// Message is Encrypted
	uint8_t Encrypted_Resp;	// Response should be Encrypted
	uint8_t Enc_Rand0;			// Random Value Byte for Encryption
	uint8_t Enc_Rand1;			// Random Value Byte for Encryption
	
	// Addressing
	uint8_t Remove_Addr_Fields;	// Removes the Address fields (for speed optimized transmission when the addresses not needed)
	uint32_t Source_Addr;				// Source Address
	uint32_t Target_Addr;				// Target Address
	
	// Data Timestamp
	uint8_t Add_Timestamp;	// Signals that the Timestamp field is added.
	uint32_t Timestamp;		// Timestamp when transmitted data was Sampled. Used primarily for logging
	
} QBX_MsgHeader_t;

// QBX Message Structure
// This contains all of the data that makes up a QBX Message
// The "QBX_" functions operate on instances of this data type.
typedef struct {
	
	// Message Meta Data
	//QBX_Msg_ParseDir_e	ParseDir;		// Parsing Direction 
	//QBX_Msg_ParseType_e ParseType;	// Determines Method for handling Parsing of Data
	uint8_t QB_Legacy;								// Indicates that this is a Legacy QB Format Message and should be Tx/Rx'd this way
	QBX_Enc_Key_Stat_Type_e EncKeyStat;	// Used to show if the Message has passed decryption successfully
	uint8_t BLE_Direct_Compatible;		// =1 Indicates the constructed packet has been verified to fit in a Single BLE 20 byte QB Direct characteristic
	uint8_t DisableStdResponse;				// If 1, the Auto Response to Write and Read Mesasages will be disabled. This allows the Message Parser to Control the option of a Response Message.
	uint8_t RunningChecksum;					// Running Checksum Value for calculating checksum while recieving characters
	
	//QBX_Msg_Class_e MsgClass;
	
	// Communication Port
	QBX_Comms_Port_e CommPort;
	
	// Message Header Data
	QBX_MsgHeader_t Header;
	
	// Message Data Buffer (Contains Actual Message Data)
	uint8_t MsgBuf[QBX_MAX_MSG_LEN];
	uint16_t MsgBuf_MsgLen;					// Length of the Current Message in the Buffer
	
	// Message Pointers and Lengths 
	uint8_t *MsgBufQBAtt_p;					// Points to the Legacy QB Attribute Field
	uint8_t *MsgBufPayloadBegin_p;	// Points to the begining of the Message Payload Field, and not moved within the buffer!
	uint8_t *MsgBuf_p;							// General Pointer for Parser Usage (Used for both TX and RX)
	uint16_t MsgDataLength;					// Length of just the Data portion of the packet
	
} QBX_Msg_t;

// QBX Comms Port Type - Contains info specific to each instance of a communications port
typedef struct {
	unsigned char RxState;										// State of Stream RX State Machine
	unsigned short RxStreamCntr;							// Count Chars RX'd from Stream
	QBX_Msg_t RxMsg;													// One Deadicated Message Instance for Each Port to Recieve Messages To
	uint8_t BlockComms;												// Blocks the actual sending of the message to the comms port.
	uint8_t EngGUIAccess;											// A Engineering GUI is connected!
	
	//uint8_t EncCapable;												// 1 = Encryption Capable Port, with Deadicated Codebook
	//uint8_t *RC4_Codebook_p;									// Pointer to the Codebook for this comms instance. 
	uint32_t EncKeyFailedCntr;								// Counts Key Failures to detrmine when the send a key reequest
	QBX_Enc_Key_Stat_Type_e QBX_Enc_Key_Stat;	// Contains the Status of Encryption on the Port
	
	// Streaming Control
	uint8_t TeleStream[32];
	uint8_t StreamTicker[16];
	
} QBX_CommsPort_t;


//****************************************************************************
// Public Vars
//****************************************************************************
extern QBX_CommsPort_t QBX_CommsPorts[QBX_COMMS_PORT_QUANTITY];
extern QBX_DevId_e QBXSourceAddress;


//****************************************************************************
// Public Function Prototypes
//****************************************************************************

// Recieve Characters from a stream, and handle recieved messages
void QBX_StreamRxCharSM(QBX_Comms_Port_e port, unsigned char rxbyte);

// Recieve a Message - Parses a Message as Recieved into a Message Structure 
// and Performs Processing if Neccesary (Send Response, Save Data, etc)
void QBX_RecvPacket(uint8_t *RxMsgBuf, QBX_Comms_Port_e CommPort);

// Full Featured Send QBX Packet - For Asynchronous use by the Application (Not in imediate response to Read/Write)
// This function builds an appropriate message structure and calls calls QBX_TxMsg().
void QBX_SendPacket(uint32_t Attrib, QBX_Comms_Port_e CommPort, QBX_Msg_Type_e Type, uint8_t Encrypt, uint8_t Silent, uint8_t AddTimestamp, uint8_t AddrDisable, uint8_t TargetAddr);

// Send a Standard QBX Packet using the most common options 
// (no Encrypt, no timestamp, address fields enabled, response enabled, send to broadcast address)
void QBX_SendPacketStd(uint32_t Attrib, QBX_Comms_Port_e CommPort, QBX_Msg_Type_e Type);

// Send a Current Value Status QBX Packet with most optional fields disabled, Timestamp Field Enabled (for DataLogging)
void QBX_SendPacketDataLog(uint32_t Attrib, QBX_Comms_Port_e CommPort);

// Set the Source Address of the Local Device (Generally called once during setup)
void QBX_SetSourceAddress(uint8_t SourceAddr);

// Sets the Protocol to use for messages that are sent from this device (responses match the type of the message they respond to)
// 0 = QBX, 1 = Legacy QB
void QBX_SetSendProtocol(uint8_t ProtocolType);

// Setup the Protocol!
void QBX_Init(void);

//****************************************************************************
// Function Prototypes to be Implemented by Application Interface
//****************************************************************************

// Parse Packet Server - Application Callback
// This Call Back Function is the primary point of exchange of comms/application data.
// This primairly handles the "Server/Slave" Role, Accepting Requests and Sending Responses
extern uint8_t *QBX_ParsePacket_Srv_CB(QBX_Msg_t *Msg_p);

// Parse Packet Client - Application Callback
// This primairly handles the "Client/Master" Role, Accepting Responses and Sending Requests
extern uint8_t *QBX_ParsePacket_Cli_CB(QBX_Msg_t *RxMsg_p);

// Called by the protocol when a message has failed decryption
// This is intended to allow the app to request an encryption key
extern void QBX_DecryptFail_CB(QBX_Msg_t *Msg_p);

// Send Tx Message to Comms Port - Application Callback
// This is an application specific function that directs the message to the appropriate communications port buffer.
// UART: AddUART(x)Tx(), BLE: aci_gatt_update_char_value(), etc
extern void QBX_SendMsg2CommsPort_CB(QBX_Msg_t *TxMsg_p);

// Parse the Message to Determine the Protocol Type (QBX or QB)
extern void QBX_GetMsgProtocolType(QBX_Msg_t *Msg_p);

// Mutexes for Protection
extern void QBX_TakeMutex_Rx(void);
extern void QBX_GiveMutex_Rx(void);
extern void QBX_TakeMutex_Tx(void);
extern void QBX_GiveMutex_Tx(void);

#endif
