/*-----------------------------------------------------------------
    Copyright by Freefly Systems Inc 2015
		
    Contains proprietary algorithms and source code
    Copyright by SM Webb and JG Ellison 2010-2012
		
    Filename: "QBX_Protocol.c"
-----------------------------------------------------------------*/

//****************************************************************************
// Headers
//****************************************************************************
#include "QBX_Protocol.h"
#include "QBX_Protocol_AppInterface.h"		// This contains the application specific interface functions
#include "QBX_Parsing_Functions.h"
#include "rc4.h"
#include <stdlib.h>		// for Standard Data Types
#include <stdint.h>		// for Standard Data Types

//****************************************************************************
// Private Definitions
//****************************************************************************


//****************************************************************************
// Private Global Vars
//****************************************************************************
QBX_CommsPort_t QBX_CommsPorts[QBX_COMMS_PORT_QUANTITY];
QBX_DevId_e QBXSourceAddress;
uint32_t QBX_Send_Protocol_Type = 1;	// QBX = 0, QB = 1

//****************************************************************************
// Private Function Prototypes
//****************************************************************************

// Initialize QBX Message Structure with Defaults
void QBX_InitMsg(QBX_Msg_t *Msg_p);

// Recieve QBX Packet, Process and Respond if Neccessary
uint32_t QBX_RxMsg(QBX_Msg_t *RxMsg_p);

// Transmit QBX Packet according to Message Structure
uint32_t QBX_TxMsg(QBX_Msg_t *TxMsg_p);

// Build QBX Message Header using data from the message structure
void QBX_BuildHeader(QBX_Msg_t *Msg_p);

// Build QB Message Header using data from the message structure (for Legacy QB Usage)
void QBX_BuildHeader_LegacyQB(QBX_Msg_t *Msg_p);

// Parse QBX Message Header to populate the message structure
void QBX_ParseHeader(QBX_Msg_t *Msg_p);

// Parse QB Message Header to populate the message structure (for Legacy QB Usage)
void QBX_ParseHeader_LegacyQB(QBX_Msg_t *Msg_p);

// Add a 7th bit extendable value to a buffer
void QBX_AddExtdValToBuf(uint8_t **p, uint32_t Val);

// Get a 7th bit extendable value from a buffer
uint32_t QBX_GetExtdValFromBuf(uint8_t **p);

// Check if the option byte is used (std or special) in the write attribute
uint8_t QBX_QBLegacy_ChkUseOptByte(uint8_t att);

// Calculate the Attribute Message Lengths for fixed length Standard Attributes
void CalcMessageLengths(void);

// calculate 8 bit checksum
uint8_t QBX_Calc8bChecksum(uint8_t *buf_p, uint32_t len);

// Creates an Encrypted Connection with another device
// This function aquires a handle to a codebook resource if it is availible.
// Returns Encryption Status
uint8_t QBX_SetupEncryptConnect(uint32_t Address, uint8_t *PublicKey, uint8_t PublicKeyLen);

// Encode/Decode - Use Address to select connection
// return status (0 = OK, 1 = No Connection, 2 = Decryption Fail
uint8_t QBX_Endecode_protocol(uint32_t Address, uint8_t *Buf, uint8_t BufLen);

//****************************************************************************
// Private Function Definitions
//****************************************************************************

//----------------------------------------------------------------------------
// Initialize all data in a Message Structure
void QBX_InitMsg(QBX_Msg_t *Msg_p){
	
	// Meta Data Fields
	Msg_p->BLE_Direct_Compatible = 0;
	Msg_p->DisableStdResponse = 0;
	Msg_p->EncKeyStat = 0;
	Msg_p->MsgBuf_MsgLen = 0;
	Msg_p->MsgDataLength = 0;
	Msg_p->QB_Legacy = QBX_Send_Protocol_Type;
	Msg_p->CommPort = 0;
	Msg_p->RunningChecksum = 0;
	
	// Header Fields
	Msg_p->Header.AddOptionByte1 = 0;
	Msg_p->Header.Add_Timestamp = 0;
	Msg_p->Header.Attrib = 0;
	Msg_p->Header.Encrypted = 0;
	Msg_p->Header.Encrypted_Resp = 0;
	Msg_p->Header.MsgLength = 0;
	Msg_p->Header.Remove_Addr_Fields = 0;
	Msg_p->Header.Silent = 0;
	Msg_p->Header.Source_Addr = 0;
	Msg_p->Header.Target_Addr = 0;
	Msg_p->Header.Timestamp = 0;
	Msg_p->Header.Type = 0;
	Msg_p->Header.VariableLen = 0;
	
	// Pointer Init
	Msg_p->MsgBufPayloadBegin_p = NULL;
	Msg_p->MsgBufQBAtt_p = &Msg_p->MsgBuf[0];
	Msg_p->MsgBuf_p = &Msg_p->MsgBuf[0];
	
	
}

//----------------------------------------------------------------------------
// Recieve QBX Packet, Process and Respond if Neccessary
//
uint32_t QBX_RxMsg(QBX_Msg_t *RxMsg_p){
	uint8_t chksum;
	
	QBX_TakeMutex_Rx();
	
	RxMsg_p->MsgBufQBAtt_p = &RxMsg_p->MsgBuf[0];
	
	// Parse Protocol Header
	QBX_GetMsgProtocolType(RxMsg_p);
	if (RxMsg_p->QB_Legacy){	
		QBX_ParseHeader_LegacyQB(RxMsg_p);
	} else {									
		QBX_ParseHeader(RxMsg_p);
	}
	RxMsg_p->MsgDataLength = RxMsg_p->Header.MsgLength - (RxMsg_p->MsgBuf_p - RxMsg_p->MsgBufQBAtt_p);
	RxMsg_p->MsgBufPayloadBegin_p = RxMsg_p->MsgBuf_p;	// Set the Start of Payload Pointer
	
	// Check to see if the Length is correct
	//QBX_CheckLength(RxMsg_p);
	// Note, use qsort to sort an array of the supported attributes by attribute #
//	uint32_t AttIdx = QBX_GetAttIdx(RxMsg_p->Header.Attrib);	// Returns -1 if not found
//	if (RxMsg_p->MsgDataLength != QBX_StdMsgLengths[AttIdx]){
//		
//	}
	
	// Decryption FIXME: Add Connection Based Decrypt
	if (RxMsg_p->Header.Encrypted){
		RxMsg_p->MsgDataLength--;	// Remove Encrypted Checksum from the Data Length
		
		if (RxMsg_p->MsgDataLength > 0){	// Must Have at least some payload to decrypt
			Endecode_protocol(RxMsg_p->MsgBufPayloadBegin_p - 2, RxMsg_p->MsgDataLength + 2);	// Add 2 for the Rand Bytes
			
			// Calc Checksum after Decrypt
			chksum = QBX_Calc8bChecksum(RxMsg_p->MsgBufPayloadBegin_p, RxMsg_p->MsgDataLength);
			
			// Test the Checksum (The Encrypted Checksum is located after the data)
			RxMsg_p->MsgBuf_p = RxMsg_p->MsgBufPayloadBegin_p + RxMsg_p->MsgDataLength;
			if(chksum + *RxMsg_p->MsgBuf_p != 0xFF)
			{
				RxMsg_p->EncKeyStat = QBX_ENC_KEY_STAT_INVALID;	// Bad checksum
				
				// Call the User Callback to Allow the App to Request a Key
				QBX_DecryptFail_CB(RxMsg_p);
				
				return QBX_STAT_ERROR_DECRYPT_FAILED;
			} else {
				RxMsg_p->EncKeyStat = QBX_ENC_KEY_STAT_VALID;		// Good checksum, Proceed!
			}
		} else {
			return QBX_STAT_ERROR_ENCRYPTED_NO_DATA;
		}
	}
	
	// Prepare to Parse Message
	RxMsg_p->DisableStdResponse = 0;	// Enable Response by default
	QBX_Parser_SetMessagePointer(RxMsg_p->MsgBufPayloadBegin_p);	// Set the Parser Pointer to the Start of the Payload
	
	// Handle Each Type of Message
	// If response needed, change message type to the response type; Otherwise, disable the response
	switch (RxMsg_p->Header.Type){
		
		case QBX_MSG_TYPE_READ:
			RxMsg_p->Header.Type = QBX_MSG_TYPE_CURVAL_READRESP;
			break;
		
		case QBX_MSG_TYPE_WRITE_ABS:
			QBX_Parser_SetDir_WriteAbs();
			RxMsg_p->MsgBuf_p = QBX_ParsePacket_Srv_CB(RxMsg_p);
			RxMsg_p->Header.Type = QBX_MSG_TYPE_CURVAL_WRITERESP;
			break;
		
		case QBX_MSG_TYPE_WRITE_DELTA:
			QBX_Parser_SetDir_WriteRel();
			RxMsg_p->MsgBuf_p = QBX_ParsePacket_Srv_CB(RxMsg_p);
			RxMsg_p->Header.Type = QBX_MSG_TYPE_CURVAL_WRITERESP;
			break;
		
		case QBX_MSG_TYPE_CURVAL_EVENT:			// All current value messages handled using the single callback
		case QBX_MSG_TYPE_CURVAL_PERIODIC:
		case QBX_MSG_TYPE_CURVAL_WRITERESP:
		case QBX_MSG_TYPE_CURVAL_READRESP:
			RxMsg_p->DisableStdResponse = 1;
			RxMsg_p->MsgBuf_p = QBX_ParsePacket_Cli_CB(RxMsg_p);
			break;
		
		default:
			return QBX_STAT_ERROR_MSG_TYPE_NOT_SUPPORTED;
			break;
	}
	
	// Send a Standard Response if Needed (Reuse the Message Instance)
	if ((RxMsg_p->Header.Silent == 0) && (RxMsg_p->DisableStdResponse == 0)){
		
		// Change the Message Direction
		RxMsg_p->Header.Target_Addr = RxMsg_p->Header.Source_Addr;
		RxMsg_p->Header.Source_Addr = QBXSourceAddress;
		RxMsg_p->Header.Encrypted = RxMsg_p->Header.Encrypted_Resp;
		RxMsg_p->Header.Encrypted_Resp = 0;
		
		// Send the Response
		QBX_TxMsg(RxMsg_p);
	}
	
	QBX_GiveMutex_Rx();
	
	return QBX_STAT_OK;
}

//----------------------------------------------------------------------------
// Transmitt QBX Packet according to Message Structure
uint32_t QBX_TxMsg(QBX_Msg_t *TxMsg_p) {
	uint8_t chksum;
	
	QBX_TakeMutex_Tx();
	
	TxMsg_p->MsgBufQBAtt_p = &TxMsg_p->MsgBuf[4];
	
	// Build the Frame Header
	if (TxMsg_p->QB_Legacy){
		QBX_BuildHeader_LegacyQB(TxMsg_p);	// QB Protocol
	} else {									
		QBX_BuildHeader(TxMsg_p);						// QBX Protocol
	}
	
	QBX_Parser_SetMessagePointer(TxMsg_p->MsgBufPayloadBegin_p);	// Set the Parser Pointer to the Start of the Payload
	
	// Handle Each Type of Message
	// The Parsers return the message pointer, which must point to the byte after the data payload.
	switch (TxMsg_p->Header.Type){
		
		case QBX_MSG_TYPE_READ:
			// Just Send Request Message, No Payload Data
			break;
		
		case QBX_MSG_TYPE_CURVAL_EVENT:
		case QBX_MSG_TYPE_CURVAL_PERIODIC:
		case QBX_MSG_TYPE_CURVAL_WRITERESP:
		case QBX_MSG_TYPE_CURVAL_READRESP:
			QBX_Parser_SetDir_Read();
			TxMsg_p->MsgBuf_p = QBX_ParsePacket_Srv_CB(TxMsg_p);	// Put Current Value Data in the Payload, Return the Payload Pointer
			break;
		
		case QBX_MSG_TYPE_WRITE_ABS:
		case QBX_MSG_TYPE_WRITE_DELTA:
			TxMsg_p->MsgBuf_p = QBX_ParsePacket_Cli_CB(TxMsg_p);		// Put Write Command Data in the Payload
			break;
		
		default:
			return QBX_STAT_ERROR_MSG_TYPE_NOT_SUPPORTED;
			break;
	}
	
	// Find the Message Data Length
	TxMsg_p->MsgDataLength = TxMsg_p->MsgBuf_p - TxMsg_p->MsgBufPayloadBegin_p;
	
	// Calculate and Add Encryption Checksum if needed
	if (TxMsg_p->Header.Encrypted){
		
		Endecode_protocol(TxMsg_p->MsgBufPayloadBegin_p - 2, TxMsg_p->MsgDataLength + 2);
		
		chksum = QBX_Calc8bChecksum(TxMsg_p->MsgBufPayloadBegin_p, TxMsg_p->MsgDataLength);
		
		*TxMsg_p->MsgBuf_p++ = 0xFF - chksum;	// Add in the Encrypted Checksum
	}
	
	// Find the Message Length
	TxMsg_p->Header.MsgLength = TxMsg_p->MsgBuf_p - TxMsg_p->MsgBufQBAtt_p;
	
	// Add the Final Message Size In
	TxMsg_p->MsgBuf[2] = (TxMsg_p->Header.MsgLength >> 8) & 0xFF;
	TxMsg_p->MsgBuf[3] = TxMsg_p->Header.MsgLength & 0xFF;
	
	// Calculate and Add Overall Checksum
	chksum = QBX_Calc8bChecksum(TxMsg_p->MsgBufQBAtt_p, TxMsg_p->Header.MsgLength);
	
	*TxMsg_p->MsgBuf_p++ = 0xFF - chksum;	// Add in the Overall Checksum
	
	// Save the size of the overall message buffer
	TxMsg_p->MsgBuf_MsgLen = TxMsg_p->MsgBuf_p - &TxMsg_p->MsgBuf[0];
	
	// Check to see if this is compatible with BLE 20 byte payload limit.
	// Strip Off the "Q, B, LEN_MSB, LEN_LSB, QB_ATT, OVERALL_CHECKSUM" (6 Bytes) Fields
	TxMsg_p->BLE_Direct_Compatible = (((TxMsg_p->MsgBuf_MsgLen - 6) < 20) && (TxMsg_p->QB_Legacy == 0)) ? 1:0;
	
	// Send the Message to the Appropriate Comms Port
	QBX_SendMsg2CommsPort_CB(TxMsg_p);
	
	QBX_GiveMutex_Tx();
	
	return QBX_STAT_OK;
}

//----------------------------------------------------------------------------
// Build a QBX Message Header
void QBX_BuildHeader(QBX_Msg_t *Msg_p){
	
	Msg_p->MsgBuf[0] = 'Q';	// Start Delimiter Chars
	Msg_p->MsgBuf[1] = 'B';
	
	Msg_p->MsgBuf_p = Msg_p->MsgBufQBAtt_p;		// Set to Legacy Attribute Field
	*Msg_p->MsgBuf_p++ = 0xFF;	// 0xFF = Use QBX Attribute Type
	
	// Add the Extendible Attribute Number
	QBX_AddExtdValToBuf(&Msg_p->MsgBuf_p, Msg_p->Header.Attrib);
	
	// Add Base Option Byte
	uint8_t OptionByte = 0;
	OptionByte |= Msg_p->Header.Type & 0xF;	
	OptionByte |= (Msg_p->Header.Add_Timestamp & 0x01) << 4;
	OptionByte |= (Msg_p->Header.Remove_Addr_Fields & 0x01) << 5;
	OptionByte |= (Msg_p->Header.Encrypted & 0x01) << 6;
	OptionByte |= (Msg_p->Header.AddOptionByte1 & 0x01) << 7;
	*Msg_p->MsgBuf_p++ = OptionByte;
	
	// Add Option Byte 1 if Needed
	if (Msg_p->Header.AddOptionByte1){
		OptionByte = 0;
		OptionByte |= (Msg_p->Header.Silent & 0x01) << 0;
		OptionByte |= (Msg_p->Header.Encrypted_Resp & 0x01) << 1;
		OptionByte |= (Msg_p->Header.VariableLen & 0x01) << 2;
		*Msg_p->MsgBuf_p++ = OptionByte;
	}
	
	// Add Optional Timestamp
	if (Msg_p->Header.Add_Timestamp){
		*Msg_p->MsgBuf_p++ = (Msg_p->Header.Timestamp >> 24) & 0xFF;
		*Msg_p->MsgBuf_p++ = (Msg_p->Header.Timestamp >> 16) & 0xFF;
		*Msg_p->MsgBuf_p++ = (Msg_p->Header.Timestamp >> 8) & 0xFF;
		*Msg_p->MsgBuf_p++ = (Msg_p->Header.Timestamp >> 0) & 0xFF;
	}
	
	// Add the Extendible Device ID Numbers
	if (Msg_p->Header.Remove_Addr_Fields == 0){
		QBX_AddExtdValToBuf(&Msg_p->MsgBuf_p, Msg_p->Header.Source_Addr);
		QBX_AddExtdValToBuf(&Msg_p->MsgBuf_p, Msg_p->Header.Target_Addr);
	} else {
		Msg_p->Header.Source_Addr = 0;
		Msg_p->Header.Target_Addr = 0;
	}
	
	// If Encrypted, Add Random Values
	if (Msg_p->Header.Encrypted){
		srand(123456);
		*Msg_p->MsgBuf_p++ = (uint8_t)rand();
		*Msg_p->MsgBuf_p++ = (uint8_t)rand();
	}
	
	Msg_p->MsgBufPayloadBegin_p = Msg_p->MsgBuf_p;	// Set the Payload Begin Pointer
}

//----------------------------------------------------------------------------
// Build a Legacy Type QB Message Header
void QBX_BuildHeader_LegacyQB(QBX_Msg_t *Msg_p){
	uint8_t OptionByte = 0;
	uint8_t UseOptByte;
	
	Msg_p->MsgBuf[0] = 'Q';	// Start Delimiter Chars
	Msg_p->MsgBuf[1] = 'B';
	
	// Add the Attribute Number
	Msg_p->MsgBuf_p = &Msg_p->MsgBuf[4];	// Set to Legacy Attribute Field
	*Msg_p->MsgBuf_p = Msg_p->Header.Attrib & 0xFF;
	
    printf("building legacy with attrib %u" , Msg_p->Header.Attrib);
    
    
	switch(Msg_p->Header.Type){
		
	case QBX_MSG_TYPE_CURVAL_EVENT:
	case QBX_MSG_TYPE_CURVAL_PERIODIC:
	case QBX_MSG_TYPE_CURVAL_WRITERESP:
	case QBX_MSG_TYPE_CURVAL_READRESP:
	case QBX_MSG_TYPE_READ:
		break;
		
		case QBX_MSG_TYPE_WRITE_ABS:
			*Msg_p->MsgBuf_p |= 0x80;
			OptionByte |= 0x1;
		break;
		
		case QBX_MSG_TYPE_WRITE_DELTA:
			*Msg_p->MsgBuf_p |= 0x80;
		OptionByte &= ~0x1;
		break;
		
		default:
		break;
	}
	Msg_p->MsgBuf_p++;
	
	UseOptByte = QBX_QBLegacy_ChkUseOptByte(Msg_p->Header.Attrib);		// Check to see if this particular attribute uses an option byte or not
	
	// Add Option Byte if Used
	if (UseOptByte){
		OptionByte |= Msg_p->Header.Encrypted << 1;
		OptionByte |= (uint8_t)(Msg_p->Header.Target_Addr & 0x3F) << 2;	// Device ID
		*Msg_p->MsgBuf_p++ = OptionByte;
	}
	
	// If Encrypted, Add Random Values
	if (Msg_p->Header.Encrypted){
		srand(123456);
		*Msg_p->MsgBuf_p++ = (uint8_t)rand();	// NOTE: FIXME USE REAL RANDOM NUM GENERATOR
		*Msg_p->MsgBuf_p++ = (uint8_t)rand();
	}
	
	Msg_p->MsgBufPayloadBegin_p = Msg_p->MsgBuf_p;	// Set the Payload Begin Pointer
}

//----------------------------------------------------------------------------
// Parse a QBX Type Message
// Note, this Function leaves the Pointer at the Start of the payload
void QBX_ParseHeader(QBX_Msg_t *Msg_p){
	uint8_t OptionByte, temp;
	uint32_t temp32;
	Msg_p->MsgBufQBAtt_p = &Msg_p->MsgBuf[0];
	Msg_p->MsgBuf_p = Msg_p->MsgBufQBAtt_p;
	Msg_p->MsgBuf_p++;	// Skip the QB Attribute Field
	
	// Get the Extendible Attribute Number
	Msg_p->Header.Attrib = QBX_GetExtdValFromBuf(&Msg_p->MsgBuf_p);
	
	// Get Base Option Byte Bits
	OptionByte = *Msg_p->MsgBuf_p++;
	Msg_p->Header.Type = OptionByte & 0xF;
	Msg_p->Header.Add_Timestamp = (OptionByte >> 4) & 0x1;
	Msg_p->Header.Remove_Addr_Fields = (OptionByte >> 5) & 0x1;
	Msg_p->Header.Encrypted = (OptionByte >> 6) && 0x1;
	Msg_p->Header.AddOptionByte1 = (OptionByte >> 7) & 0x1;
	
	// If Availible, Get Option Byte 1 Bits
	if (Msg_p->Header.AddOptionByte1){
		OptionByte = *Msg_p->MsgBuf_p++;
		Msg_p->Header.Silent = (OptionByte >> 0) & 0x1;
		Msg_p->Header.Encrypted_Resp = (OptionByte >> 1) && 0x1;
		Msg_p->Header.VariableLen = (OptionByte >> 2) && 0x1;
	}
	
	// If Availible, Get the Timestamp
	if (Msg_p->Header.Add_Timestamp){
		temp32 = ((uint32_t)*Msg_p->MsgBuf_p++) << 24;
		temp32 |= ((uint32_t)*Msg_p->MsgBuf_p++) << 16;
		temp32 |= ((uint32_t)*Msg_p->MsgBuf_p++) << 8;
		temp32 |= ((uint32_t)*Msg_p->MsgBuf_p++) << 0;
		Msg_p->Header.Timestamp = temp32;
	}
	
	// If Availible, Get the Addresses
	if (Msg_p->Header.Remove_Addr_Fields == 0){
		Msg_p->Header.Source_Addr = QBX_GetExtdValFromBuf(&Msg_p->MsgBuf_p);
		Msg_p->Header.Target_Addr = QBX_GetExtdValFromBuf(&Msg_p->MsgBuf_p);
	}
	
	// If Encrypted, Skip the Rand Bytes
	if (Msg_p->Header.Encrypted){
		Msg_p->MsgBuf_p += 2;
	}
}

//----------------------------------------------------------------------------
// Parse a Legacy QB Type Message
// Note, this Function leaves the Pointer at the Start of the payload
void QBX_ParseHeader_LegacyQB(QBX_Msg_t *Msg_p){
	uint8_t OptionByte, temp8, UseOptByte;
	uint32_t temp32;
	Msg_p->MsgBufQBAtt_p = &Msg_p->MsgBuf[0];
	Msg_p->MsgBuf_p = Msg_p->MsgBufQBAtt_p;
	uint8_t write;
	
	// Get Message Type and Attribute Number
	write = *Msg_p->MsgBuf_p & 0x80;
	Msg_p->Header.Attrib = *Msg_p->MsgBuf_p++ & 0x7F;
	
	UseOptByte = QBX_QBLegacy_ChkUseOptByte(Msg_p->Header.Attrib);		// Check to see if this particular attribute uses an option byte or not
	
	if (write){	// Write
		if (UseOptByte){
			OptionByte = *Msg_p->MsgBuf_p++;
			if (OptionByte & 0x1){	// Bit 0: Absolute (1) or Delta (0)
				Msg_p->Header.Type = QBX_MSG_TYPE_WRITE_ABS;
			} else {
				Msg_p->Header.Type = QBX_MSG_TYPE_WRITE_DELTA;
			}
			// Parse Option Byte Fields
			Msg_p->Header.Target_Addr = (OptionByte >> 2) & 0x3F;
			Msg_p->Header.Encrypted = (OptionByte >> 1) & 0x1;
		} else {
			Msg_p->Header.Type = QBX_MSG_TYPE_WRITE_ABS;
			Msg_p->Header.Target_Addr = 0;
			Msg_p->Header.Encrypted = 0;
		}
		
	} else {	// Read
		Msg_p->Header.Type = QBX_MSG_TYPE_READ;
		Msg_p->Header.Target_Addr = 0;
		Msg_p->Header.Encrypted = 0;
	}
	
	// If Encrypted, Skip the Rand Bytes
	if (Msg_p->Header.Encrypted){
		Msg_p->MsgBuf_p += 2;
	}
	
	// Set Additional QBX Fields
	Msg_p->Header.AddOptionByte1 = 0;
	Msg_p->Header.Add_Timestamp = 0;
	Msg_p->Header.Encrypted_Resp = 0;
	Msg_p->Header.Remove_Addr_Fields = 0;
	Msg_p->Header.Source_Addr = 0;
	Msg_p->Header.Silent = 0;
	Msg_p->Header.Timestamp = 0;
	Msg_p->Header.VariableLen = 0;
}

//----------------------------------------------------------------------------
// Determine the Protocol Type (QBX or QB)
void QBX_GetMsgProtocolType(QBX_Msg_t *Msg_p){
	if (*Msg_p->MsgBufQBAtt_p == 0xFF){
		Msg_p->QB_Legacy = 0;
	} else {
		Msg_p->QB_Legacy = 1;
	}
}

//----------------------------------------------------------------------------
// Calculate 8 Bit Checksum
uint8_t QBX_Calc8bChecksum(uint8_t *buf_p, uint32_t len){
	uint8_t checksum = 0;
	for(int j = 0; j < len; j++)
	{
		checksum += *buf_p++;
	}
	return checksum;
}

//----------------------------------------------------------------------------
// Add a 7th bit extendable (4 byte max) value to a buffer
void QBX_AddExtdValToBuf(uint8_t **p, uint32_t Val){
	
	for (int n = 0; n < 4; n++){
		uint8_t this_chunk = (Val >> (7*n)) & 0x7F;
		uint8_t next_chunk = (Val >> (7*(n+1))) & 0x7F;
		if (next_chunk > 0){
			**p = this_chunk | 0x80;
			(*p)++;
		} else {
			**p = this_chunk;
			(*p)++;
			break;
		}
	}
}

//----------------------------------------------------------------------------
// Get a 7th bit extendable (4 byte max) value from a buffer
uint32_t QBX_GetExtdValFromBuf(uint8_t **p){
	uint32_t Val = 0;
	
	for (int n = 0; n < 4; n++){
		Val = (((uint32_t)(**p & 0x7F)) << (7*n)) | Val;
		if (**p & 0x80){
			(*p)++;
		} else {
			(*p)++;
			break;
		}
	}
	return Val;
}


//****************************************************************************
// Public Function Definitions
//****************************************************************************

//----------------------------------------------------------------------------
// QBX Stream RX Char State Machine
// Accepts 1 charater from a serial data stream and recieves full messages
void QBX_StreamRxCharSM(QBX_Comms_Port_e port, unsigned char rxbyte){
	uint8_t chksum;
	
	switch(QBX_CommsPorts[port].RxState)
	{
		case 0:
				if(rxbyte == 'Q')
				{
						QBX_CommsPorts[port].RxState = 1;
				}
				break;
				
		case 1:
				if(rxbyte == 'B')
				{
						QBX_CommsPorts[port].RxState = 2;
				}
				else
				{
						QBX_CommsPorts[port].RxState = 0;
				}
				break;
				
		case 2:
				QBX_CommsPorts[port].RxMsg.Header.MsgLength = rxbyte << 8;
				QBX_CommsPorts[port].RxState = 3;
				break;
				
		case 3:
				QBX_CommsPorts[port].RxMsg.Header.MsgLength |= rxbyte;
				QBX_CommsPorts[port].RxMsg.RunningChecksum = 0;
				QBX_CommsPorts[port].RxState = 4;
				QBX_CommsPorts[port].RxStreamCntr = 0;
				if(QBX_MAX_MSG_LEN < QBX_CommsPorts[port].RxMsg.Header.MsgLength)        // Maximum message length = 64, may change this later
				{
						QBX_CommsPorts[port].RxState = 0;
				}
				break;
				
		case 4:
				QBX_CommsPorts[port].RxMsg.MsgBuf[QBX_CommsPorts[port].RxStreamCntr] = rxbyte;
				QBX_CommsPorts[port].RxMsg.RunningChecksum += rxbyte;
				QBX_CommsPorts[port].RxStreamCntr++;
				if(QBX_CommsPorts[port].RxMsg.Header.MsgLength <= QBX_CommsPorts[port].RxStreamCntr)
				{
						QBX_CommsPorts[port].RxState = 5;
				}
				break;
				
		case 5:
				chksum = rxbyte;
				QBX_CommsPorts[port].RxState = 0;
				QBX_CommsPorts[port].RxMsg.CommPort = port;
				if((chksum + QBX_CommsPorts[port].RxMsg.RunningChecksum) == 0xFF)		// Is checksum ok?
				{
						QBX_RxMsg(&QBX_CommsPorts[port].RxMsg);	// Receive the Message
				}
				break;
	}
}

//----------------------------------------------------------------------------
// Recieve a Whole, Pre-Error-Checked Packet
// (Used for WIFI, BLE, and other packet based interfaces which have thier own Error Checking).
// Note, Buffer contains full message except for Q,B,LEN1,LEN2 and CHECKSUM Bytes
void QBX_PacketRx(QBX_Comms_Port_e port, uint8_t *buf, uint16_t buf_len){
	for(int i=0; i<buf_len; i++){
		QBX_CommsPorts[port].RxMsg.MsgBuf[i] = *buf++;
	}
	QBX_CommsPorts[port].RxMsg.CommPort = port;
	QBX_CommsPorts[port].RxMsg.Header.MsgLength = buf_len;
	QBX_RxMsg(&QBX_CommsPorts[port].RxMsg);	// Receive the Message
}

//----------------------------------------------------------------------------
// Full Featured Send QBX Packet - For Asynchronous use by the Application (Not in auto response to Read/Write)
// This function builds an appropriate message structure and calls calls QBX_TxMsg().
void QBX_SendPacket(uint32_t Attrib, QBX_Comms_Port_e CommPort, QBX_Msg_Type_e Type, uint8_t Encrypt, uint8_t Silent, uint8_t AddTimestamp, uint8_t AddrDisable, uint8_t TargetAddr){
	QBX_Msg_t TxMsg;
	
	TxMsg.CommPort = CommPort;
	TxMsg.Header.Attrib = Attrib;
	TxMsg.Header.Type = Type;
	TxMsg.Header.Encrypted = Encrypt;
	TxMsg.Header.Silent = Silent;
	TxMsg.Header.Add_Timestamp = AddTimestamp;
	TxMsg.Header.Remove_Addr_Fields = AddrDisable;
	TxMsg.Header.Target_Addr = TargetAddr;
	TxMsg.Header.Source_Addr = QBXSourceAddress;
	
	// Set other unused option bytes
	TxMsg.QB_Legacy = QBX_Send_Protocol_Type;
	TxMsg.Header.VariableLen = 0;
	TxMsg.Header.Encrypted_Resp = 0;
	
	// Extend Option Byte Field if neccesary
	if (TxMsg.Header.Encrypted_Resp || TxMsg.Header.Silent || TxMsg.Header.VariableLen){
		TxMsg.Header.AddOptionByte1 = 1;
	} else {
		TxMsg.Header.AddOptionByte1 = 0;
	}
	
	QBX_TxMsg(&TxMsg);	// Send the Message
}

//----------------------------------------------------------------------------
// Send a Standard QBX Packet using the most common options 
// (no Encrypt, no timestamp, address fields enabled, response enabled, send to broadcast address)
void QBX_SendPacketStd(uint32_t Attrib, QBX_Comms_Port_e CommPort, QBX_Msg_Type_e Type){
	QBX_SendPacket(Attrib, CommPort, Type, 0, 0, 0, 0, 0);
}

//----------------------------------------------------------------------------
// Send a Current Value Status QBX Packet with most optional fields disabled, Timestamp Field Enabled (for DataLogging)
void QBX_SendPacketDataLog(uint32_t Attrib, QBX_Comms_Port_e CommPort){
	QBX_SendPacket(Attrib, CommPort, QBX_MSG_TYPE_CURVAL_PERIODIC, 0, 0, 1, 1, 0);
}

//----------------------------------------------------------------------------
// Sets the Protocol to use for messages that are sent from this device (responses match the type of the message they respond to)
// 0 = QBX, 1 = Legacy QB
void QBX_SetSendProtocol(uint8_t ProtocolType){
	QBX_Send_Protocol_Type = ProtocolType;
}

//----------------------------------------------------------------------------
// Setup the Protocol!
void QBX_Init(void){
	Init_RC4();
	
	QBX_SetSendProtocol(1);
}

