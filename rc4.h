/*-----------------------------------------------------------------
    Copyright by Freefly Systems Inc 2015

    Contains proprietary algorithms and source code
    Copyright by SM Webb and JG Ellison 2010-2012

    Filename: "rc4.h"
-----------------------------------------------------------------*/

#ifndef RC4_H
#define RC4_H

#include <stdint.h>

extern void Init_RC4(void);
extern void DoKSA(uint8_t *, int32_t);
extern uint8_t GetPRGAOutput(void);
extern void EnDecode(uint8_t *, uint32_t, uint8_t *);
extern void Prime_PRBS(uint8_t *);
extern void Copy_codebook(void);
extern void Endecode_protocol(uint8_t * p, uint32_t n);

#endif
