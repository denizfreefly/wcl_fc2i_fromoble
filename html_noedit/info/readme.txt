The info folder should include all meta data for configurators across all platforms.

These include;
->help/info webpages
->Firmware
->Extra parameter metadata (control type, min/max) etc.

Currently the V1 Configurators point to "info/index.html" future revs should move index.html to "info/web/index.html"

