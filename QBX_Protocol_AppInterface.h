/*-----------------------------------------------------------------
    Copyright by Freefly Systems Inc 2015
		
    Contains proprietary algorithms and source code
    Copyright by SM Webb and JG Ellison 2010-2012
		
    Filename: "QBX_Protocol_AppInterface.h"
-----------------------------------------------------------------*/

#ifndef QBX_PROTOCOL_APPINTERFACE_H
#define QBX_PROTOCOL_APPINTERFACE_H

//****************************************************************************
// Headers
//****************************************************************************
#include <stdlib.h>		// for Standard Data Types
#include <stdint.h>		// for Standard Data Types

//****************************************************************************
// Definitions
//****************************************************************************
#define QBX_MAX_MSG_LEN			64
#define COMMS_SERVICE_RATE	1000	// HZ

//****************************************************************************
// Data Types
//****************************************************************************

// Communication Ports - port dependent
typedef enum {
	QBX_COMMS_PORT_DEBUG,
	QBX_COMMS_PORT_UART1,
	QBX_COMMS_PORT_UART2,
	QBX_COMMS_PORT_UART3,
	QBX_COMMS_PORT_BLE_DIRECT_ACK,
	QBX_COMMS_PORT_BLE_DIRECT_NAK0,
	QBX_COMMS_PORT_BLE_DIRECT_NAK1,
	QBX_COMMS_PORT_BLE_UART,
	QBX_COMMS_PORT_BLE_DLOG1,
	QBX_COMMS_PORT_BLE_DLOG2,
	QBX_COMMS_PORT_QUANTITY		// Ensure this remains as the last value because it sets the number of ports!
} QBX_Comms_Port_e;

// BLE Datalog Setup
typedef struct {
	uint8_t SendIdx;
	uint32_t Attrib[4];	// Attribute Number (Bandwidth is split between all)
} QBX_BLE_Datalog_t;


//****************************************************************************
// Public Function Prototypes
//****************************************************************************

// Service the QBX Comms Ports. Check for RX Messages, etc
void QBX_Service_Comms(void);




//****************************************************************************
// NEEDS CLEAN UP - Global data Variables/Structs
//****************************************************************************

#define TWOPI 6.28318531f
#define DEGTORAD 0.01745329f
#define RADTODEG 57.29577951f
#define g 9.80665f // acceleration due to gravity

// Calibration procedures
#define CALIBRATION_ACCEL                   0
#define CALIBRATION_LAST_CUST               0
#define CALIBRATION_CROSSAXIS               1
#define CALIBRATION_CROSSAXIS_INV           2
#define CALIBRATION_TEMPCO                  3
#define CALIBRATION_PUCK_REPORT             4
#define CALIBRATION_CONTROLLER_REPORT       5
#define CALIBRATION_LAST_ENG                5

#define CAUSAL            1
#define NON_CAUSAL        0

#define LSF_ROLL         0
#define LSF_PITCH         1
#define LSF_YAW             2
#define LSF_GPS_VR_NORTH 3
#define LSF_GPS_VR_EAST     4
#define LSF_GPS_VR_UP     5
#define LSF_M0             6
#define LSF_M1             7
#define LSF_M2             8
#define LSF_JOINT_ROLL     9
#define LSF_JOINT_TILT    10
#define LSF_JOINT_PAN    11
#define LSF_AR_NORTH    12
#define LSF_AR_EAST     13
#define LSF_AR_UP        14
#define LSF_TILT_SERVO    15
#define LSF_ROLL_SERVO    16
#define LSF_PAN_SERVO    17
#define LSF_LAST        18
#define LSF_MAX_LENGTH    30

#define HPF_Y_ACCEL        0
#define HPF_Z_ACCEL        1
#define HPF_LAST        2

#define LPF_BATTERY            0
#define LPF_PTAT            1
#define LPF_ACCEL_FREQ        2
#define LPF_COMP_FREQ        3
#define LPF_GPS_FREQ        4
#define LPF_RADIOA_FREQ        5
#define LPF_RADIOB_FREQ        6
#define LPF_RADIO_FREQ        7
#define LPF_POINTING_FREQ     8
#define LPF_TARGET_FREQ     9
#define LPF_LAST            10

#define LMS_BSF_ROLL_GYRO    0
#define LMS_LAST        1
#define LMS_MAX_LENGTH    32

#define WIN_LAST        1
#define WIN_MAX_LENGTH    50


#define ACCELFAIL        0x0001    // 9150 or 6050 issue
#define GYROFAIL        0x0002    // 6050 issue
#define COMPASSFAIL        0x0004    // 9150 issue
#define PRESSUREFAIL    0x0008
#define GPSFAIL            0x0010
#define BATTFAIL        0x0020
#define ATTITUDEFAIL    0x0040
#define RN41FAIL        0x0080
#define DRIVEFAIL        0x0100
#define CALFAIL            0x0200
#define GIMBALFAIL        0x0400    // gimbal badly orientated
#define CONFIGFAIL        0x0800
#define OS_FAIL            0x1000
#define FAIL5   0x2000
#define FAIL6   0x4000
#define FAIL7   0x8000


//#warning SET FIRMWARE BUILD AND HARDWARE VERSION HERE
#define COMMSREV 7
#define HWTYPE 5
#define SW_REV_MAJOR 4
#define SW_REV_MINOR 1

#define TRUE 1
#define FALSE 0

#define DEBUG0

#define LICENSEDVERSION
#define LICENCE_DETAILS // uncomment to show licence details in green text

#define HW_RN41 0
#define HW_WF121 1

//#define DEBUG // disable motors and allow kalmans to work for walk-around testing
//#define CLEAN_FLASH_PAGES // write default config and cal to flash pages prior to making licence
//#define MAKE_LICENCE // create a working licence

// to install a licence:
// compile with clean flash pages, load on and reboot
// compile with make licence, load on and reboot
// compile normal software, load on and reboot
// write cal and factory cal, then reboot

// Timer 1 ticks per ms
#define TICKSPERMS 50000

// Scaling applied to max/min to get reasonable GUI values
#define ACCEL_PRESCALE 80
#define COMPASS_PRESCALE 1

// These are nominal values required by the VB calibration code
extern int32_t lsb_per_deg_per_sec; // LSB per gyro deg/sec
extern int32_t lsb_per_g; // LSB per 1g
extern int32_t lsb_per_t; // LSB per earth field unit

#define TWOPI 6.28318531f
#define DEGTORAD 0.01745329f
#define RADTODEG 57.29577951f
#define g 9.80665f // acceleration due to gravity

#define READ 0
#define WRITE 1
#define DEFAULT 2
#define FACTORY_READ 3
#define FACTORY_WRITE 4

#define SAMPLE_RATE 10000 // 10kHz ADC sampling
#define IMU_RATE 1000.0f // 1000Hz IMU tick, PWM runs at its own rate
#define INV_IMU_RATE 0.001f

// Radio Control channel defines
#define PITCH_CMD 1
#define ROLL_CMD 2
#define YAW_CMD 3
#define CH4_CMD 4
#define MODE_CMD 5
#define PAN_CLAMP_CMD 6
#define TILT_CLAMP_CMD 7
#define SHUTTER_CMD 8
#define CH9_CMD 9

// flight modes
#define YM_RATE 0
#define YM_HEADING_HOLD 1
#define YM_TAIL_DRAGGER 2
#define HM_OL 0
#define HM_VARIO 1
#define HM_VARIO_HH 2
#define PM_OL 0
#define PM_VELOCITY_PH 1 // Modes 1 and 2 swapped so that rate mode is last (not used)
#define PM_VELOCITY 2
#define AM_RATE 0
#define AM_ANGLE 1 // self levelling
#define PM_NORMAL 0
#define PM_SAUCER 1
#define PM_ORBIT 2

// MOVI application defines
#define HANDHELD_APP 0
#define MR_APP 1

// MOVI motion booting
#define NORMAL_BOOT 0
#define MOTION_BOOTING 1

// Targetting options
#define TARGET_MODE_POSITION 0
#define TARGET_MODE_POSITION_HEIGHT 1
#define TARGET_LINGER_TIMEOUT 0
#define TARGET_LINGER_FOREVER 1

// PTR (pan/tilt/roll) control modes
#define PTR_CONTROL_RATE 0
#define PTR_CONTROL_ANGLE 1

// Control modes
#define CM_ACCEL 0
#define CM_VELOCITY 1

// IMU defines
#define ROLL 0
#define PITCH 1
#define YAW 2
#define BATTERY 3
#define PTAT 4

#define AX 0
#define AY 1
#define AZ 2

#define NORTH 0
#define EAST 1
#define UP 2

#define COMPASS 0
#define ACCEL 1

// Gimbal parking
#define FLOPPY 0
#define GOING_FLOPPY 1
#define PARKING 2
#define PARKED 3
#define ACTIVE 4
#define ACQUIRING 5

// Axis modes
#define NORMAL 0
#define JOINT_ANGLE 1
#define GYRO_RATE 2
#define DISABLED 3
#define JOINT_RATE 4

// Heading modes
#define HM_FREERUN 0
#define HM_FIXEDMOUNT 1
#define HM_GPS 2
#define HM_MAGNETOMETER 3

// heading_state
#define ACQUIRE 0
#define LOCKED 1

// Aux function
#define AUX_UART 0
#define AUX_SHUTTER 1

#define NOT_ON_STATION 0x00 // bit masks for on station checks
#define POSITION_CORRECT 0x01
#define ATTITUDE_CORRECT 0x02
#define ON_STATION 0x03

// Tracking modes (gui info only)
#define MODE_NORMAL 0
#define MODE_HEADTRACKER 1
#define MODE_TARGET 2
#define MODE_SEQUENCE 3

// Control Modes
#define OPENLOOP 0
#define VELOCITY 1
#define POSITION 2
#define CAMERA_LEVEL 3
#define CAMERA_LOCK 4

// UART buffer length
#define TXBUF 1536
#define RXBUF 512

// camera servos
#define CAM_TILT 0
#define CAM_ROLL 1
#define CAM_PAN 2
#define CAM_SHUT 3

// Radio types
#define DSM2_1024 0
#define DSM2_2048 1
#define DSMX_1024 2
#define DSMX_2048 3
#define SBUS 4
#define PPM_GRAUPNER 5
#define FTX 6
#define MAXRADIOTYPE 6

// Power options
#define PO_LIPO 0
#define PO_EXTERNAL 1

//extern config_type config;

extern int32_t lsb_per_deg_per_sec; // LSB per gyro deg/sec
extern int32_t lsb_per_g; // LSB per 1g
extern int32_t lsb_per_t; // LSB per earth field unit

extern int32_t man_motor[8];
extern float motor[3];
extern float joint[3];
extern float jointrads[3];
extern int32_t uber_clamp;
extern float motor_to_amps;
extern float motor_to_percent;
extern float joint_rate[3];
extern float estimated_temp[3];
extern uint32_t cell_count;
extern uint32_t new_joint_reading;
extern float battery_current;
extern float battery_voltage;
extern float tilt_flip;
extern float charge_used;
extern uint32_t drive_led_status[3];

//extern volatile uint16_t machine_fail;
//extern uint32_t status;
extern float timeNC;
extern float occupancy ;
extern int32_t alarm_led;

extern uint32_t time_idle_entry;
extern uint32_t time_idle_entry_last;
extern uint32_t time_idle_exit;
extern uint32_t time_in_idle;
extern uint32_t period_idle;
extern float idle_percent;

extern float * att;
extern float * e;

extern float c_live_offset[3];// = { 0.0f, 0.0f, 0.0f };
extern float * a_pred;
extern float * rate_offset;
extern float ptat;
extern float * rate;
extern float * rate_filt;
extern float h_correction;
extern float * rate_raw;
extern float * rate_cmd_error;
extern float * rate_control;
extern float * rate_cmd;
extern float climb_control;
extern float * vr_cmd_error;
extern float * att_cmd_error;
extern float * ar_cmd_error;
extern float * att_cmd;
extern float * ar_cmd;
extern float * xr_cmd_error;
extern float * rate_cmd_error;
extern float * vr_cmd;
extern float * cal_mode;
extern float * debugvar;
extern uint32_t  gyro_orientation;
extern uint32_t  accel_orientation;
extern uint32_t compass_orientation;
extern uint8_t config_page;
extern uint32_t autotune_enable;// = 0;
extern uint32_t autotune_progress;// = 0;

typedef struct
{
    uint32_t time_acquired;
    uint32_t new_fix;
    uint32_t new_time_pulse;
    uint32_t pvt;
    uint8_t fix;             // register to say what kind of fix we have (looking for 0x03=3D)
    uint8_t new_data;        // new valid GPS data
    uint8_t los;            // timed out
    int32_t latitude;
    int32_t longitude;
    int32_t latitude_prev;
    int32_t longitude_prev;
    int32_t latitude_home;
    int32_t longitude_home;
    int32_t latitude_origin;
    int32_t longitude_origin;
    float altitude;
    float vr[3];
    float ar[3];                    // LSF accel in reference frame
    float gspeed;
    float heading;
    uint8_t sats;
    float sacc;                        // speed accuracy
    float hacc;                        // horizontal accuracy
    float vacc;                     // vertical accuracy
    int32_t ack;
    float freq;
    uint32_t crc;
    uint8_t sbas_mode;
    int8_t sbas_system;
    uint8_t sbas_prn;
    uint16_t agc;
    uint8_t jam_indicator;
    uint16_t year;
    uint8_t month;
    uint8_t day;
    uint8_t hour;
    uint8_t min;
    uint8_t sec;
    uint8_t sw[31];
    uint8_t hw[11];
    uint8_t rom[31];
    float xr[3];                    // reference frame position in metres
    uint8_t kalman_locked;    // flag to indicate velocity kalman is locked
    uint8_t first_fix;      // set when 3D fix first achieved
    uint8_t heading_mode;
    uint8_t mode;
    float height_ref;
    uint32_t clockticks_per_sec;
    float timer_to_hz;
} GPS_type;

typedef struct
{
    uint8_t new_data;
    uint8_t los;
    int32_t ch[13];
    float a_freq, b_freq, freq;
    uint8_t los_timeout;
    uint8_t a_new_frame, b_new_frame;
    uint32_t control_loss;
    uint32_t a_dropouts, b_dropouts;
    uint8_t a_los, b_los;
    uint32_t a_bad_frames, b_bad_frames;
    uint8_t data_source;
} radio_type;

typedef struct
{
    uint32_t los;
    uint32_t last_time;
} husq_type;

typedef struct
{
    uint32_t cal[8];
    uint32_t new_data;
    uint32_t reading;
    float T;
    float P;
    float h;
    float h_corr;
    float P0;
    float factor;
} Pressure_type;

typedef struct
{
    int32_t latitude;
    int32_t longitude;
    float altitude;
    float position[3];
    float range[3];
    float distance;
    float heading;
    float elevation;
    uint32_t new_reading;
    float freq;
    float timer;
    uint32_t set_current;
} target_type;

typedef struct
{
    int16_t ptat_raw;
    uint32_t sample_time;
    float rate;
    float time;
    uint32_t latency;
} IMU_type;

typedef struct
{
    uint8_t flashed[3];
    int32_t serial_number;
    int32_t hardware_type;
    int32_t year;
    uint8_t month;
    uint8_t day;
    uint8_t rn41_mac[13];
    uint32_t baro_cal[8];
    float voltage_cal;
    float c_max[3];
    float c_min[3];
    float motor_available_current;
    int32_t joint_offset[3];
    int32_t reserved[15];
} cal_type;


typedef struct
{
    int16_t year;
    uint8_t month;
    uint8_t day;
} pc_date_type;

typedef struct
{
    int32_t map[10];
    int32_t radioType;
    int32_t reserved[8];
    int32_t bind_enable;
} commonconfig_type;

// When changing config try and use the reserved gaps for extra parameters and add reserved placeholders for deleted parameters
// so that stored config parameters do not move around in flash.
// If the version number is being changed then this is not very important as the config will be defaulted anyway.
typedef struct
{
    // General
    uint8_t flashed[3];
    uint32_t software_rev_major;
    uint32_t software_rev_minor;
    int32_t cells;
    float battery_alarm;
    float battery_dead;
    float imu_offset[3];
    float gps_ant1_offset[3];
    float trim[3];
    int32_t reserved1[7];
    
    // Motors
    int32_t output_filter;
    int32_t motor_rate;
    float max_motor_current;
    uint8_t motor_addr[8];
    int32_t axis_mode[3];
    int32_t reserved2[4];
    
    // Compass
    int32_t declination;
    int32_t auto_heading_k;
    //uint8_t enable_auto_declination;
    uint8_t reservedA;
    int32_t auto_mag_offset_k;
    uint8_t enable_auto_mag_offset;
    int32_t heading_mode;
    int32_t reserved3[7];
    
    // Kalman filters
    float k1accel;
    float k2accel;
    float k1magnet;
    float k2magnet;
    float baro_accel_k1;
    float baro_velocity_k2;
    float baro_height_k3;
    float gps_accel_k1;
    float gps_velocity_k3;
    float gps_position_k4;
    int32_t reserved4[8];
    
    // Filters
    int32_t gyro_filter;
    int32_t accelerometer_filter;   // NOT USED
    int32_t gps_velocity_filter;
    int32_t vert_accel_filter;
    float gps_accel_threshold;
    int32_t reserved5[7];
    
    // PID coefficients
    int32_t tilt_angle_kp;
    int32_t tilt_rate_kp;
    int32_t tilt_rate_ki;
    int32_t tilt_rate_kd;
    int32_t roll_angle_kp;
    int32_t roll_rate_kp;
    int32_t roll_rate_ki;
    int32_t roll_rate_kd;
    int32_t yaw_angle_kp;
    int32_t yaw_rate_kp;
    int32_t yaw_rate_ki;
    int32_t yaw_rate_kd;
    int32_t reserved6[8];
    
    // Clamps
    float max_tilt_angle;
    float max_pr_rate;
    float max_y_rate;       // Not used
    float min_tilt_angle;
    float max_roll_angle;
    int32_t reserved7[6];
    
    // Radio
    int32_t pr_stick_window;
    int32_t yaw_stick_window;
    int32_t los_delay;
    int32_t rc_rate_scale;
    int32_t tilt_expo;
    int32_t rc_lp_filter;
    int32_t tilt_s_curve;
    int32_t pan_s_curve;
    int32_t pan_expo;
    int32_t tilt_control_mode;  // Angle or rate
    int32_t roll_control_mode;
    int32_t pan_control_mode;
    int32_t reserved8[4];
    
    // GPS
    float maxhacc;
    int32_t min_sats;
    uint8_t relocate_enabled;
    int32_t reserved9[8];
    
    // Modes and features
    int32_t mode;
    float pan_majestic_curve;
    float pan_majestic_window;
    float pan_majestic_span;
    float tilt_majestic_curve;
    float tilt_majestic_window;
    float tilt_majestic_span;
    uint8_t acceleration_comp_enable;
    int32_t cat_distance;
    int32_t cat_speed;
    int32_t split_rate;
    int32_t application;
    int32_t reserved10[2];
    
    // Expert features
    int32_t shakey_pan;
    int32_t shakey_tilt;
    int32_t stiffy_setdown;
    int32_t gain_scheduling;
    int32_t roll_notch;
    int32_t tilt_notch;
    int32_t boot_mode;
    float autotune_backoff;
    int32_t jolt_rejection;
    int32_t reserved11[2];
    
    // Debug
    int32_t parameter[8];
    int32_t reserved12[4];
    
    
    // shutter things
    float sensor_width;
    float sensor_height;
    float zoom;
    float overlap;
    float shutter_period;
    float shutter_duration;
    int32_t shutter_mode;
    int32_t shutter_release;
    float timelapse_tilt_rate;
    float timelapse_pan_rate;
    float pointing_accuracy;
    int32_t aux_function;
    int32_t reserved13[6];
    
    // Logging setup
    uint32_t record;
    uint32_t log_rate_index;
    
    // Cal mode constants
    float cal_temp_step;
    int32_t cal_temp_points;
    
    // Targetting constants
    int32_t target_mode;        // position or position+height
    int32_t target_linger;      // timeout or lock forever
    float target_height_offset;     // height trim
} config_type;

typedef struct
{
    int32_t invert;
    int32_t joint_angle[3];
    int32_t fast_zero_yaw;
    int32_t stable;
} Gimbal_control_type;

extern Gimbal_control_type gimbal_control;

typedef struct
{
    float q[4];
    float q_delta[4];
    uint32_t new_reading;
    uint32_t timer;
    uint32_t interval;
    uint32_t last_time;
    float freq;
} remote_type;

extern uint32_t RWD_puck(uint32_t);

typedef struct
{
    uint8_t flashed[3];
    uint32_t serial_number;
    uint8_t hardware_rev;
    int32_t year;
    uint8_t month;
    uint8_t day;
    float gyro_scale[3];
    int16_t a_max[3];
    int16_t a_min[3];
    
    float gyro_yaw_induced_roll;
    float gyro_yaw_induced_pitch;
    float gyro_yaw_induced_roll_inv;
    float gyro_yaw_induced_pitch_inv;
    float reserved1[5];
    
    float gyro_ptat_comp[3];
    float gyro_ptat_ref;
    float accel_offset_tempco[3];
    float accel_scale_tempco[3];
    float accel_temp_ref;
} puck_type;

extern puck_type puck;

typedef struct
{
    uint32_t UIDL;
    uint32_t UIDM;
    uint32_t UIDH;
} UID_TypeDef;

typedef struct
{
    uint8_t flashed[3];
    uint32_t serial_number;
    uint32_t hardware_type;
    int32_t activate_year;
    uint8_t activate_month;
    uint8_t activate_day;
    int32_t expire_year;
    uint8_t expire_month;
    uint8_t expire_day;
    uint8_t rn41_mac[13];
    uint32_t baro_cal[8];
    //uint8_t accel_cal[8];
    float max_altitude;
    float max_range;
    int32_t licence_type;
    int32_t control_functionality;
    int32_t airframe_functionality;
    int32_t waypoint_functionality;
    int32_t camera_functionality;
} lic_type;

extern uint32_t RWD_lic(uint32_t);
extern void Make_licence(uint32_t);
extern uint32_t Check_licence_date(void);
extern uint32_t Obscurify(uint32_t);
extern uint32_t Unobscurify(uint32_t);

extern lic_type lic;



typedef struct {
    float ft_delta_puck[3];
    float ft_delta_gcu[3];
} gyrotest_type;

typedef struct
{
    int32_t calibration_select;
    int32_t calibration_start;
    int32_t calibration_progress;
    int32_t calibration_total_progress;
} Cal_control_type;

extern Cal_control_type cal_control;

typedef struct {
    uint8_t cal[8];
    int32_t raw[3];
    int32_t raw_tc_scale[3];
    int32_t raw_tc_total[3];
    float vec[3];
    float magnitude;
    float vec_offset[3];
    float g_offset;
    uint32_t new_data;
    float freq;
    float dt;
} accel_type;

typedef struct
{
    int32_t raw[3];
    float vec[3];
    float magnitude;
    float freq;
    float field;
    uint32_t new_data;
    uint32_t corruptions;
    uint32_t bad;
    uint32_t use_external;
    uint32_t start_cal;
    uint32_t heading_acquired;
    uint32_t sample_time;
} Compass_type;

extern config_type config;


extern void SendPacket(uint32_t attrib);


#endif
