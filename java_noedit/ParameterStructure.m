//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Volumes/SERVER/tmp/Java_src/ParameterStructure.java
//

#include "Comms.h"
#include "Dbg.h"
#include "IOSObjectArray.h"
#include "IOSPrimitiveArray.h"
#include "J2ObjC_source.h"
#include "ParameterStructure.h"
#include "java/lang/Double.h"
#include "java/lang/Math.h"
#include "java/util/ArrayList.h"

@interface ParameterStructure () {
 @public
  jboolean recieved_;
  jdouble value_;
  NSString *valueAsStr_;
  jint controlType_;
}

@end

J2OBJC_FIELD_SETTER(ParameterStructure, valueAsStr_, NSString *)

@implementation ParameterStructure

- (instancetype)init {
  ParameterStructure_init(self);
  return self;
}

- (instancetype)initWithFloat:(jfloat)value
                      withInt:(jint)id_
                      withInt:(jint)messlen
                      withInt:(jint)os
                      withInt:(jint)size
                  withBoolean:(jboolean)su
                    withFloat:(jfloat)scale_
                 withNSString:(NSString *)fs
                 withNSString:(NSString *)units
                      withInt:(jint)partype
                      withInt:(jint)ctrl
                   withDouble:(jdouble)meta1
                   withDouble:(jdouble)meta2
                 withNSString:(NSString *)meta {
  ParameterStructure_initWithFloat_withInt_withInt_withInt_withInt_withBoolean_withFloat_withNSString_withNSString_withInt_withInt_withDouble_withDouble_withNSString_(self, value, id_, messlen, os, size, su, scale_, fs, units, partype, ctrl, meta1, meta2, meta);
  return self;
}

- (instancetype)initWithFloat:(jfloat)value
                      withInt:(jint)id_
                      withInt:(jint)messlen
                      withInt:(jint)os
                      withInt:(jint)size
                  withBoolean:(jboolean)su
                    withFloat:(jfloat)scale_
                 withNSString:(NSString *)fs
                 withNSString:(NSString *)units
                      withInt:(jint)partype {
  ParameterStructure_initWithFloat_withInt_withInt_withInt_withInt_withBoolean_withFloat_withNSString_withNSString_withInt_(self, value, id_, messlen, os, size, su, scale_, fs, units, partype);
  return self;
}

- (jdouble)getValue {
  return value_;
}

- (jdouble)packetValue {
  return (jdouble) ([self getValue] * scale__);
}

//yesedit
- (uint8_t)getAttribNum {
    return id__;
}


- (NSString *)getFormattedValue {
  return (valueAsStr_ == nil) ? @"" : valueAsStr_;
}

- (void)getLicenseValueWithJavaUtilArrayList:(JavaUtilArrayList *)al
                                     withInt:(jint)attrib {
  if (attrib != id__) return;
  if (valueAsStr_ != nil) [((JavaUtilArrayList *) nil_chk(al)) addWithId:JreStrcat("$$$", key_, @": ", valueAsStr_)];
}

- (NSString *)calculateFormatedValueWithDouble:(jdouble)value {
  if (selectionOptions_ == nil) {
    return NSString_formatWithNSString_withNSObjectArray_(fs_, [IOSObjectArray newArrayWithObjects:(id[]){ JavaLangDouble_valueOfWithDouble_(value) } count:1 type:NSObject_class_()]);
  }
  else {
    jint index = (jint) JavaLangMath_roundWithDouble_(value + 1);
    if (index >= selectionOptions_->size_ || index < 0) {
      Dbg_logWithNSString_(JreStrcat("$$C", @"selectionOptions error [", key_, ']'));
      return @"";
    }
    else return IOSObjectArray_Get(selectionOptions_, index);
  }
}

- (jboolean)setValueWithDouble:(jdouble)value {
  if (!(recieved_)) return NO;
  self->value_ = value;
  valueAsStr_ = [self calculateFormatedValueWithDouble:value];
  return YES;
}

- (jdouble)setDataWithByteArray:(IOSByteArray *)msg {
  if (IOSByteArray_Get(nil_chk(msg), 0) != self->id__) return 0;
  recieved_ = YES;
  if (scale__ == -1) {
    NSString *sep = ([((NSString *) nil_chk(key_)) isEqual:@"MAC"]) ? @":" : @"";
    if (os_ <= msg->size_ - size_) {
      valueAsStr_ = @"";
      for (jint i = 1; i <= size_; i++) {
        (void) JreStrAppendStrong(&valueAsStr_, "C", (jchar) (IOSByteArray_Get(msg, os_ + i - 1)));
        if ((i < size_) && ((i + 1) % 2 == 1)) (void) JreStrAppendStrong(&valueAsStr_, "$", sep);
      }
    }
    return 0;
  }
  jint byteoffset = os_;
  switch (size_) {
    case 1:
    if (byteoffset <= msg->size_) {
      if (su_) {
        value_ = IOSByteArray_Get(msg, byteoffset) / scale__;
      }
      else {
        value_ = Comms_usByteWithByte_(IOSByteArray_Get(msg, byteoffset)) / scale__;
      }
    }
    else value_ = 0;
    break;
    case 2:
    if (byteoffset <= msg->size_ - 2) {
      value_ = JavaLangMath_roundWithFloat_(((JreLShift32((IOSByteArray_Get(msg, byteoffset)), 8)) | Comms_usByteWithByte_(IOSByteArray_Get(msg, byteoffset + 1)))) / scale__;
    }
    else value_ = 0;
    break;
    case 4:
    if (byteoffset <= msg->size_ - 4) {
      if (su_) {
        value_ = JavaLangMath_roundWithFloat_(((JreLShift32((IOSByteArray_Get(msg, byteoffset)), 24)) | (JreLShift32(Comms_usByteWithByte_((IOSByteArray_Get(msg, byteoffset + 1))), 16))) | ((JreLShift32(Comms_usByteWithByte_((IOSByteArray_Get(msg, byteoffset + 2))), 8)) | Comms_usByteWithByte_(IOSByteArray_Get(msg, byteoffset + 3)))) / scale__;
      }
      else {
        value_ = Comms_usByteWithByte_(IOSByteArray_Get(msg, byteoffset + 3));
        JrePlusAssignDoubleD(&value_, (jdouble) Comms_usByteWithByte_(IOSByteArray_Get(msg, byteoffset + 2)) * 256);
        JrePlusAssignDoubleD(&value_, (jdouble) Comms_usByteWithByte_(IOSByteArray_Get(msg, byteoffset + 1)) * 65536);
        JrePlusAssignDoubleD(&value_, (jdouble) Comms_usByteWithByte_(IOSByteArray_Get(msg, byteoffset + 0)) * 16777216);
        JreDivideAssignDoubleD(&value_, scale__);
      }
    }
    else {
      value_ = 0;
    }
    break;
  }
  [self setValueWithDouble:value_];
  return value_;
}

- (jboolean)isSlider {
  return controlType_ == ParameterStructure_SLIDER;
}

- (jboolean)isJog {
  return controlType_ == ParameterStructure_JOG;
}

- (jboolean)isSwitch {
  return controlType_ == ParameterStructure_SWITCH;
}

- (jboolean)isDropdown {
  return controlType_ == ParameterStructure_DROP_DOWN;
}

- (jboolean)isJogWithStops {
  return controlType_ == ParameterStructure_JOG_STOP;
}

- (jboolean)isStart {
  return controlType_ == ParameterStructure_START;
}

- (jboolean)isNone {
  return controlType_ == ParameterStructure_NONE;
}

- (JavaLangDouble *)minVal {
  if (selectionOptions_ != nil) return JavaLangDouble_valueOfWithDouble_([((NSString *) nil_chk(IOSObjectArray_Get(selectionOptions_, 0))) equalsIgnoreCase:@""] ? 0 : [JavaLangDouble_valueOfWithDouble_(-1) doubleValue]);
  return minValue_;
}

- (JavaLangDouble *)maxVal {
  if (selectionOptions_ != nil) return JavaLangDouble_valueOfWithDouble_(selectionOptions_->size_ - 2);
  return maxValue_;
}

- (IOSObjectArray *)getSelectionOptions {
  return selectionOptions_;
}

+ (const J2ObjcClassInfo *)__metadata {
  static const J2ObjcMethodInfo methods[] = {
    { "init", "ParameterStructure", NULL, 0x1, NULL, NULL },
    { "initWithFloat:withInt:withInt:withInt:withInt:withBoolean:withFloat:withNSString:withNSString:withInt:withInt:withDouble:withDouble:withNSString:", "ParameterStructure", NULL, 0x1, NULL, NULL },
    { "initWithFloat:withInt:withInt:withInt:withInt:withBoolean:withFloat:withNSString:withNSString:withInt:", "ParameterStructure", NULL, 0x1, NULL, NULL },
    { "getValue", NULL, "D", 0x1, NULL, NULL },
    { "packetValue", NULL, "D", 0x1, NULL, NULL },
    { "getFormattedValue", NULL, "Ljava.lang.String;", 0x1, NULL, NULL },
    { "getLicenseValueWithJavaUtilArrayList:withInt:", "getLicenseValue", "V", 0x1, NULL, NULL },
    { "calculateFormatedValueWithDouble:", "calculateFormatedValue", "Ljava.lang.String;", 0x1, NULL, NULL },
    { "setValueWithDouble:", "setValue", "Z", 0x1, NULL, NULL },
    { "setDataWithByteArray:", "setData", "D", 0x1, NULL, NULL },
    { "isSlider", NULL, "Z", 0x1, NULL, NULL },
    { "isJog", NULL, "Z", 0x1, NULL, NULL },
    { "isSwitch", NULL, "Z", 0x1, NULL, NULL },
    { "isDropdown", NULL, "Z", 0x1, NULL, NULL },
    { "isJogWithStops", NULL, "Z", 0x1, NULL, NULL },
    { "isStart", NULL, "Z", 0x1, NULL, NULL },
    { "isNone", NULL, "Z", 0x1, NULL, NULL },
    { "minVal", NULL, "Ljava.lang.Double;", 0x1, NULL, NULL },
    { "maxVal", NULL, "Ljava.lang.Double;", 0x1, NULL, NULL },
    { "getSelectionOptions", NULL, "[Ljava.lang.String;", 0x1, NULL, NULL },
  };
  static const J2ObjcFieldInfo fields[] = {
    { "JOG", "JOG", 0x19, "I", NULL, NULL, .constantValue.asInt = ParameterStructure_JOG },
    { "SWITCH", "SWITCH", 0x19, "I", NULL, NULL, .constantValue.asInt = ParameterStructure_SWITCH },
    { "SLIDER", "SLIDER", 0x19, "I", NULL, NULL, .constantValue.asInt = ParameterStructure_SLIDER },
    { "DROP_DOWN", "DROP_DOWN", 0x19, "I", NULL, NULL, .constantValue.asInt = ParameterStructure_DROP_DOWN },
    { "JOG_STOP", "JOG_STOP", 0x19, "I", NULL, NULL, .constantValue.asInt = ParameterStructure_JOG_STOP },
    { "START", "START", 0x19, "I", NULL, NULL, .constantValue.asInt = ParameterStructure_START },
    { "NONE", "NONE", 0x19, "I", NULL, NULL, .constantValue.asInt = ParameterStructure_NONE },
    { "recieved_", NULL, 0x2, "Z", NULL, NULL, .constantValue.asLong = 0 },
    { "units_", NULL, 0x1, "Ljava.lang.String;", NULL, NULL, .constantValue.asLong = 0 },
    { "key_", NULL, 0x1, "Ljava.lang.String;", NULL, NULL, .constantValue.asLong = 0 },
    { "partype_", NULL, 0x4, "I", NULL, NULL, .constantValue.asLong = 0 },
    { "selectionOptions_", NULL, 0x4, "[Ljava.lang.String;", NULL, NULL, .constantValue.asLong = 0 },
    { "id__", "id", 0x4, "B", NULL, NULL, .constantValue.asLong = 0 },
    { "messlen_", NULL, 0x4, "I", NULL, NULL, .constantValue.asLong = 0 },
    { "os_", NULL, 0x4, "I", NULL, NULL, .constantValue.asLong = 0 },
    { "size_", NULL, 0x4, "I", NULL, NULL, .constantValue.asLong = 0 },
    { "su_", NULL, 0x4, "Z", NULL, NULL, .constantValue.asLong = 0 },
    { "scale__", "scale", 0x4, "D", NULL, NULL, .constantValue.asLong = 0 },
    { "fs_", NULL, 0x4, "Ljava.lang.String;", NULL, NULL, .constantValue.asLong = 0 },
    { "value_", NULL, 0x2, "D", NULL, NULL, .constantValue.asLong = 0 },
    { "valueAsStr_", NULL, 0x2, "Ljava.lang.String;", NULL, NULL, .constantValue.asLong = 0 },
    { "controlType_", NULL, 0x2, "I", NULL, NULL, .constantValue.asLong = 0 },
    { "maxValue_", NULL, 0x4, "Ljava.lang.Double;", NULL, NULL, .constantValue.asLong = 0 },
    { "minValue_", NULL, 0x4, "Ljava.lang.Double;", NULL, NULL, .constantValue.asLong = 0 },
  };
  static const J2ObjcClassInfo _ParameterStructure = { 2, "ParameterStructure", NULL, NULL, 0x1, 20, methods, 24, fields, 0, NULL, 0, NULL, NULL, NULL };
  return &_ParameterStructure;
}

@end

void ParameterStructure_init(ParameterStructure *self) {
  (void) NSObject_init(self);
  self->recieved_ = NO;
  self->valueAsStr_ = nil;
  self->controlType_ = 0;
  self->maxValue_ = nil;
  self->minValue_ = nil;
}

ParameterStructure *new_ParameterStructure_init() {
  ParameterStructure *self = [ParameterStructure alloc];
  ParameterStructure_init(self);
  return self;
}

void ParameterStructure_initWithFloat_withInt_withInt_withInt_withInt_withBoolean_withFloat_withNSString_withNSString_withInt_withInt_withDouble_withDouble_withNSString_(ParameterStructure *self, jfloat value, jint id_, jint messlen, jint os, jint size, jboolean su, jfloat scale_, NSString *fs, NSString *units, jint partype, jint ctrl, jdouble meta1, jdouble meta2, NSString *meta) {
  (void) ParameterStructure_initWithFloat_withInt_withInt_withInt_withInt_withBoolean_withFloat_withNSString_withNSString_withInt_(self, value, id_, messlen, os, size, su, scale_, fs, units, partype);
  self->controlType_ = ctrl;
  self->minValue_ = JavaLangDouble_valueOfWithDouble_(meta1);
  self->maxValue_ = JavaLangDouble_valueOfWithDouble_(meta2);
}

ParameterStructure *new_ParameterStructure_initWithFloat_withInt_withInt_withInt_withInt_withBoolean_withFloat_withNSString_withNSString_withInt_withInt_withDouble_withDouble_withNSString_(jfloat value, jint id_, jint messlen, jint os, jint size, jboolean su, jfloat scale_, NSString *fs, NSString *units, jint partype, jint ctrl, jdouble meta1, jdouble meta2, NSString *meta) {
  ParameterStructure *self = [ParameterStructure alloc];
  ParameterStructure_initWithFloat_withInt_withInt_withInt_withInt_withBoolean_withFloat_withNSString_withNSString_withInt_withInt_withDouble_withDouble_withNSString_(self, value, id_, messlen, os, size, su, scale_, fs, units, partype, ctrl, meta1, meta2, meta);
  return self;
}

void ParameterStructure_initWithFloat_withInt_withInt_withInt_withInt_withBoolean_withFloat_withNSString_withNSString_withInt_(ParameterStructure *self, jfloat value, jint id_, jint messlen, jint os, jint size, jboolean su, jfloat scale_, NSString *fs, NSString *units, jint partype) {
  (void) NSObject_init(self);
  self->recieved_ = NO;
  self->valueAsStr_ = nil;
  self->controlType_ = 0;
  self->maxValue_ = nil;
  self->minValue_ = nil;
  self->value_ = value;
  self->id__ = (jbyte) id_;
  self->messlen_ = messlen;
  self->os_ = os;
  self->size_ = size;
  self->su_ = su;
  self->scale__ = scale_;
  self->fs_ = fs;
  self->units_ = units;
  self->partype_ = partype;
}

ParameterStructure *new_ParameterStructure_initWithFloat_withInt_withInt_withInt_withInt_withBoolean_withFloat_withNSString_withNSString_withInt_(jfloat value, jint id_, jint messlen, jint os, jint size, jboolean su, jfloat scale_, NSString *fs, NSString *units, jint partype) {
  ParameterStructure *self = [ParameterStructure alloc];
  ParameterStructure_initWithFloat_withInt_withInt_withInt_withInt_withBoolean_withFloat_withNSString_withNSString_withInt_(self, value, id_, messlen, os, size, su, scale_, fs, units, partype);
  return self;
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ParameterStructure)
