//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Volumes/SERVER/tmp/Java_src/Firmware7_1.java
//

#ifndef _Firmware7_1_H_
#define _Firmware7_1_H_

#include "Globals.h"
#include "J2ObjC_header.h"

@interface Firmware7_1 : NSObject < Globals_Firmware >

#pragma mark Public

- (instancetype)init;

- (void)populateChartsetsAndParametersWithGlobals:(Globals *)g;

@end

J2OBJC_EMPTY_STATIC_INIT(Firmware7_1)

FOUNDATION_EXPORT void Firmware7_1_init(Firmware7_1 *self);

FOUNDATION_EXPORT Firmware7_1 *new_Firmware7_1_init() NS_RETURNS_RETAINED;

J2OBJC_TYPE_LITERAL_HEADER(Firmware7_1)

#endif // _Firmware7_1_H_
