/*-----------------------------------------------------------------
    Copyright by Freefly Systems Inc 2015

    Contains proprietary algorithms and source code
    Copyright by SM Webb and JG Ellison 2010-2012

    Filename: "rc4.c"
-----------------------------------------------------------------*/

// standard headers
#include <stdint.h>
#include <string.h>
// project headers
#include "rc4.h"
//#include "uart_user.h"

void Swap(uint8_t *, uint8_t *);
void Init_RC4(void);
void DoKSA(uint8_t *, int32_t);
uint8_t GetPRGAOutput(void);
void EnDecode(uint8_t *, uint32_t, uint8_t *);

uint8_t s[256];
int32_t rc4_i;
int32_t rc4_j;
uint8_t codebook[256];

void swap(uint8_t *s1, uint8_t *s2)
{
    uint8_t temp = *s1;
    
    *s1 = *s2;
    *s2 = temp;
}

void Init_RC4(void)
{
    uint32_t i;
    
    for(i = 0 ; i < 256; i++) s[i] = i;
    rc4_i = rc4_j = 0;
}

void DoKSA(uint8_t *key, int32_t key_len)
{
    for(rc4_i = 0; rc4_i < 256; rc4_i++)
    {
        rc4_j = (rc4_j + (int32_t)s[rc4_i] + (int32_t)key[rc4_i % key_len]) % 256;
        swap(&s[rc4_i], &s[rc4_j]);
    }
    rc4_i = rc4_j = 0;
}

uint8_t GetPRGAOutput(void)
{
    rc4_i = (rc4_i + 1) % 256;
    rc4_j = (rc4_j + (int32_t)s[rc4_i]) % 256;
    swap(&s[rc4_i], &s[rc4_j]);
    return s[((int32_t)s[rc4_i] + (int32_t)s[rc4_j]) % 256];
}

void EnDecode(uint8_t *string, uint32_t length, uint8_t *key)
{
    uint32_t i;
    
    Init_RC4();
    DoKSA(key, strlen((char *)key));
    for(i = 0; i < length; i++)
    {
        *string ^= GetPRGAOutput();
        string++;
    }
}

void Prime_PRBS(uint8_t *key)
{
    Init_RC4();
    DoKSA(key, strlen((char *)key));
}

void Copy_codebook()
{
    uint32_t i;
    for(i = 0; i < 256; i++) codebook[i] = s[i];            // store the codebook now it has been generated
}

// use codebook to code/decode n bytes of protocol
// first 2 bytes are rand1 and rand2 used to init
void Endecode_protocol(uint8_t * p, uint32_t n)
{
    uint8_t i, j;
    uint8_t k = 8;                                // we start the index at byte 8 since this is the first one coded
    uint8_t index;
    
    i = *p++;                                            // rand1
    j = *p++;                                            // rand2
    
    n -= 2;                                                // ignore first 2 bytes
    while(n--)
    {
        index = (uint8_t)k + (uint8_t)i;    // modulus arithmetic
        index ^= j;
        *p ^= codebook[index];                            // EXOR the message with the codebook lookup
        k++;
        p++;
    }
}
