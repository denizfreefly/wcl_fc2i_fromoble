

@class IOSByteArray;

#import "JreEmulation.h"
#import "ConnectionInterface.h"
#import "java/lang/Thread.h"

@interface Wifi : NSObject < NSStreamDelegate, UIAlertViewDelegate, ConnectionInterface > {
        NSInputStream *InputStream;
        NSOutputStream *OutputStream;
}

- (id)init;
- (int)getMaxMTU;
- (void)sendConfigurationPacketWithByteArray:(IOSByteArray *)packet;
- (BOOL)sendMessageToDeviceWithByteArray:(IOSByteArray *)msg;
- (BOOL)sendMessageToDeviceWithByteArray:(IOSByteArray *)msg withInt:(int)len;
- (void)resetConnection;
- (void)resetConnectionWithId:(id)newDevice;
- (void)stopThread;

@end

