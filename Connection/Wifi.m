//
//  Manage the Wifi connection

#import "Wifi.h"

@implementation Wifi

IOSByteArray *configPacket;
BOOL CTS;

- (id)init
{
    self = [super init];
    if (self) [self privateConnect];
    configPacket = nil;
    NSLog(@"Starting wifi");
    return self;
}

// No thread, just disconnect
- (void)stopThread {
    [self finishConnection];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(privateConnect) object:nil];
}

#if 0
#pragma mark -
#pragma mark Methods required by synchronized Android code
#endif

// ConnecitonInterface - largest packets supported by this connection 4092, 2044, 224, 120
- (int)getMaxMTU {
    return 4092;
}

// ConnecitonInterface - set ssid or config message
- (void)sendConfigurationPacketWithByteArray:(IOSByteArray *)packet {
    configPacket = packet;
    [self resetConnection];
}


// ConnecitonInterface - sends message to the connected device
- (BOOL)sendMessageToDeviceWithByteArray:(IOSByteArray *)msg {
    int buffLen = (int) msg.count;
    return [self sendMessageToDeviceWithByteArray:msg withInt:buffLen];
}

- (BOOL)sendMessageToDeviceWithByteArray:(IOSByteArray *)msg withInt:(int)buffLen {
    
    if(CTS) {
        NSData *data = [msg toNSData];
        long nob = [OutputStream write:[data bytes] maxLength:buffLen];
        if(nob != buffLen) {
            NSLog(@"Failed to send. Returned=%li, Buffer length=%lu", nob, (unsigned long)[data length]);
            [self resetConnectionWithId:nil];
        }
    } else {
        NSLog(@"NOT_CTS");
    }
    return true;
}


- (void)resetConnection {
    [self resetConnectionWithId:nil];
}


// Interface Wifi - resets connection
- (void)resetConnectionWithId:(id)ignored {
    
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread: @selector(resetConnectionWithId:) withObject: nil waitUntilDone:true  ];
        return;
    }
    
    //    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    [self finishConnection];
    [self performSelector:@selector(privateConnect) withObject:nil afterDelay:2.0];//Keep short for ssid reset
    //    }];
}

// iOS version has no thread, but requires disconnect
- (void)finishConnection {
    NSLog(@"--- DISCONNECT ---");
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        CTS= false;
        
        if (InputStream) {
            CFReadStreamSetProperty((__bridge CFReadStreamRef)(InputStream), kCFStreamPropertyShouldCloseNativeSocket, kCFBooleanTrue);
            //Close and reset inputstream
            [InputStream setDelegate:nil];
            [InputStream close];
            [InputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            InputStream = nil;
        }
        if (OutputStream) {
            CFReadStreamSetProperty((__bridge CFReadStreamRef)(OutputStream), kCFStreamPropertyShouldCloseNativeSocket, kCFBooleanTrue);
            //Close and reset outputstream
            [OutputStream setDelegate:nil];
            [OutputStream close];
            [OutputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            OutputStream = nil;
        }
        
        [[S comms] processConnectionLostWithInt:0];
    }];
    
    
}

#if 0
#pragma mark -
#pragma mark WIFI related
#endif

// Connect
- (void) privateConnect {
    
    NSLog(@"--- CONNECT ---");
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread: @selector(privateConnect) withObject: nil waitUntilDone:true  ];
        return;
    }
    
    // check correct connection
    NSString *hs = [WCUi getCurrentWifiHotSpotName ];
    
    NSLog(@" conneciton =[%@] ",hs);
    
    if(!hs)    NSLog(@" no hs ");
    
    if(![hs contains:Globals_SSID_PREFIX_ ])    NSLog(@" no prifix ");
    
    if(!hs || ![hs contains:Globals_SSID_PREFIX_ ] ) {
        [WCUi showAlertWithMessage:[S getStringWithInt:R_string_connection_lost] andTitle:[S getStringWithInt:R_string_connection_wifi_fail]];
        //      [self performSelector:@selector(privateConnect) withObject:nil afterDelay: CON_RETRY_DELAY ];
        return;
    }
    
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    
    int port = (configPacket) ? 4321 : 8080;
    
    NSLog(@"connecting on port %i", port );
    
    CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, ( CFStringRef)@"192.168.1.1", port, &readStream, &writeStream);
    
    if(readStream && writeStream)
    {
        //Setup inpustream
        InputStream = (__bridge_transfer NSInputStream *)readStream;
        [InputStream setDelegate:self];
        [InputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [InputStream open];
        
        //Setup outputstream
        OutputStream = (__bridge_transfer NSOutputStream *)writeStream;
        [OutputStream setDelegate:self];
        [OutputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [OutputStream open];
    }
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)StreamEvent
{
    
    switch (StreamEvent)
    {
        case NSStreamEventOpenCompleted:
            NSLog(@"TCP Client - Stream opened");
            
            if (!configPacket) [[S comms] processConnectionMade];
            break;
            
        case NSStreamEventHasBytesAvailable:
            if (theStream == InputStream)
            {
                uint8_t buffer[1024];
                long len;
                while ([InputStream hasBytesAvailable])
                {
                    len = [InputStream read:buffer maxLength:sizeof(buffer)];
                    if (len > 0)
                    {
                        IOSByteArray * iba = [IOSByteArray arrayWithBytes:( char *) buffer count: len ];
                        [[S comms]  receiveDataWithInt: (int) [iba count] withByteArray: ( IOSByteArray *) iba];
                    }
                }
            }
            break;
            
        case NSStreamEventErrorOccurred:
            NSLog(@"TCP Client - Can't connect to the host - disconnect and try again");
            [self resetConnectionWithId: nil];
            break;
            
        case NSStreamEventEndEncountered:
            NSLog(@"TCP Client - End encountered - another device has superseded the connection");
            [self finishConnection];
            [WCUi showExitDialogWithMessage: [S getStringWithInt:R_string_connection_superseded] andTitle: [S getStringWithInt:R_string_connection_lost]];
            
            break;
            
        case NSStreamEventNone:
            NSLog(@"TCP Client - None event");
            break;
            
        case NSStreamEventHasSpaceAvailable:
            CTS=true;
            
            if (configPacket) {
                NSLog(@"Resetting ssid!");
                [self sendMessageToDeviceWithByteArray:configPacket];
                configPacket = nil;
                [self resetConnection];
            }
            
            break;
            
        default:
            NSLog(@"TCP Client - Unknown event");
    }
}






@end
