/*-----------------------------------------------------------------
    Copyright by Freefly Systems Inc 2015
		
    Contains proprietary algorithms and source code
    Copyright by SM Webb and JG Ellison 2010-2012
		
    Filename: "QBX_Parsing_Functions.c"
-----------------------------------------------------------------*/

//****************************************************************************
// Headers
//****************************************************************************
#include "QBX_Parsing_Functions.h"
#include <stdlib.h>		// for Standard Data Types
#include <stdint.h>		// for Standard Data Types
#include <math.h>

//****************************************************************************
// Private Global Vars
//****************************************************************************
uint8_t *msgPtr;
uint8_t rw;

//****************************************************************************
// Private Defines
//****************************************************************************

// Parser Macros
#define ADDSL(v) { *msgPtr++ = (uint8_t)(v >> 24); *msgPtr++ = (uint8_t)(v >> 16); *msgPtr++ = (uint8_t)(v >> 8); *msgPtr++ = (uint8_t)(v); }
#define ADDSS(v) { *msgPtr++ = (uint8_t)(v >> 8); *msgPtr++ = (uint8_t)(v); }
#define ADDCHAR(v) { *msgPtr++ = (uint8_t)(v); }

#define GETUC ((uint8_t)*msgPtr++)
#define GETSC ((int8_t)*msgPtr++)
#define GETSS ((int16_t)((*msgPtr++) << 8) | (int16_t)((*msgPtr++)))
#define GETSL ((int32_t)((*msgPtr++) << 24) | (int32_t)((*msgPtr++) << 16) | (int32_t)((*msgPtr++) << 8) | (int32_t)((*msgPtr++)))

//****************************************************************************
// Public Function Definitions
//****************************************************************************

// Set the Parser Message Pointer. 
// This function is used prior to the first parsing function calls, then the parsing functions increment the pointer as needed.
void QBX_Parser_SetMessagePointer(uint8_t *p){
	msgPtr = p;
}

// Gets the Parser Pointer
uint8_t *QBX_Parser_GetMessagePointer(void){
	return msgPtr;
}

// Sets the Parser to Read
void QBX_Parser_SetDir_Read(void){
	rw = READ;
}

// Sets the Parser to Delta Write
void QBX_Parser_SetDir_WriteRel(void){
	rw = WRITEDELTA;
}

// Sets the Parser to Absolute Write
void QBX_Parser_SetDir_WriteAbs(void){
	rw = WRITEABS;
}

//****************************************************************************
// Private Function Definitions
//****************************************************************************

void AddFloatAsSignedLong(float *v, uint32_t n, float scaleto)
{
    while(n--)
    {
        int32_t value = (int32_t)(((*v) * scaleto) + 0.5f * ((0 < *v) - (*v < 0)));
        ADDSL(value);
        v++;
    }
}

void AddFloatAsSignedShort(float *v, uint32_t n, float scaleto)
{
    while(n--)
    {
        int16_t value = (int16_t)(((*v) * scaleto) + 0.5f * ((0 < *v) - (*v < 0)));
        ADDSS(value);
        v++;
    }
}

void AddFloatAsSignedChar(float *v, uint32_t n, float scaleto)
{
    while(n--)
    {
        int8_t value = (int8_t)(((*v) * scaleto) + 0.5f * ((0 < *v) - (*v < 0)));
        ADDCHAR(value);
        v++;
    }
}

void AddFloatAsUnsignedChar(float *v, uint32_t n, float scaleto)
{
    while(n--)
    {
        uint8_t value = (uint8_t) (((*v) * scaleto) + 0.5f * ((0 < *v) - (*v < 0)));
        ADDCHAR(value);
        v++;
    }
}

void GetFloatAsSignedLong(float *v, uint32_t n, float max, float min, float scalefrom)
{
    if(rw == WRITEDELTA)
    {
        while(n--)
        {
            float r = (float)GETSL;
            r = r * scalefrom;
            *v += r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            if(isnan(*v)) *v = 0.0f;
            v++;
        }
    }
    else
    {
        while(n--)
        {
            float r = (float)GETSL;
            r = r * scalefrom;
            *v = r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            if(isnan(*v)) *v = 0.0f;
            v++;
        }
    }
}

void GetFloatAsSignedShort(float *v, uint32_t n, float max, float min, float scalefrom)
{
    if(rw == WRITEDELTA)
    {
        while(n--)
        {
            float r = (float)GETSS;
            r = r * scalefrom;
            *v += r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            if(isnan(*v)) *v = 0.0f;
            v++;
        }
    }
    else
    {
        while(n--)
        {
            float r = (float)GETSS;
            r = r * scalefrom;
            *v = r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            if(isnan(*v)) *v = 0.0f;
            v++;
        }
    }
}

void GetFloatAsSignedChar(float *v, uint32_t n, float max, float min, float scalefrom)
{
    if(rw == WRITEDELTA)
    {
        while(n--)
        {
            float r = (float)GETSC;
            r = r * scalefrom;
            *v += r;
            if(max < *v) *v = max;
            else if(*v < min) *v = min;
            if(isnan(*v)) *v = 0.0f;
            v++;
        }
    }
    else
    {
        while(n--)
        {
            float r = (float)GETSC;
            r = r * scalefrom;
            *v = r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            if(isnan(*v)) *v = 0.0f;
            v++;
        }
    }
}

void GetFloatAsUnsignedChar(float *v, uint32_t n, float max, float min, float scalefrom)
{
    if(rw == WRITEDELTA)
    {
        while(n--)
        {
            float r = (float)GETSC;
            r = r * scalefrom;
            *v += (float)r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            if(isnan(*v)) *v = 0.0f;
            v++;
        }
    }
    else
    {
        while(n--)
        {
            float r = (float)GETUC;
            r = r * scalefrom;
            *v = (float)r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            if(isnan(*v)) *v = 0.0f;
            v++;
        }
    }
}

void AddSignedLongAsSignedLong(int32_t *v, uint32_t n)
{
    while(n--)
    {
        int32_t value = *v;
        ADDSL(value);
        v++;
    }
}

void AddSignedLongAsSignedShort(int32_t *v, uint32_t n)
{
    while(n--)
    {
        int16_t value = (int16_t)(*v);
        ADDSS(value);
        v++;
    }
}

void AddSignedLongAsSignedChar(int32_t *v, uint32_t n)
{
    while(n--)
    {
        int8_t value = (int8_t)(*v);
        ADDCHAR(value);
        v++;
    }
}

void AddSignedLongAsUnsignedChar(int32_t *v, uint32_t n)
{
    while(n--)
    {
        uint8_t value = (uint8_t)(*v);
        ADDCHAR(value);
        v++;
    }
}

void GetSignedLongAsSignedLong(int32_t *v, uint32_t n, int32_t max, int32_t min)
{
    if(rw == WRITEDELTA)
    {
        while(n--)
        {
            int32_t r = GETSL;
            *v += r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            v++;
        }
    }
    else
    {
        while(n--)
        {
            int32_t r = GETSL;
            *v = r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            v++;
        }
    }
}

void GetSignedLongAsSignedShort(int32_t *v, uint32_t n, int32_t max, int32_t min)
{
    if(rw == WRITEDELTA)
    {
        while(n--)
        {
            int32_t r = (int32_t)GETSS;
            *v += r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            v++;
        }
    }
    else
    {
        while(n--)
        {
            int32_t r = (int32_t)GETSS;
            *v = r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            v++;
        }
    }
}

void GetSignedLongAsSignedChar(int32_t *v, uint32_t n, int32_t max, int32_t min)
{
    if(rw == WRITEDELTA)
    {
        while(n--)
        {
            int32_t r = (int32_t)GETSC;
            *v += r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            v++;
        }
    }
    else
    {
        while(n--)
        {
            int32_t r = (int32_t)GETSC;
            *v = r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            v++;
        }
    }
}

void GetSignedLongAsUnsignedChar(int32_t *v, uint32_t n, int32_t max, int32_t min)
{
    if(rw == WRITEDELTA)
    {
        while(n--)
        {
            int32_t r = (int32_t)GETSC;    // Force int8_t if relative change
            *v += r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            v++;
        }
    }
    else
    {
        while(n--)
        {
            int32_t r = (int32_t)GETUC;
            *v = r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            v++;
        }
    }
}

void AddSignedShortAsSignedShort(int16_t *v, uint32_t n)
{
    while(n--)
    {
        int16_t value = *v;
        ADDSS(value);
        v++;
    }
}

void AddSignedShortAsSignedChar(int16_t *v, uint32_t n)
{
    while(n--)
    {
        int8_t value = (int8_t)(*v);
        ADDCHAR(value);
        v++;
    }
}

void AddSignedShortAsUnsignedChar(int16_t *v, uint32_t n)
{
    while(n--)
    {
        uint8_t value = (uint8_t)(*v);
        ADDCHAR(value);
        v++;
    }
}

void GetSignedShortAsSignedShort(int16_t *v, uint32_t n, float max, float min)
{
    if(rw == WRITEDELTA)
    {
        while(n--)
        {
            int16_t r = GETSS;
            *v += r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            v++;
        }
    }
    else
    {
        while(n--)
        {
            int16_t r = GETSS;
            *v = r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            v++;
        }
    }
}

void GetSignedShortAsSignedChar(int16_t *v, uint32_t n, float max, float min)
{
    if(rw == WRITEDELTA)
    {
        while(n--)
        {
            int16_t r = (int16_t)GETSC;
            *v += (int16_t)r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            v++;
        }
    }
    else
    {
        while(n--)
        {
            int16_t r = (int16_t)GETSC;
            *v = (int16_t)r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            v++;
        }
    }
}

void GetSignedShortAsUnsignedChar(int16_t *v, uint32_t n, int16_t max, int16_t min)
{
    if(rw == WRITEDELTA)
    {
        while(n--)
        {
            int16_t r = (int16_t)GETSC;
            *v += r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            v++;
        }
    }
    else
    {
        while(n--)
        {
            int16_t r = (int16_t)GETUC;
            *v = r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            v++;
        }
    }
}

void AddSignedCharAsSignedChar(int8_t *v, uint32_t n)
{
    while(n--)
    {
        ADDCHAR(*v);
        v++;
    }
}

void GetSignedCharAsSignedChar(int8_t *v, uint32_t n, int8_t max, int8_t min)
{
    if(rw == WRITEDELTA)
    {
        while(n--)
        {
            int8_t r = GETSC;
            int32_t temp = *v;    // Promote to signed so we can do a meaningful clamp check
            temp += r;
            if(max < temp) temp = max;
            if(temp < min) temp = min;
            *v = (uint8_t)temp;
            v++;
        }
    }
    else
    {
        while(n--)
        {
            int8_t r = GETSC;
            *v = r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            v++;
        }
    }
}

void AddUnsignedCharAsUnsignedChar(uint8_t *v, uint32_t n)
{
    while(n--)
    {
        ADDCHAR(*v);
        v++;
    }
}

void GetUnsignedCharAsUnsignedChar(uint8_t *v, uint32_t n, uint8_t max, uint8_t min)
{
    if(rw == WRITEDELTA)
    {
        while(n--)
        {
            int8_t r = GETSC;
            int32_t temp = *v;    //Promote to signed so we can do a meaningful clamp check
            temp += r;
            if(max < temp) temp = max;
            if(temp < min) temp = min;
            *v = (uint8_t)temp;
            v++;
        }
    }
    else
    {
        while(n--)
        {
            uint8_t r = GETUC;
            *v = r;
            if(max < *v) *v = max;
            if(*v < min) *v = min;
            v++;
        }
    }
}

