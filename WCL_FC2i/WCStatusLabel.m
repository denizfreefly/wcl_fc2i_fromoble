//
//  WCStatusLabel.m
//  WCL_FC2i
//
//  Created by Lorne Kelly on
//  Copyright (c) 2014 Lorne Kelly. All rights reserved.
//

@implementation WCStatusLabel

const float SS_FONT_SIZE = 11;

/** Override - Get a sized and initialized subview  */
- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        // Create a label and add as subView
        [self setSubLabel: [[UILabel alloc] initWithFrame: self.bounds ]];
        [_subLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:_subLabel];
        // Set rounded corners
        self.layer.cornerRadius = 3;
        self.layer.masksToBounds = YES;
    }
    return self;
}

// Show progress bar data as a status label (smallscreen only)
- (void)updateAsProgressBarWithIndex:(int) itemId {
    
    ProgressBarStructure *ps = [[S globals] getProgressBarWithInt:itemId];
    
    if (ps) {
        [self  setHidden: false];
        [_subLabel setTextAlignment:NSTextAlignmentLeft ];
        [_subLabel setFont:[UIFont systemFontOfSize:SS_FONT_SIZE]];
        if ([[S globals] isLoggedOn]) {
            _gradientColors = [WCUi createColorArrayWithId:ps.getColor];
            _subLabel.text = [NSString stringWithFormat:@"%@: %@" ,ps.getTitle, ps.getFormattedValue];
        } else {
            _gradientColors = [WCUi createColorArrayWithId:R_drawable_status_box_grey];
            _subLabel.text = [NSString stringWithFormat:@"%@: %@" ,ps.getTitle, [S getStringWithInt:R_string_generic_default] ];
        }
        
    } else {
        [self setHidden: true];
    }
    [self setNeedsDisplay];
}


- (void)updateBoot {
    
    MachineFailStructure *fs = [[S globals] getMachineFail];
    
    _gradientColors = [WCUi createColorArrayWithId:fs.getColor];
    if ([WCUi smallScreen]) {
        _subLabel.text = [NSString stringWithFormat:@"BOOT: %@" , fs.getFormattedValue];
        [_subLabel setFont:[UIFont systemFontOfSize:SS_FONT_SIZE]];
    }else {
        _subLabel.text = [NSString stringWithFormat:@"%@" , fs.getFormattedValue];
    }
    [_subLabel setTextAlignment:NSTextAlignmentLeft ];
    [self setNeedsDisplay];
}

/** Public - Update the Label text, View color, and visibility according to info from globals */
- (void)updateWithIndex:(int) index {
    
    IndicatorStructure *is = [[S globals] getStatusIndicatorWithInt:index];
    
    if (is) {
        [self  setHidden: false];
        _gradientColors = [WCUi createColorArrayWithId:is.getColor];
        if ([WCUi smallScreen]) {
            _subLabel.text = [NSString stringWithFormat:@"%@: %@",is.getTitle , is.getFormattedValue];
            [_subLabel setTextAlignment:NSTextAlignmentLeft ];
            [_subLabel setFont:[UIFont systemFontOfSize:SS_FONT_SIZE]];
        }else{
            _subLabel.text = [NSString stringWithFormat:@"%@\n%@",is.getTitle , is.getFormattedValue];
            [_subLabel setTextAlignment:NSTextAlignmentCenter ];
            [_subLabel setNumberOfLines:2];
        }
    } else {
        [self setHidden: true];
    }
    [self setNeedsDisplay];
}


/** Draw the background as a gradient defined in updateStatus... call   */
- (void)drawRect:(CGRect)rect
{
    // Set up the gradient info
    CGFloat locations[4] = {0.0 ,0.15f, 0.85f, 1.0};
    
    // Create it
    CGColorSpaceRef colorSpc = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpc, (__bridge CFArrayRef)(_gradientColors), locations);
    // Apply it
    CGContextRef ref = UIGraphicsGetCurrentContext();
    CGContextDrawLinearGradient(ref, gradient, CGPointMake(0.5, 0.0), CGPointMake(0.5, CGRectGetMaxY(self.bounds)), kCGGradientDrawsAfterEndLocation);
    // Clean up
    CGColorSpaceRelease(colorSpc);
    CGGradientRelease(gradient);
}

@end