//
//  
//  WCL_FC2i
//
//  Created by Lorne Kelly on 14-02-19.
//  Copyright (c) 2014 Lorne Kelly. All rights reserved.
//

#import <UIKit/UIKit.h>


@class WCAboutPopup;

@interface WCAboutPopup : WCPopup

@property (strong) IBOutlet UILabel *versionUILabel;
@property (strong) IBOutlet UILabel *poweredUILabel;
@property (strong) IBOutlet UILabel *variantUILabel;
@property (strong) IBOutlet WCTableView *licenseUITable;
@property (strong) IBOutlet UILabel *titleLable;

- (IBAction)doneButton:(id)sender;

@end
