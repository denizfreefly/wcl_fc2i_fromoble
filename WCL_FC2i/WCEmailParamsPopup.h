//
//  FlipsidePopup.h
//  WCL_FC2i
//
//  Created by Lorne Kelly on 14-02-19.
//  Copyright (c) 2014 Lorne Kelly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@class WCEmailParamsPopup;

@interface WCEmailParamsPopup : WCPopup <UITableViewDelegate, WCEditDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) id <WCPopupDelegate> delegate;
@property (weak) IBOutlet WCTableView *filesUITable;

- (IBAction)doneButton:(id)sender;
- (IBAction)manageButton:(id)sender; 

@end
