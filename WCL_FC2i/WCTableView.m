//  TableViewDataSource is attached to tables and provided with the data to show in the table

#import "WCTableView.h"

// Heights for small screen (iPhone)
const int SS_ROW_HEIGHT = 30;
const int SS_FONT_HEIGHT = 15;

UIView *progressView;

@implementation WCTableView

/** Initiates a redraw of cell at _progressIndex */
//- (void) showProgress {

//    if (![NSThread isMainThread]) {
//        [self performSelectorOnMainThread: @selector(showProgress) withObject: nil waitUntilDone:true  ];
//        return;
//    }
//    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//        if (_progressIndex) {
//            NSLog(@"showProgress - note this reload may cause firmware update issues?  needs more testing");
//            [self reloadRowsAtIndexPaths: @[_progressIndex]  withRowAnimation: UITableViewRowAnimationNone];
//        } else {
//            [self reloadData];
//        }
//    }];
//}

/** Override - Set rowheight on init  */
- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder]) && [WCUi smallScreen]) [self setRowHeight:SS_ROW_HEIGHT];
    return self;
}

/** Override - return the table cell */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *) indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        // Create a progressview to fit this cell - can be displayed with cell.accessoryView
        UIActivityIndicatorView *progress = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        progressView = [[UIView alloc] initWithFrame:CGRectMake(0,0,20,20)];
        [progressView addSubview: progress];
        if(cell.accessoryView) progressView.frame = cell.accessoryView.bounds;
        progress.center = progressView.center;
        [progress startAnimating];
    }
    
    if ([WCUi smallScreen]) cell.textLabel.font=[UIFont systemFontOfSize:SS_FONT_HEIGHT];
    
    if (_colorData) cell.textLabel.textColor =  [_colorData objectAtIndex:indexPath.row];
    
    // Set generic text or display progress
//    if (!(_progressIndex) || [S globals]->slfProgress_ == 0) {
        cell.textLabel.text = [_tableData objectAtIndex: indexPath.row ];
//    } else {
//        // Calculate the text
//        float p = 100.0f * ((float)[S globals]->slfProgress_ / (float)[S globals]->slfProgressMax_);
//        cell.textLabel.text = [NSString stringWithFormat:@"%@ (%1.1f%%)",  [_tableData objectAtIndex: _progressIndex.row ],p ];
//        // Show the progress indicator
//        cell.accessoryView = progressView;
//    }

    return cell;
}

/** Override - Return the number of rows in the section. */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tableData.count;
}

/** Override - Return title if any */
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return _tableTitle;
}

/** Override - Pass the edit (delete) event back to the registered delegate. */
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_editDelegate && editingStyle == UITableViewCellEditingStyleDelete) {
        NSString *name = [_tableData objectAtIndex: indexPath.row ];
        [_editDelegate editDeleteRequestWithName: name];
    }
}

@end
