//
//  Firmware update dialog

#import "WCFirmwareFlashPopup.h"

@implementation WCFirmwareFlashPopup

static NSString *const EXT_BIN = @"bin";
int bootStrapMode;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) self.preferredContentSize = CGSizeMake(480.0,640.0);
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"sys viewDidLoad");
    
    _filesUITable.tableData = (IOSObjectArray *) [WCUi getFilesWithExt: EXT_BIN];
    [_filesUITable setDelegate: self];
    [_filesUITable setDataSource:_filesUITable];
    [_filesUITable setEditDelegate: self];
}

/** Interface TableViewDelegate - UI call when user selects a cell */
- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath {
    
    // read the file
    NSString * fn = [_filesUITable.tableData objectAtIndex: indexPath.row];
    NSData *data = [NSData dataWithContentsOfURL: [WCUi getNSURLfor:fn withExt:EXT_BIN]];
    if (!data) return;
    // initialize and fill buffer
     [S globals]->slfFlashBuffer_ = [IOSByteArray arrayWithLength:[data length]];
    unsigned char *bytePtr = (unsigned char *)[data bytes];
    for (int i = 0; i < [data length]; i++) {
        [[S globals]->slfFlashBuffer_  replaceByteAtIndex:i withByte: (char) *(bytePtr+i) ];
    }
    
    // Set bootstrap mode. Show bootstrap warning or save dialog
    NSString *msg;
    if ([S globals]->logonState_ != Globals_LOGGEDON) {
        bootStrapMode = Globals_SLF_FLASH_BOOTSTRAP;
        msg = [S getStringWithInt:R_string_firmware_bootstrap];
    } else {
        bootStrapMode = Globals_SLF_FLASH;
        msg = [NSString stringWithFormat: NSLocalizedStringFromTable (@"slf_ready_to_flash", @"R", nil) , fn ];
    }
    [[[UIAlertView alloc] initWithTitle:[S getStringWithInt:R_string_app_name]
                               message:msg
                              delegate:self
                     cancelButtonTitle:[S getStringWithInt:R_string_generic_no]
                     otherButtonTitles:[S getStringWithInt:R_string_generic_yes], nil] show];
    // Set up table progress indicator
//TODO     [_filesUITable setProgressIndex:indexPath];
}

#if 0
#pragma mark -
#pragma mark Delegates
#endif

/** Alert Delegate - start flash if YES */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex) {
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        [S globals]->slfOperation_ = bootStrapMode;
        [self performSelectorInBackground:@selector (waitLoopBackgroundThread) withObject:nil];
    } else {
//TODO        [_filesUITable setProgressIndex:nil];
    }
}

/** Interface WCEditDelegate - Remove file & redisplay list */
- (void) editDeleteRequestWithName: name {
    NSURL *delURL = [WCUi getNSURLfor:name withExt:EXT_BIN];
    [[NSFileManager defaultManager] removeItemAtURL:delURL error:nil];
    _filesUITable.tableData = (IOSObjectArray *) [WCUi getFilesWithExt: EXT_BIN];
    [_filesUITable reloadData];
    NSLog(@"Recieved callback with %@", name);
}

#if 0
#pragma mark -
#pragma mark Actions
#endif

/** Button */
- (IBAction)doneButton:(id)sender
{
    [self.delegate popupDidFinish:self];
}

/** Button - Turn delete mode on and off*/
- (IBAction)manageButton:(id)sender {
    [_filesUITable setEditing: !(_filesUITable.editing)];
}

#if 0
#pragma mark -
#pragma mark Flash
#endif

/** Wait in background for process to finish */
- (void) waitLoopBackgroundThread {
    while ([S globals]->slfOperation_ != Globals_SLF_IDLE) {
        [self performSelectorOnMainThread:@selector (updateProgressMainThread) withObject:nil waitUntilDone:YES];
        [NSThread sleepForTimeInterval:0.25];
    }
    [self performSelectorOnMainThread:@selector (finishWithMessageMainThread) withObject:nil waitUntilDone:NO];
}

/** Update the progress bar */
- (void)updateProgressMainThread {
//TODO    [_filesUITable showProgress];
}

/** Clean up, show message, and exit */
- (void)finishWithMessageMainThread {
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    int result = [((JavaLangInteger *) [S globals]->slfResultRID_) intValue];
    [WCUi showAlertWithMessage: [S getStringWithInt:result] andTitle:nil];
    [self.delegate popupDidFinish:self];
}





@end
