

#import "WCMainViewController.h"

@implementation WCMainViewController

id systemSender;


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    // Set logo image
    UIImage *image = [UIImage imageNamed:@"icon_40x40.png"];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleBordered target:nil action:nil];
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
    // Set segmented tab control
    [_segmentedTabs addTarget:self action:@selector(segmentedTabClick:) forControlEvents:UIControlEventValueChanged];
    
    // Load & register tab viewControllers, set current tab
    if ([WCUi smallScreen])
        _configViewController = [[WCConfigViewController alloc] initWithNibName:@"WCConfigViewController_iPhone" bundle:nil];
    else
        _configViewController = [[WCConfigViewController alloc] initWithNibName:@"WCConfigViewController_iPad" bundle:nil];
    _configViewController.view.frame = _containerView.bounds;
    [_containerView addSubview:_configViewController.view];
    [_configViewController lateInit];
    
    _termViewController = [[WCTerminalViewController alloc] initWithNibName:@"WCTerminalViewController" bundle:nil];
    _termViewController.view.frame = _containerView.bounds;
    [_containerView addSubview:_termViewController.view];
    
    if ([WCUi smallScreen])
        _chartViewController = [[WCChartViewController alloc] initWithNibName:@"WCChartViewController_iPhone" bundle:nil];
    else
        _chartViewController = [[WCChartViewController alloc] initWithNibName:@"WCChartViewController_iPad" bundle:nil];
    _chartViewController.view.frame = _containerView.bounds;
    [_containerView addSubview:_chartViewController.view];
    [_chartViewController lateInit];
    
    // Select the chart
    _segmentedTabs.selectedSegmentIndex = 2;
    [self segmentedTabClick: _segmentedTabs];
    
    // Update UI
    [self onGuiUpdateStatus];
}

/** segmentedTabs selector defined with addTarget: in viewDidLoad
 CAUTION: using raw index */
-(void) segmentedTabClick:(id)sender{
    
    // Add selected ViewController
    switch ([_segmentedTabs selectedSegmentIndex])
    {
        case 0:
            [_configViewController.view setHidden:false];
            [_termViewController.view setHidden:true];
            [_chartViewController.view setHidden:true];
            break;
            
        case 1:
            [_configViewController.view setHidden:true];
            [_termViewController.view setHidden:false];
            [_chartViewController.view setHidden:true];
            break;
            
        case 2:
            [_configViewController.view setHidden:true];
            [_termViewController.view setHidden:true];
            [_chartViewController.view setHidden:false];
            [[S comms] setupStream];
            break;
    }
}


#if 0
#pragma mark -
#pragma mark Interfaces S_mainUiCallBack implementation
#endif

/** Interface S_mainUiCallBack - draw  */
- (void) onGuiUpdateStatus {
    
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread: @selector(onGuiUpdateStatus) withObject: nil waitUntilDone:true  ];
        return;
    }
    
    // Update progress bars
    if ([WCUi smallScreen]) {
        [_progressSsLable0 updateAsProgressBarWithIndex:0];
        [_progressSsLable1 updateAsProgressBarWithIndex:1];
        [_progressSsLabel2 updateAsProgressBarWithIndex:2];
    } else {
        [self updateProgressBarWith:0 andUIProgress: _progressBar0 andUILabel:_progressBarTitle0 andUILabel: _progressBarValue0];
        [self updateProgressBarWith:1 andUIProgress: _progressBar1 andUILabel:_progressBarTitle1 andUILabel: _progressBarValue1];
        [self updateProgressBarWith:2 andUIProgress: _progressBar2 andUILabel:_progressBarTitle2 andUILabel: _progressBarValue2];
    }
    
    // Update status indicators on left
    [_statusLabel0 updateWithIndex:0];
    [_statusLabel1 updateWithIndex:1];
    [_statusLabel2 updateWithIndex:2];
    [_statusLabel3 updateWithIndex:3];
    [_statusLabel4 updateWithIndex:4];
    [_statusLabel5 updateWithIndex:5];
    [_statusLabel6 updateWithIndex:6];
    [_statusLabel7 updateWithIndex:7];
    [_statusLabel8 updateWithIndex:8];
    [_statusLabel9 updateWithIndex:9];
    [_statusLabel10 updateWithIndex:10];
    
    // Update boot
    [_bootUILabel updateBoot];
}


/** Interface S_mainUiCallBack -  Display message & update menu buttons if status changed */
- (void)onConnectionStatusChangedWithInt:(int)msg {
    [self performSelectorOnMainThread: @selector(ocscMainThreadWithInt:) withObject: [NSNumber numberWithInt:msg] waitUntilDone:true  ];
}

- (void)ocscMainThreadWithInt:(NSNumber*) NSmsg {
    NSLog( @"MAIN onConnectionStatusChanged");
    
    // Note: most "toast" connection messages excluded in iOS version
    if([NSmsg intValue] == R_string_connection_no_hardware) {
        [WCUi showExitDialogWithMessage: [S getStringWithInt:[NSmsg intValue]] andTitle: nil];
    }
    
    [_chartViewController tableView: nil didSelectRowAtIndexPath:nil];
    [_configViewController tableView: nil didSelectRowAtIndexPath:nil];
    [_termViewController onDisplayTextWithChar:' '];
    [self onDisplayBootIndication];
    
}

/** Interface S_mainUiCallBack - Update Boot indicator ONLY  */
- (void)onDisplayBootIndication {
    
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread: @selector(onDisplayBootIndication) withObject: nil waitUntilDone:true  ];
        return;
    }
    
    [_bootUILabel updateBoot];
}

/** Private - CPU/tery/Temp Adjust the passed progress bar and label with data from globals */
- (void)updateProgressBarWith:(int) index andUIProgress: (UIProgressView *) progress andUILabel: (UILabel*) title andUILabel: (UILabel*) value {
    
    ProgressBarStructure *ps = [[S globals] getProgressBarWithInt:index];
    
    if (ps) {
        [[progress superview]  setHidden:false];
        // Set text
        [title setText: ps.getTitle];
        [value setText: ps.getFormattedValue];
        // Set bar value
        if (ps.getBarValue == 0)
            progress.progress = 0;
        else
            progress.progress =   ((float) ps.getBarValue / (float) ps.getMaxBarValue);
        // Set bar color
        UIColor *tintColor = [WCUi createProgressColorWithId:ps.getColor];
        progress.progressTintColor = tintColor;
    } else {
        [[progress superview]  setHidden:true];
    }
}



#if 0
#pragma mark -
#pragma mark Popup View Controllers
#endif

- (IBAction)infoButton:(id)sender
{
    //[self showPopup: [[WCInfoPopup alloc] initWithNibName:@"WCInfoPopup" bundle:nil]  withButton:sender];
    
    
//    [[S persist] setConnectionTypeBluetooth]
//    [[S bluetooth] openSerialPortProfile] ;
//    [S connection];
    S_connection();
//    NSLog(@"Not Implemented");
    
}

- (IBAction)aboutButton:(id)sender
{
    [self showPopup: [[WCAboutPopup alloc] initWithNibName:@"WCAboutPopup" bundle:nil]  withButton:sender];
}


/** Private - Show a WCPopup (ViewController ext) with animations for pad or phone */
-(void)showPopup:( WCPopup *) controller withButton:(id) sender {
    controller.delegate = self;
    if ([WCUi smallScreen]) {
        controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        if (!(self.popupController) || !([self.popupController isPopoverVisible]) ) {
            self.popupController = [[UIPopoverController alloc] initWithContentViewController:controller];
        }
        if ([self.popupController isPopoverVisible]) {
            [self.popupController dismissPopoverAnimated:YES];
        } else {
            [self.popupController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }
}

/** WCPopupDelegate - Hide the popup NOTE: popup may hide without calling here */
- (void)popupDidFinish:(WCPopup *)controller
{
    if ([WCUi smallScreen]) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.popupController dismissPopoverAnimated:YES];
    }
}

#if 0
#pragma mark -
#pragma mark System Menu
#endif

/** Button - Displays an action sheet with "Flash Device" options */
- (IBAction)systemButton:(id)sender {
    
    systemSender = sender;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"SYSTEM"
                                  delegate:self
                                  cancelButtonTitle:[S getStringWithInt:R_string_generic_cancel]
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:nil];
    
    
    // Add all items from the dynamic menu in Globals
    for (NSString * __strong s in nil_chk([((JavaUtilLinkedHashMap *) nil_chk(((Globals *) nil_chk([S globals]))->menuArray_)) values])) {
        // Hide bootstrap - not possible with dongle
        if(![self selectedBtn:s matchesRid: R_id_menu_bootstrap_controller]  &&
            ![self selectedBtn:s matchesRid: R_id_menu_bootstrap_dongle]) {
            [actionSheet addButtonWithTitle: s ];
        }
    }
    // Add iOS only "Email settings" item
     if([S globals].isLoggedOn) [actionSheet addButtonWithTitle:  NSLocalizedStringFromTable (@"menu_email_settings", @"R", nil) ];
    
    // Add ssid menu item if connected
    if([S globals].isLoggedOn) [actionSheet addButtonWithTitle: [S getStringWithInt: R_string_menu_set_wifi_ssid] ];
    
    // Modify color
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [[UIView appearance] setTintColor:[UIColor darkGrayColor]];
    
    // Now display the actionSheet
    [actionSheet showInView:self.view];
}



/** Interface UIActionSheetDelegate - Respond to actions sheet selection (send Flash command)*/
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // Ignore Cancel button
    if( buttonIndex == -1) return;
    // iOS8 can no longer launce a popup from Actionsheet, so delay for Actionsheet to close
    [[UIView appearance] setTintColor:[WCUi appCyan]];
    [self performSelector:@selector(assMenu:) withObject:[actionSheet buttonTitleAtIndex:buttonIndex] afterDelay:0.01];
}
-(void) assMenu: (NSString*) sel {
    
    if ([self selectedBtn:sel matchesRid: R_id_menu_bootstrap_controller]) {
//        [S globals]->slfCommand_ = Globals_FLASH_BOOTSTRAP_CONTROLLER;
        [S globals]->slfCommand_ = Globals_FLASH_BOOTSTRAP_SD_CONTROLLER;
        if ([self checkConWithMsg])
            [self showPopup: [[WCFirmwareFlashPopup alloc] initWithNibName:@"WCFirmwareFlashPopup" bundle:nil]  withButton: systemSender];
        [S configFragOnStateChange];
    }
    if ([self selectedBtn:sel matchesRid: R_id_menu_bootstrap_dongle]) {
        [S globals]->slfCommand_ = Globals_FLASH_BOOTSTRAP_SD_DONGLE;
//        [S globals]->slfCommand_ = Globals_FLASH_BOOTSTRAP_DONGLE;
        if ([self checkConWithMsg])
            [self showPopup: [[WCFirmwareFlashPopup alloc] initWithNibName:@"WCFirmwareFlashPopup" bundle:nil]  withButton: systemSender];
        [S configFragOnStateChange];
    }
    if ([self selectedBtn:sel matchesRid: R_id_menu_write_config]) {
        [self flashDialogWithCmd: Globals_FLASH_WRITE_CONFIG andMessage: [S getStringWithInt:R_string_menu_write_config] ];
    }
    if ([self selectedBtn:sel matchesRid: R_id_menu_read_config]) {
        [self flashDialogWithCmd: Globals_FLASH_READ_CONFIG andMessage: [S getStringWithInt:R_string_menu_read_config] ];
    }
    if ([self selectedBtn:sel matchesRid: R_id_menu_default_config]) {
        [self flashDialogWithCmd: Globals_FLASH_DEFAULT_CONFIG andMessage: [S getStringWithInt:R_string_menu_default_config] ];
    }
    if ([self selectedBtn:sel matchesRid: R_id_menu_write_controller_cal]) {// not impl
        [self flashDialogWithCmd: Globals_FLASH_WRITE_CONTROLLER_CAL andMessage: [S getStringWithInt:R_string_menu_write_controller_cal] ];
    }
    if ([self selectedBtn:sel matchesRid: R_id_menu_read_controller_factorycal]) {
        [self flashDialogWithCmd: Globals_FLASH_READ_CONTROLLER_FACTORYCAL andMessage: [S getStringWithInt:R_string_menu_read_controller_factorycal] ];
    }
    if ([self selectedBtn:sel matchesRid: R_id_menu_autotune]) {
        if ([self checkLogStateWithMsg]) {
            [self showPopup: [[WCAutotunePopup alloc] initWithNibName:@"WCAutotunePopup" bundle:nil]  withButton: systemSender];
        }
    }
    if ([self selectedBtn:sel matchesRid: R_id_menu_compass_cal]) {
        if ([self checkLogStateWithMsg]) {
            [self showPopup: [[WCCompassCalPopup alloc] initWithNibName:@"WCCompassCalPopup" bundle:nil]  withButton: systemSender];
        }
    }
    if ([self selectedBtn:sel matchesRid: R_id_menu_reset_controller]) {
        [self flashDialogWithCmd: Globals_FLASH_RESET_CONTROLLER andMessage: [S getStringWithInt:R_string_menu_reset_controller] ];
    }
    if ([self selectedBtn:sel matchesRid: R_id_menu_reset_dongle]) {
        [self flashDialogWithCmd: Globals_FLASH_RESET_DONGLE andMessage: [S getStringWithInt:R_string_menu_reset_dongle] ];
    }
    if ([self selectedBtn:sel matchesRid: R_id_menu_bind_spektrum]) {
        [self flashDialogWithCmd: Globals_FLASH_SPEKTRUM_BIND andMessage: [S getStringWithInt:R_string_menu_bind_spektrum] ];
    }
    if ([self selectedBtn:sel matchesRid: R_id_menu_save_config]) {
        if ([self checkLogStateWithMsg]) {
            [self showPopup: [[WCSaveParamsPopup alloc] initWithNibName:@"WCSaveParamsPopup" bundle:nil]  withButton: systemSender];
            [S configFragOnStateChange];
        }
    }
    if ([self selectedBtn:sel matchesRid: R_id_menu_load_config]) {
        if ([self checkLogStateWithMsg]) {
            [self showPopup: [[WCLoadParamsPopup alloc] initWithNibName:@"WCLoadParamsPopup" bundle:nil]  withButton: systemSender];
            [S configFragOnStateChange];
        }
    }
    // Non-android menu items
    if ([sel isEqualToString: [S getStringWithInt: R_string_menu_set_wifi_ssid] ]) {
        if ([self checkLogStateWithMsg]) {
            [self showPopup: [[WCssidPopup alloc] initWithNibName:@"WCssidPopup" bundle:nil]  withButton: systemSender];
        }
    }
    if ([sel isEqualToString: NSLocalizedStringFromTable (@"menu_email_settings", @"R", nil) ]) {
        [self showPopup: [[WCEmailParamsPopup alloc] initWithNibName:@"WCEmailParamsPopup" bundle:nil]  withButton: systemSender];
    }
    
}
-(bool) selectedBtn: (NSString*) sel matchesRid: (int) Rid {
    NSString *R = [[S globals] getMenuNameWithInt: Rid];
    if([R isEqualToString:sel]) [[S globals] setInfoContextWithInt:Rid];
    return [R isEqualToString:sel];
}

/** Return true if logged on, otherwise display a message */
-(bool) checkLogStateWithMsg {
    if(![S globals].isLoggedOn ) {
        [WCUi showAlertWithMessage:[S getStringWithInt:R_string_main_no_connection] andTitle:nil];
        return false;
    }
    return true;
}


/** Return true if connected, otherwise display a message */
-(bool) checkConWithMsg {
    if(!([S globals]->connected_)) {
        [WCUi showAlertWithMessage:[S getStringWithInt:R_string_main_no_connection] andTitle:nil];
        return false;
    }
    return true;
}


/** Display a dialog before sending flash command */
int commandRequest;
-(void) flashDialogWithCmd: (int) command andMessage: (NSString*) msg
{
    commandRequest = command;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[S getStringWithInt:R_string_app_name]
                                                    message:msg
                                                   delegate:self
                                          cancelButtonTitle:[S getStringWithInt:R_string_generic_no]
                                          otherButtonTitles:[S getStringWithInt:R_string_generic_yes], nil];
    
    if([self checkLogStateWithMsg]) [alert show];
}

/** Send flash command (dialog delegate) */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(!buttonIndex) return;
    
    // Send the command
    NSLog(@"Flashing, command=%i ", commandRequest);
    IOSByteArray *msg = [IOSByteArray arrayWithBytes:(char[]){ Globals_CMD_FLASH + (char) 128, commandRequest } count:2];
    [[S comms] sendPacketTermWithByteArray:msg];
    [S configFragOnStateChange];
    
    // Show closing message & reboot etc (iOS has no Toast, so ignoring most messages)
    switch (commandRequest) {
            
//        case Globals_FLASH_SPEKTRUM_BIND:
//            [[S connection] resetConnectionWithId:nil];
//            [self performSelector:@selector(delayExitMessage) withObject:nil afterDelay: 0.2 ];
//            break;
    }
}

- (void) delayExitMessage {
   [WCUi showExitDialogWithMessage:[S getStringWithInt:R_string_menu_bind_spektrum_reboot] andTitle:nil];
}
@end
