

#import "WCChartViewChart.h"
#import "Charts.h"
#import "TraceStructure.h"

@implementation WCChartViewChart

float textHeight;
float textSpacingY;
float xCol1;
float xCol2;


/** Override NSView - redraw the view */
- (void)drawRect:(CGRect) r
{
    // Do local layout calculations
    xCol1 = 5;
    xCol2 = r.size.width /1.7;
    textHeight = [WCUi smallScreen] ? 12 : 15;
    textSpacingY = [WCUi smallScreen] ? 13 : 18;
    
    // Do the calculations for the layout
    [[S charts] setScalingWithFloat:r.size.height withFloat: r.size.width];
    
    // Get data for upper and lower charts
    IOSObjectArray * tsUpper = [[S charts] getTraceListSplitUpper];
    IOSObjectArray * tsLower = [[S charts] getTraceListSplitLower];
    
    // If there is no upper chart there will be no lower either
    if (tsUpper == nil) return;
    
    // Display a single chart
    if (tsLower == nil) {
        [self drawGraticuleOnRect: r forTrace:  [tsUpper objectAtIndex:0] ];
        [self drawChartOnRect: r withTraces:tsUpper];
    }
    
    // Display a split chart scale
    else {
        TraceStructure *ts = (TraceStructure *) [tsUpper objectAtIndex:0];
        
        [self drawGraticuleOnRect: r forTrace:  ts ];
        [self drawChartOnRect: r withTraces:tsUpper];
        
        [[UIColor grayColor] setStroke];
        [WCUi lineOnRect: r X1: ts->chartLeft_ + 20  Y1: ts->chartBottom_ + TraceStructure_Y_MARGIN X2:ts->chartRight_ - 20 Y2: ts->chartBottom_ + TraceStructure_Y_MARGIN];
        
        [self drawGraticuleOnRect: r forTrace:  [tsLower objectAtIndex:0] ];
        [self drawChartOnRect: r withTraces:tsLower];
    }
    
    // add radio info if appropriate
    if ( [[[S charts] getSelectedChartSetName] isEqualToString: @"Radio"] ) {

        TraceStructure *ts = (TraceStructure *) [tsUpper objectAtIndex:0];
        float yStart = ts->chartTop_;
        float yCursor = yStart + textHeight;
        
        NSString *msg;
        NSString *yes = [S getStringWithInt: R_string_generic_yes ];
        NSString *no = [S getStringWithInt: R_string_generic_no];
        
        msg = [NSString stringWithFormat:@"A %@: %@" , [S getStringWithInt:R_string_chart_details_spektrum_badframes], [S globals]->spektrumBadframesA_ ];
        [self drawTextLeft: r message: msg X: xCol2 Y: yCursor Color: [UIColor blackColor] ];
        yCursor += textSpacingY ;
        msg = [NSString stringWithFormat:@"A %@: %@", [S getStringWithInt:R_string_chart_details_spektrum_frequency], [S globals]->spektrumFrequencyA_];
        [self drawTextLeft: r message: msg X: xCol2 Y: yCursor Color: [UIColor blackColor] ];
        yCursor += textSpacingY ;
        msg = [NSString stringWithFormat:@"A %@: %@" , [S getStringWithInt:R_string_chart_details_spektrum_dropouts], [S globals]->spektrumDropoutsA_];
        [self drawTextLeft: r message: msg X: xCol2 Y: yCursor Color: [UIColor blackColor] ];
        yCursor += textSpacingY ;
        msg = [NSString stringWithFormat:@"A %@: %@" , [S getStringWithInt:R_string_chart_details_spektrum_los], [S globals]->spektrumLosA_];
        [self drawTextLeft: r message: msg X: xCol2 Y: yCursor Color: [UIColor blackColor] ];
        yCursor += textSpacingY ;
        msg = [NSString stringWithFormat:@"A %@: %@" , [S getStringWithInt:R_string_chart_details_spektrum_active], ([S globals]->spektrumActiveA_) ? yes : no];
        [self drawTextLeft: r message: msg X: xCol2 Y: yCursor Color: [UIColor blackColor] ];
        yCursor += textSpacingY ;
        msg = [NSString stringWithFormat:@"A %@: %@" , [S getStringWithInt:R_string_chart_details_spektrum_badframes], [S globals]->spektrumBadframesB_];
        [self drawTextLeft: r message: msg X: xCol2 Y: yCursor Color: [UIColor blackColor] ];
        yCursor += textSpacingY ;
        msg = [NSString stringWithFormat:@"A %@: %@" , [S getStringWithInt:R_string_chart_details_spektrum_frequency], [S globals]->spektrumFrequencyB_];
        [self drawTextLeft: r message: msg X: xCol2 Y: yCursor Color: [UIColor blackColor] ];
        yCursor += textSpacingY ;
        msg = [NSString stringWithFormat:@"A %@: %@" , [S getStringWithInt:R_string_chart_details_spektrum_dropouts], [S globals]->spektrumDropoutsB_];
        [self drawTextLeft: r message: msg X: xCol2 Y: yCursor Color: [UIColor blackColor] ];
        yCursor += textSpacingY ;
        msg = [NSString stringWithFormat:@"A %@: %@" , [S getStringWithInt:R_string_chart_details_spektrum_los],[S globals]->spektrumLosB_];
        [self drawTextLeft: r message: msg X: xCol2 Y: yCursor Color: [UIColor blackColor]  ];
        yCursor += textSpacingY ;
        msg = [NSString stringWithFormat:@"A %@: %@" , [S getStringWithInt:R_string_chart_details_spektrum_active], ([S globals]->spektrumActiveB_) ? yes : no];
        [self drawTextLeft: r message: msg X: xCol2 Y: yCursor Color: [UIColor blackColor] ];
        yCursor += textSpacingY ;
    }
}

/** Private - Draws the graticule.  Location based on the tpassed TraceStructure */
-(void) drawGraticuleOnRect: (CGRect) r forTrace: (TraceStructure*) ts {
    [[UIColor lightGrayColor] setStroke];
    for ( int i = 0; i <= TraceStructure_DIVS ; i++) {
        [WCUi lineOnRect: r X1: ts->chartLeft_ Y1: [ts getGraticuleYWithInt: i] X2:ts->chartRight_ Y2: [ts getGraticuleYWithInt:i]];
        [WCUi lineOnRect: r X1: [ts getGraticuleXWithInt: i] Y1: ts->chartTop_  X2: [ts getGraticuleXWithInt: i] Y2: ts->chartBottom_ ];
    }
//    if (Dbg_D) [self drawTextLeft:r message:@"DEBUG MODE" X: (ts->chartRight_ / 2) Y: (ts->chartBottom_ /2 ) Color:[UIColor redColor] ];
}


/** Private - Draws a set of traces whith the passed traceList */
-(void) drawChartOnRect: (CGRect) r withTraces: (IOSObjectArray*) tsList {
    
    TraceStructure *ts = (TraceStructure *) [tsList objectAtIndex:0];
    float yStart = ts->chartTop_;
    float yCursor = yStart + textHeight;
    
    for (int x = 0; x < [tsList count]; x++) {
        ts = (TraceStructure *) [tsList objectAtIndex:x];
        // draw details text
        NSString *msg = [NSString stringWithFormat:@"%@: %@", [ts getLabel], [ts getValueWithUnits]];
        [self drawTextLeft: r message: msg X: xCol1 Y: yCursor Color: [WCUi getColorForTrace:ts] ];
        yCursor += textSpacingY ;
        // draw trace
        if (ts->isEnabled_) {
            [[WCUi getColorForTrace:ts] setStroke];
            float xPos = ts->chartRight_;
            [ts resetForDraw];
            for (int i=1; i <= [S charts]->maxPoints_ ; i++ ) {
                [WCUi lineOnRect: r X1: xPos Y1: [ts getY] X2: xPos - ts->xStep_ Y2: [ts getYafterDecrement]];
                xPos -= ts->xStep_;
            }
        }
    }
}

/** Private - Draw left aligned text with the color and cooridnates specified */
- (void) drawTextLeft: (CGRect) r message: (NSString*) str X: (float) x Y: (float) y Color: (UIColor*) uiColor {
    CGRect strFrame = { { x, y - textHeight }, { x + 300, y  } };
    NSMutableParagraphStyle *textStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    textStyle.alignment = NSTextAlignmentLeft;
    UIFont *textFont = [UIFont systemFontOfSize:textHeight];
    [str drawInRect:strFrame withAttributes:@{NSFontAttributeName:textFont, NSParagraphStyleAttributeName:textStyle, NSForegroundColorAttributeName: uiColor }];

}

@end
