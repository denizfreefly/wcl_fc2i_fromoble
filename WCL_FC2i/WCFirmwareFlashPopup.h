//
//  FlipsidePopup.h
//  WCL_FC2i
//
//  Created by Lorne Kelly on 14-02-19.
//  Copyright (c) 2014 Lorne Kelly. All rights reserved.
//

#import <UIKit/UIKit.h>


@class WCFirmwareFlashPopup;

@interface WCFirmwareFlashPopup : WCPopup <UITableViewDelegate, WCEditDelegate>

@property (weak, nonatomic) id <WCPopupDelegate> delegate;
@property (weak) IBOutlet WCTableView *filesUITable;

- (IBAction)doneButton:(id)sender;
- (IBAction)manageButton:(id)sender;

@end
