

#import <UIKit/UIKit.h>

@interface WCTerminalViewController : UIViewController < S_TerminalUiCallBack>

@property (strong, nonatomic) IBOutlet UITextView *textView;

@end