//
//  Autotune dialog

#import "WCAutotunePopup.h"

@implementation WCAutotunePopup

/** Set size on startup */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) self.preferredContentSize = CGSizeMake(480.0,100.0);
    return self;
}

/** Set status label on load */
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"WCAutotuenPopup viewDidLoad");
    [_textLabel setText:[S getStringWithInt:R_string_autotune_ready]];
}


/** Wait for completion */
- (void) waitLoopBackgroundThread
{
    while ([S globals]->slfOperation_ != Globals_SLF_IDLE) {
        [self performSelectorOnMainThread:@selector (updateStatusMainThread) withObject:nil waitUntilDone:NO];
        [NSThread sleepForTimeInterval:0.25];
    }
    [self performSelectorOnMainThread:@selector (finishOnMainThread) withObject:nil waitUntilDone:NO];
}

/** Update the status label on main UI thread */
- (void)updateStatusMainThread
{
    NSString *s = [NSString stringWithFormat:@"%@ (%i%%)",[S getStringWithInt:R_string_autotune_running], [S globals]->slfProgress_];
    [_textLabel setText:s];
}

/** Finish on main UI thread */
- (void)finishOnMainThread
{
    [self.delegate popupDidFinish:self];
    [[S comms] startOrStopAutotuneWithBoolean:false];
}

#if 0
#pragma mark -
#pragma mark Actions
#endif

/** Button - cancel*/
- (IBAction)doneButton:(id)sender
{
    [self finishOnMainThread];
}

/** Button - start*/
- (IBAction)startButton:(id)sender
{
    [_startButtonOutlet1 setEnabled:NO];
    [[S comms] startOrStopAutotuneWithBoolean:true];
    [self performSelectorInBackground:@selector (waitLoopBackgroundThread) withObject:nil];
}

@end


