//
//  Save settings to device dialog

#import "WCSaveParamsPopup.h"

@implementation WCSaveParamsPopup

NSURL *saveURL;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) self.preferredContentSize = CGSizeMake(480.0,640.0);
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"WCSaveParamsPopup viewDidLoad");
    _filesUITable.tableData = (IOSObjectArray *) [WCUi getFilesWithExt:EXT];
    [_filesUITable setDelegate: self];
    [_filesUITable setDataSource:_filesUITable];
    [_fnTextField setReturnKeyType:UIReturnKeyGo];
    [_fnTextField setDelegate: self];
}

/** Interface TableViewDelegate - UI call when user selects a cell */
- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath {
    [_fnTextField setText: [_filesUITable.tableData objectAtIndex: indexPath.row]];
}

- (void)keyboardWasShown:(NSNotification*)aNotification {
}



#if 0
#pragma mark -
#pragma mark Actions
#endif

- (IBAction)doneButton:(id)sender
{
    [self.delegate popupDidFinish:self];
}

- (IBAction)saveButton:(id)sender {

    if ( [[_fnTextField text] length] == 0 ) [_fnTextField setText:@"Untitled"];
    
    saveURL = [[[WCUi getDocsNSURL] URLByAppendingPathComponent: [_fnTextField text]] URLByAppendingPathExtension:EXT];
   
    if ([saveURL checkResourceIsReachableAndReturnError:nil] == NO) {
        [self startFileSave];
    } else  {
        [[[UIAlertView alloc] initWithTitle:[S getStringWithInt:R_string_app_name]
                                   message:[S getStringWithInt:R_string_sl_file_overwrite]
                                  delegate:self
                         cancelButtonTitle:[S getStringWithInt:R_string_generic_no]
                         otherButtonTitles:[S getStringWithInt:R_string_generic_yes], nil] show];
    }
}

#if 0
#pragma mark -
#pragma mark Delegates
#endif

/** Alert Delegate - delete & save if user chooses overwrite */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex) {
        [[NSFileManager defaultManager] removeItemAtURL:saveURL error:nil];
        [self startFileSave];
    }
}

/** Keyboard Delegate - Respond to 'Go' (Return) key */
 - (BOOL) textFieldShouldReturn:(UITextField *)textField {
     [self saveButton:nil];
     return YES;
 }

#if 0
#pragma mark -
#pragma mark File Save
#endif

- (void) startFileSave {
    [[S comms] startSaveThread];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [self performSelectorInBackground:@selector (waitLoopBackgroundThread) withObject:nil];
}

/** Wait in background for gather process to finish */
- (void) waitLoopBackgroundThread {
    // Loop till buffer is ready to save
    while ([S globals]->slfOperation_!= Globals_SLF_IDLE) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            float p = 100.0f * ((float)[S globals]->slfProgress_ / (float)[S globals]->slfProgressMax_);
            _navBar.title = [NSString stringWithFormat:@"Saving (%1.1f%%)",p ];
        }];
        [NSThread sleepForTimeInterval:0.25];
    }
    [self performSelectorOnMainThread:@selector (saveFileMainThread) withObject:nil waitUntilDone:NO];
}

/** Save on main UI thread */
- (void)saveFileMainThread
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
    int result = [((JavaLangInteger *) [S globals]->slfResultRID_) intValue];

    NSString *stringToSave = [S globals ]->slfFileBuffer_;
    NSError *error = nil;
    [stringToSave writeToURL:saveURL atomically:NO encoding:NSUTF8StringEncoding error:&error];
    
    if (error) result = R_string_sl_file_fail_general;
    
    
    // Show result message
    [WCUi showAlertWithMessage: [S getStringWithInt: result] andTitle:nil];
    
    // Close the dialog
    [self.delegate popupDidFinish:self];
}

@end
