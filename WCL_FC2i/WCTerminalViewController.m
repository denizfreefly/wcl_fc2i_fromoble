

#import "WCTerminalViewController.h"

@implementation WCTerminalViewController

//static Boolean ignore;
NSTimer *myTimer;

/** Interface S_TerminalUiCallBack - Not using */
- (void)onDisplayTextWithChar:(unichar) c {
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self timedWrite];
    myTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(timedWrite) userInfo:nil repeats:YES];
}

/** Private - Write to textView and status indicators */
- (void) timedWrite {
    
    if(!([_textView.text equalsIgnoreCase:[[S globals]->terminalBuff_ description]])) {
        [_textView setText:[[S globals]->terminalBuff_ description]];
        [_textView scrollRangeToVisible:NSMakeRange(_textView.text.length, 0)];
        [_textView setTextColor: [UIColor whiteColor]];
    }
}

- (void)dealloc {
    [myTimer invalidate];
}

@end
