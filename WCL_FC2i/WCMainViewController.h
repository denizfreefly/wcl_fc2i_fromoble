//
//  MainViewController.h
//  WCL_FC2i
//
//  Created by Lorne Kelly on 14-02-19.
//  Copyright (c) 2014 Lorne Kelly. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface WCMainViewController : UIViewController <WCPopupDelegate, S_MainUiCallBack, UIActionSheetDelegate>

// Menu related
@property (strong, nonatomic) UIPopoverController *popupController;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *aboutButtonOutlet;
- (IBAction)aboutButton:(id)sender;
- (IBAction)systemButton:(id)sender;
- (IBAction)infoButton:(id)sender;


// Segmanted Tab ViewControllers and containing view
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedTabs;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property WCTerminalViewController *termViewController;
@property WCChartViewController *chartViewController;
@property WCConfigViewController *configViewController;
@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;

// Status indicators, boot, progressbars
@property (strong, nonatomic) IBOutlet WCStatusLabel *statusLabel0;
@property (strong, nonatomic) IBOutlet WCStatusLabel *statusLabel1;
@property (strong, nonatomic) IBOutlet WCStatusLabel *statusLabel2;
@property (strong, nonatomic) IBOutlet WCStatusLabel *statusLabel3;
@property (strong, nonatomic) IBOutlet WCStatusLabel *statusLabel4;
@property (strong, nonatomic) IBOutlet WCStatusLabel *statusLabel5;
@property (strong, nonatomic) IBOutlet WCStatusLabel *statusLabel6;
@property (strong, nonatomic) IBOutlet WCStatusLabel *statusLabel7;
@property (strong, nonatomic) IBOutlet WCStatusLabel *statusLabel8;
@property (strong, nonatomic) IBOutlet WCStatusLabel *statusLabel9;
@property (strong, nonatomic) IBOutlet WCStatusLabel *statusLabel10;

@property (strong) IBOutlet WCStatusLabel *bootUILabel;

//Dynamically assigned progressbars and matchin lables for small screeens
@property (strong, nonatomic) IBOutlet UILabel *progressBarTitle0;
@property (strong, nonatomic) IBOutlet UIProgressView *progressBar0;
@property (strong, nonatomic) IBOutlet UILabel *progressBarValue0;

@property (strong, nonatomic) IBOutlet WCStatusLabel *progressSsLable0;

@property (strong, nonatomic) IBOutlet UILabel *progressBarTitle1;
@property (strong, nonatomic) IBOutlet UIProgressView *progressBar1;
@property (strong, nonatomic) IBOutlet UILabel *progressBarValue1;

@property (strong, nonatomic) IBOutlet WCStatusLabel *progressSsLable1;

@property (strong, nonatomic) IBOutlet UILabel *progressBarTitle2;
@property (strong, nonatomic) IBOutlet UIProgressView *progressBar2;
@property (strong, nonatomic) IBOutlet UILabel *progressBarValue2;

@property (strong, nonatomic) IBOutlet WCStatusLabel *progressSsLabel2;

-(void) segmentedTabClick:(id)sender;

@end
