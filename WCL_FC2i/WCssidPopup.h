#import <UIKit/UIKit.h>


@class WCssidPopup;

@interface WCssidPopup : WCPopup < UITextFieldDelegate>

@property (weak, nonatomic) id <WCPopupDelegate> delegate;

- (IBAction)doneButton:(id)sender;
- (IBAction)startButton:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *ssidLabel;
@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;
@property (strong, nonatomic) IBOutlet UILabel *passwordConfirmLabel;
@property (strong, nonatomic) IBOutlet UILabel *defaultLabel;
@property (strong, nonatomic) IBOutlet UITextField *ssidText;
@property (strong, nonatomic) IBOutlet UITextField *passwordText;
@property (strong, nonatomic) IBOutlet UITextField *passwordConfirmText;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)defaultSwitch:(id)sender;
- (IBAction)ssidTextEditingChanged:(id)sender;


@end
