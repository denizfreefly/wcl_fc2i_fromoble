

@protocol WCEditDelegate <NSObject>
-(void) editDeleteRequestWithName: (NSString *) name;
@end

@interface WCTableView : UITableView <UITableViewDataSource>

@property (weak, nonatomic) id <WCEditDelegate> editDelegate;
@property(readwrite, retain) NSString * tableTitle;
@property(readwrite, retain) IOSObjectArray * tableData;
@property(readwrite, retain) IOSObjectArray * colorData;
//@property(readwrite, retain)  NSIndexPath * progressIndex;
@property(readwrite) CGFloat * fontSize;

//- (void) showProgress;


@end
