//
//  WCConfigView.h
//  WCL_FC2i
//
//  Created by Lorne Kelly on 13-06-25.
//  Copyright (c) 2014 Lorne Kelly. All rights reserved.
//

#import "WCChartViewChart.h"
#import "WCChartViewScale.h"

@interface WCChartViewController : UIViewController <UITableViewDelegate, S_ChartUiCallBack, UIActionSheetDelegate, UIGestureRecognizerDelegate>

- (void) lateInit;

@property (strong, nonatomic) IBOutlet WCChartViewScale *chartScale;
@property (strong, nonatomic) IBOutlet WCChartViewChart *chartView;

@property (weak) IBOutlet WCTableView *chartsetTable;
@property (weak) IBOutlet WCTableView *traceTable;

@property (strong, nonatomic) IBOutlet UISwitch *streamSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *resetSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *visibleSwitch;
@property (strong, nonatomic) IBOutlet UIButton *streamRateButton;

- (IBAction)streamSwitchAction:(id)sender;
- (IBAction)resetSwitchAction:(id)sender;
- (IBAction)visibleSwitchAction:(id)sender;
- (IBAction)streamRateButtonAction:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *streamLabel;
@property (strong, nonatomic) IBOutlet UILabel *resetLabel;
@property (strong, nonatomic) IBOutlet UILabel *visibleLabel;


- (IBAction)ssDoneButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *ssDoneButtonOutlet;
- (IBAction)ssGearButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *ssControlsView;


@end
