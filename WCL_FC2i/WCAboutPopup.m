//
//
//  Display info about connected device etc

#import "WCAboutPopup.h"

@implementation WCAboutPopup
//static CGFloat TABLE_FONTSIZE = 14;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        int width = [WCUi smallScreen] ? 320 : 480;
        self.preferredContentSize = CGSizeMake(480.0,width);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"about viewDidLoad");
    
    // Configure Labels
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [_poweredUILabel setText: [[S globals] getVersionsDescriptionWithNSString:version  ]];
    [_versionUILabel setText: [S getStringWithInt:R_string_ab_sub_title] ];
    
    // Set Variant type (M5 M10 etc)
    ParameterStructure *vPs = [[S globals] getParameterWithNSString:@"License Type"];
    if(vPs != nil)
        [_variantUILabel setText: [vPs getFormattedValue] ];
    else
        [_variantUILabel setText: @"" ];
    
    // Set title
    [_titleLable setText:  [[S globals] getTitle]];

    // Update table data
//    _licenseUITable.fontSize = &(TABLE_FONTSIZE);
    _licenseUITable.dataSource = _licenseUITable;
    _licenseUITable.tableData = (IOSObjectArray *) [[S globals] getLicenseInformation];
    [_licenseUITable reloadData];
}

- (IBAction)doneButton:(id)sender
{
    [self.delegate popupDidFinish:self];
}

@end
