//
//  WCConfigViewController.m
//  WCL_FC2i
//
//  Created by Lorne Kelly on 14-02-24.
//  Copyright (c) 2014 Lorne Kelly. All rights reserved.
//

#import "WCConfigViewController.h"

// Remember Param selection
static NSInteger paramSel[100];

// Autorepeat buttons
const float TIMER_RATE = 0.7;
const float TIMER_RATE_MIN = 0.3;
const float TIMER_RATE_INC = 0.09;
float timerRate = 0;

@implementation WCConfigViewController

/** These items must be initialized after 'self' is placed in a parent view */
- (void) lateInit {
    
    // Configure tables
    _categoryUITable.tableData = (IOSObjectArray *) [[S globals] getParameterCategoryNames];
    [_categoryUITable setDelegate: self];
    [_categoryUITable setDataSource:_categoryUITable];
    
    [_namesUITable setDelegate: self];
    [_namesUITable setDataSource:_namesUITable];
    
    // Configure buttons
    if (![WCUi smallScreen]) {
        [_coarseIncreaseButtonOutlet addTarget:self action:@selector(stopTimer) forControlEvents:UIControlEventTouchUpInside];
        [_coarseIncreaseButtonOutlet addTarget:self action:@selector(stopTimer) forControlEvents:UIControlEventTouchUpOutside];
        [_coarseIncreaseButtonOutlet addTarget:self action:@selector(coarseIncreaseButtonDown) forControlEvents:UIControlEventTouchDown];
        //[_coarseIncreaseButtonOutlet setBackgroundColor: [WCUi appGrey]];
        [_coarseDecreaseButtonOutlet addTarget:self action:@selector(stopTimer) forControlEvents:UIControlEventTouchUpInside];
        [_coarseDecreaseButtonOutlet addTarget:self action:@selector(stopTimer) forControlEvents:UIControlEventTouchUpOutside];
        [_coarseDecreaseButtonOutlet addTarget:self action:@selector(coarseDecreaseButtonDown) forControlEvents:UIControlEventTouchDown];
        //[_coarseDecreaseButtonOutlet setBackgroundColor: [WCUi appGrey]];
        [_fineIncreaseButtonOutlet addTarget:self action:@selector(stopTimer) forControlEvents:UIControlEventTouchUpInside];
        [_fineIncreaseButtonOutlet addTarget:self action:@selector(stopTimer) forControlEvents:UIControlEventTouchUpOutside];
        [_fineIncreaseButtonOutlet addTarget:self action:@selector(fineIncreaseButtonDown) forControlEvents:UIControlEventTouchDown];
        //[_fineIncreaseButtonOutlet setBackgroundColor: [WCUi appGrey]];
        [_fineDecreaseButtonOutlet addTarget:self action:@selector(stopTimer) forControlEvents:UIControlEventTouchUpInside];
        [_fineDecreaseButtonOutlet addTarget:self action:@selector(stopTimer) forControlEvents:UIControlEventTouchUpOutside];
        [_fineDecreaseButtonOutlet addTarget:self action:@selector(fineDecreaseButtonDown) forControlEvents:UIControlEventTouchDown];
        //[_fineDecreaseButtonOutlet setBackgroundColor: [WCUi appGrey]];
    }
    
    // Set selections (causes namesTable and paramater update)
    [self tableView: nil didSelectRowAtIndexPath:nil];
}

#if 0
#pragma mark -
#pragma mark S_ConfigUiCallBack implementation
#endif

/** Interface S_ConfigUiCallBack - called if UI needs updating */
- (void) onStateChange {
    NSLog(@"onStateChange config");
    
    if ([S globals]->connected_) {
        [[S comms] requestValueWithNSString: _selectedParamName];
    } else {
        [self onDisplayParameterWithParameterStructure:nil];
    }
}


/** Interface S_ConfigUiCallBack - called when parameter data changes */
- (void) onDisplayParameterWithParameterStructure:(ParameterStructure *)parameter {
    
    NSString * valNameStr = [S getStringWithInt: R_string_configValue ];
    NSString * unitNameStr = [S getStringWithInt:R_string_configUnits];
    NSString * noValStr = [S getStringWithInt:R_string_generic_default];
    
    if (parameter == nil) {
        [_unitsUILabel setText: [NSString stringWithFormat:@"%@ %@",unitNameStr,noValStr]];
        [_valueUILabel setText: [NSString stringWithFormat:@"%@ %@",valNameStr,noValStr]];
    } else {
        if ([_selectedParamName isEqual:parameter->key_]) {
            [_unitsUILabel setText: [NSString stringWithFormat:@"%@ %@",unitNameStr,parameter->units_ ]];
            [_valueUILabel setText: [NSString stringWithFormat:@"%@ %@",valNameStr,[parameter getFormattedValue]]];
        }
    }
}

#if 0
#pragma mark -
#pragma mark Table delegate stuff
#endif


/** Interface TableViewDelegate - UI call when user selects a cell */
- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath {
    
    // Set category table to first selection if nothing specified
    if (indexPath == nil) {
        [self.categoryUITable selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:0];
    }
    
    // Set category table if not specified
    if (tableView == nil) {
        _categoryUITable.tableData = (IOSObjectArray *) [[S globals] getParameterCategoryNames];
        [_categoryUITable reloadData];
        [_categoryUITable selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:0];
    }
    
    // If call from categoryTable or null, repopulate names table
    if (tableView != _namesUITable) {
        _selectedCategoryName = [_categoryUITable.tableData objectAtIndex: indexPath.row];
        _namesUITable.tableData = (IOSObjectArray *) [[S globals] getParameterNamesWithNSString:_selectedCategoryName];
        [_namesUITable reloadData];
        // Set name selection to stored index for this category
        long nameIndex = paramSel[indexPath.row ];
        if (nameIndex >= _namesUITable.tableData.count) nameIndex = 0;
        [_namesUITable selectRowAtIndexPath:[NSIndexPath indexPathForRow:nameIndex inSection:0] animated:NO scrollPosition:0];
        _selectedParamName = [_namesUITable.tableData objectAtIndex: nameIndex];
    }
    
    // If call from namesTable record the selection
    if (tableView == _namesUITable ) {
        _selectedParamName = [_namesUITable.tableData objectAtIndex: indexPath.row];
        paramSel[[_categoryUITable indexPathForSelectedRow].row ] = indexPath.row;
    }
    
    if(indexPath != nil) {
        [[S globals] setInfoContextWithNSString:_selectedCategoryName withNSString:_selectedParamName];
    }
    
    [self onStateChange];
}


/** As the dongle required for this App connects on the Aux port, we dissable Parameters that make changes to that port */
-(bool) dissableIncreaseButtons {
    if( [_selectedParamName equalsIgnoreCase:@"Aux Port Function"]) return true;
    if( [_selectedParamName equalsIgnoreCase:@"Radio Type"] ) {
        ParameterStructure *ps = [((Globals *) nil_chk([S globals])) getParameterWithNSString:@"Radio Type"];
        if (ps != nil && ps.getValue > 4) return true;
    }
    return false;
}

#if 0
#pragma mark -
#pragma mark Respond to Inc/Dec buttons iPhone
#endif

/** iPhone version just sends the value from the stepper */
- (IBAction)fineIncDecButton:(id)sender {
    UIStepper *st = (UIStepper*) sender;
    if ([self dissableIncreaseButtons] && [st value] > 0){
        [st setValue:0];
        return;
    }
    [[S comms] changeValueWithLong:[st value] withNSString: _selectedParamName ];
    if( [st value] != 0) [st setValue:0];
}

#if 0
#pragma mark -
#pragma mark Respond to Inc/Dec buttons iPad
#endif

/** iPad version is complicated as iPad does not have autorepeat */


/** Start button loop */
-(void) coarseIncreaseButtonDown {
    if ([self dissableIncreaseButtons]) return;
    timerRate = TIMER_RATE;
    [self coarseIncreaseTimer];
}

/** Continue button loop */
-(void) coarseIncreaseTimer {
    if (timerRate != 0) {
        timerRate = (timerRate < TIMER_RATE_MIN) ? TIMER_RATE_MIN : (timerRate - TIMER_RATE_INC);
        [self performSelector:@selector(coarseIncreaseTimer) withObject:nil afterDelay: timerRate ];
        [[S comms] changeValueWithLong:10 withNSString: _selectedParamName ];
        NSLog(@"c+");
    }
}

/** Start button loop */
-(void) coarseDecreaseButtonDown {
//    if ([self dissableButtons]) return;
    timerRate = TIMER_RATE;
    [self coarseDecreaseTimer];
}

/** Continue button loop */
-(void) coarseDecreaseTimer {
    if (timerRate != 0) {
        timerRate = (timerRate < TIMER_RATE_MIN) ? TIMER_RATE_MIN : (timerRate - TIMER_RATE_INC);
        [self performSelector:@selector(coarseDecreaseTimer) withObject:nil afterDelay: timerRate ];
        [[S comms] changeValueWithLong:-10 withNSString:_selectedParamName];
        NSLog(@"c-");
    }
}

/** Start button loop */
-(void) fineIncreaseButtonDown {
    if ([self dissableIncreaseButtons]) return;
    timerRate = TIMER_RATE;
    [self fineIncreaseTimer];
}

/** Continue button loop */
-(void) fineIncreaseTimer {
    if (timerRate != 0) {
        timerRate = (timerRate < TIMER_RATE_MIN) ? TIMER_RATE_MIN : (timerRate - TIMER_RATE_INC);
        [self performSelector:@selector(fineIncreaseTimer) withObject:nil afterDelay: timerRate ];
        [[S comms] changeValueWithLong:1 withNSString: _selectedParamName ];
        NSLog(@"f+");
    }
}


/** Start button loop */
-(void) fineDecreaseButtonDown {
//    if ([self dissableButtons]) return;
    timerRate = TIMER_RATE;
    [self fineDecreaseTimer];
}

/** Continue button loop */
-(void) fineDecreaseTimer {
    if (timerRate != 0) {
        timerRate = (timerRate < TIMER_RATE_MIN) ? TIMER_RATE_MIN : (timerRate - TIMER_RATE_INC);
        [self performSelector:@selector(fineDecreaseTimer) withObject:nil afterDelay: timerRate ];
        [[S comms] changeValueWithLong:-1 withNSString: _selectedParamName ];
        NSLog(@"f-");
    }
}

/** Called when user lifts finger from button - stops button loop */
-(void) stopTimer {
    timerRate = 0;
}

@end
