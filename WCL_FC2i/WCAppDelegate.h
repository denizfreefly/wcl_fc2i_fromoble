//
//  AppDelegate.h
//  WCL_FC2i
//
//  Created by Lorne Kelly on 14-02-19.
//  Copyright (c) 2014 Lorne Kelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WCMainViewController;

@interface WCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) WCMainViewController *mainViewController;

@end
