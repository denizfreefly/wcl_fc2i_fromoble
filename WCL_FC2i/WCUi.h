//
//  WCUi.h
//
//  Created by Lorne Kelly 
//  Copyright (c) 2014 Lorne Kelly. All rights reserved.
//

@interface WCUi : NSObject

+ (bool) smallScreen;
+ (UIColor*) appCyan;
+ (UIColor*) appGrey;
+ (NSURL *)getDocsNSURL;
+ (NSURL *)getNSURLfor: (NSString *) fileName withExt: ext;
+ (IOSObjectArray *)getFilesWithExt: (NSString *) ext;
+ (NSArray *)createColorArrayWithId:(int) colorId;
+ (UIColor *)createProgressColorWithId:(int) colorId;
+ (void)lineOnRect: (CGRect) r X1: (float) x1 Y1: (float) y1 X2: (float) x2 Y2: (float) y2;
+ (UIColor *)getColorForTrace: (TraceStructure *) ts;
+ (void) showAlertWithMessage: (NSString*) msg andTitle: (NSString*) title;
+ (void) showDialogWithMessage: (NSString*) msg andTitle: (NSString*) title andDelegate: (id) delegate;
+ (void) showExitDialogWithMessage: (NSString*) msg andTitle: (NSString*) title;
+ (void) dismissDialogs;
+ (NSString *)getCurrentWifiHotSpotName;
@end
