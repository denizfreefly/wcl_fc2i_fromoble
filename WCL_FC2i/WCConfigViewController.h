//
//  WCConfigViewController.h
//  WCL_FC2i
//
//  Created by Lorne Kelly on 14-02-24.
//  Copyright (c) 2014 Lorne Kelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WCConfigViewController : UIViewController <UITableViewDelegate, S_ConfigUiCallBack>

- (void) lateInit;

// Parameter name selected in table
@property (strong)  NSString *selectedParamName;
@property (strong)  NSString *selectedCategoryName;

// Display lables
@property (weak) IBOutlet UILabel *unitsUILabel;
@property (weak) IBOutlet UILabel *valueUILabel;

// Tables & table controllers
@property (weak) IBOutlet WCTableView *categoryUITable;
@property (weak) IBOutlet WCTableView *namesUITable;

- (IBAction)fineIncDecButton:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *coarseIncreaseButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *coarseDecreaseButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *fineIncreaseButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *fineDecreaseButtonOutlet;



@end
