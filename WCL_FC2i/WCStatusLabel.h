//
//  UIW_StatusLabel.h
//  WCL_FC2i
//
//  Created by Lorne Kelly on 
//  Copyright (c) 2014 Lorne Kelly. All rights reserved.
//


@interface WCStatusLabel : UIView

@property(readwrite, retain) UILabel* subLabel;
@property(readwrite, retain) NSArray* gradientColors;

- (void)updateWithIndex:(int) itemId;
- (void)updateBoot;
- (void)updateAsProgressBarWithIndex:(int) itemId;

@end
