#import <UIKit/UIKit.h>


@class WCInfoPopup;

@interface WCInfoPopup : WCPopup <UITableViewDelegate, UIWebViewDelegate>

@property (weak, nonatomic) id <WCPopupDelegate> delegate;

- (IBAction)doneButton:(id)sender;

@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
