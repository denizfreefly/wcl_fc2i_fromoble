//
//  Autotune dialog

#import "WCCompassCalPopup.h"
#import "WCAppDelegate.h"
#import "WCMainViewController.h"

@implementation WCCompassCalPopup

/** Set size on startup */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) self.preferredContentSize = CGSizeMake(480.0,200.0);
    return self;
}

/** Set status label on load */
- (void)viewDidLoad
{
    [super viewDidLoad];
    [_textLabel setText:[S getStringWithInt: R_string_compass_cal_message]];
    [_textLabel setNumberOfLines:0];
    [_textLabel sizeToFit];
}

/** Button - cancel*/
- (IBAction)doneButton:(id)sender
{
    [self.delegate popupDidFinish:self];
}

/** Button - start*/
- (IBAction)startButton:(id)sender
{
    WCMainViewController *vc = (WCMainViewController *) [[[UIApplication sharedApplication] keyWindow] rootViewController];
    [vc segmentedTabs].selectedSegmentIndex = 1 ;  // Hardcoded tab 1 = terminal
    [vc segmentedTabClick:nil];
    [[S comms] changeValueWithLong:1 withNSString: Globals_PARAM_COMPASS_CAL_ ];
    [self.delegate popupDidFinish:self];
}

@end


