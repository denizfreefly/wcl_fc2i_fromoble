//  A colection of static convienence methods shared across the application UI
//


@implementation WCUi

static UIAlertView *alertView;

/** Returns the URL for the documents directory */
+ (NSURL *)getDocsNSURL {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

/** Search the Docs directory for fileName.ext & return NSURL if found */
+ (NSURL *)getNSURLfor: (NSString *) fileName withExt: ext {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *allFiles = [fileManager contentsOfDirectoryAtURL:[self getDocsNSURL] includingPropertiesForKeys:[NSArray arrayWithObject:NSURLNameKey] options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
    for (NSURL *fItem in allFiles) {
        if ([[fItem pathExtension] isEqualToString: ext] && [[[fItem lastPathComponent] stringByDeletingPathExtension] isEqualToString:fileName ]) return fItem;
    }
    return nil;
}

/** Returns an array of files with file extension ext */
+ (IOSObjectArray *)getFilesWithExt: (NSString *) ext {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *allFiles = [fileManager contentsOfDirectoryAtURL:[self getDocsNSURL] includingPropertiesForKeys:[NSArray arrayWithObject:NSURLNameKey] options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
    NSMutableArray*files=[NSMutableArray array];
    
    for (NSURL *fileName in allFiles) {
        if ([[fileName pathExtension] isEqualToString: ext])
            [files addObject: [[fileName lastPathComponent] stringByDeletingPathExtension] ];
    }
    return nil;//TODO [IOSObjectArray arrayWithNSArray:files type: [IOSClass classWithClass:[NSString class]]  ];
}



+ (bool) smallScreen{
    return   ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone);
}

//5f f0 c9 is 95 240 201
+ (UIColor*) appCyan {
    static UIColor * cyan;
    if (!(cyan)) cyan=  [self createUIColorWithBytesRed:0x5F andGreen:0xF0 andBlue:0xC9 andAlpha:0xFF];
    return cyan;
}

+ (UIColor*) appGrey {
    static UIColor * grey;
    if (!(grey)) grey=  [UIColor colorWithRed:217.0/255.0 green:217.0/255.0 blue:217.0/255.0 alpha:1.0];
    return grey;
}


/** Generates a color for LevelIndicator gradient. Based on cyan  */
+ (UIColor *)createProgressColorWithId:(int) colorId {
    static UIColor * red;
    if (!(red)) red= [self createUIColorWithBytesRed:0xFB andGreen:0x69 andBlue:0x69 andAlpha:0xFF];
    
    if (colorId == R_drawable_status_progress_bar) {
        return [self appCyan];
    } else {
        return red;
    }
}


/** Generates a color array for status indicator gradient. Based on cyan 5FF0C9 */
+ (NSArray *)createColorArrayWithId:(int) colorId {
    
    UIColor *edgeColor;
    UIColor *midColor;
    UIColor *mid2Color;
    
    static NSArray *plainGradient = nil;
    static NSArray *redGradient = nil;
    static NSArray *okGradient = nil;
    
    if (plainGradient == nil) {
        edgeColor = [self createUIColorWithBytesRed:0xBA andGreen:0xBA andBlue:0xBA andAlpha:0xFF];
        midColor = [self createUIColorWithBytesRed:0xCA andGreen:0xCA andBlue:0xCA andAlpha:0xFF];
        mid2Color = [self createUIColorWithBytesRed:0xC7 andGreen:0xC7 andBlue:0xC7 andAlpha:0xFF];
        plainGradient = [NSArray arrayWithObjects:(id)edgeColor.CGColor,(id)midColor.CGColor,(id)mid2Color.CGColor,(id)edgeColor.CGColor,nil];
    }
    if (okGradient == nil) {
        edgeColor = [self createUIColorWithBytesRed:0x3F andGreen:0xD0 andBlue:0xA9 andAlpha:0xFF];
        midColor = [self createUIColorWithBytesRed:0x5F andGreen:0xF0 andBlue:0xC9 andAlpha:0xFF];
        mid2Color = [self createUIColorWithBytesRed:0x63 andGreen:0xEB andBlue:0xC4 andAlpha:0xFF];
        okGradient = [NSArray arrayWithObjects:(id)edgeColor.CGColor,(id)midColor.CGColor,(id)mid2Color.CGColor,(id)edgeColor.CGColor,nil];
    }
    if (redGradient == nil) {
        edgeColor = [self createUIColorWithBytesRed:0xDF andGreen:0x33 andBlue:0x33 andAlpha:0xFF];
        midColor = [self createUIColorWithBytesRed:0xFF andGreen:0x73 andBlue:0x73 andAlpha:0xFF];
        mid2Color = [self createUIColorWithBytesRed:0xFB andGreen:0x69 andBlue:0x69 andAlpha:0xFF];
        redGradient = [NSArray arrayWithObjects:(id)edgeColor.CGColor,(id)midColor.CGColor,(id)mid2Color.CGColor,(id)edgeColor.CGColor,nil];
    }
    
    switch (colorId) {
            
        case R_drawable_status_box_grey :
            
            return plainGradient;
            break;
            
        case R_drawable_status_box_red :
        case R_drawable_status_progress_bar_red:
            return redGradient;
            break;
            
        case R_drawable_status_progress_bar:
        case R_drawable_status_box_cyan :
            return okGradient;
            break;
    }
    return nil;
}

/** Returns a UIcolor using rgb values in the 0-255 range (rather than ios 0-1) */
+ (UIColor *)createUIColorWithBytesRed:(Byte) red  andGreen:(Byte) green andBlue:(Byte) blue andAlpha:(Byte) alpha {
    float redF = red / (float)255;
    float greenF = green / (float)255;
    float blueF = blue / (float)255;
    float alphaF = alpha / (float)255;
    return [UIColor colorWithRed:redF green:greenF blue:blueF alpha:alphaF];
}

/** Convienence method draws a line on the passed NSRect */
+ (void)lineOnRect: (CGRect) r X1: (float) x1 Y1: (float) y1 X2: (float) x2 Y2: (float) y2 {
    UIBezierPath *line = [UIBezierPath bezierPath];
    [line moveToPoint:CGPointMake(x1, y1)];
    [line addLineToPoint:CGPointMake(x2, y2)];
    [line setLineWidth:1.0];
    [line stroke];
}

/** Returns a trace color based on the passed traceStructure and the trace array in [S charts] */
+ (UIColor *)getColorForTrace: (TraceStructure *) ts {
    if (ts == nil) {
        return [UIColor blackColor];
    } else {
        int red = IOSIntArray_Get(nil_chk(IOSObjectArray_Get(nil_chk(Charts_get_traceColors_()), ts->mapIndex_)), 0);
        int green = IOSIntArray_Get(nil_chk(IOSObjectArray_Get(nil_chk(Charts_get_traceColors_()), ts->mapIndex_)), 1);
        int blue = IOSIntArray_Get(nil_chk(IOSObjectArray_Get(nil_chk(Charts_get_traceColors_()), ts->mapIndex_)), 2);
        return [self createUIColorWithBytesRed:red andGreen:green andBlue:blue andAlpha:0xFF];
    }
}

/** Show an alert with ok as the only choice and NO delegate */
+ (void) showAlertWithMessage: (NSString*) msg andTitle: (NSString*) title {
    if (!title) title = [S getStringWithInt:R_string_app_name];
    alertView = [[UIAlertView alloc]
                          initWithTitle: title
                          message: msg
                          delegate:nil
                          cancelButtonTitle:[S getStringWithInt:R_string_generic_ok]
                          otherButtonTitles:nil, nil];
    [alertView show];
}

/** Show an alert with ok as the only choice and NO delegate */
+ (void) showDialogWithMessage: (NSString*) msg andTitle: (NSString*) title andDelegate: (id) delegate {
    if (!title) title = [S getStringWithInt:R_string_app_name];
    alertView = [[UIAlertView alloc]
                          initWithTitle: title
                          message: msg
                          delegate:delegate
                          cancelButtonTitle:[S getStringWithInt:R_string_generic_no]
                          otherButtonTitles:[S getStringWithInt:R_string_generic_yes], nil];
    [alertView show];
}


/** NOTE: alertView with no buttons is the best way to 'exit an app' */
+ (void) showExitDialogWithMessage: (NSString*) msg andTitle: (NSString*) title {
    
    if (!title) title = [S getStringWithInt:R_string_app_name];
    alertView = [[UIAlertView alloc] initWithTitle: title
                                       message: msg
                                      delegate: nil
                             cancelButtonTitle: nil
                             otherButtonTitles:nil, nil];
    [alertView show];
//    [[S comms] stopStatusThread];
}

/** Returns an NSString containing the name of the connected hotspot */
/** Note: This will return null in simulator regardless of coneection */
+ (NSString *) getCurrentWifiHotSpotName {
    
    if ( [[[UIDevice currentDevice] model] contains:@"Simulator"] ) return @"Freefly-Simulator";
    
    NSString *wifiName = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    for (NSString *ifnam in ifs) {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info[@"SSID"]) {
            wifiName = info[@"SSID"];
        }
    }
    return wifiName;
}


+ (void) dismissDialogs {
    [alertView dismissWithClickedButtonIndex:5 animated:NO];
}
@end
