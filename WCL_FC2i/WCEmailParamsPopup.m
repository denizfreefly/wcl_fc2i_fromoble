//
//  Send settings/config files by email

#import "WCEmailParamsPopup.h"


@implementation WCEmailParamsPopup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) self.preferredContentSize = CGSizeMake(480.0,640.0);
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"WCEmailParamsPopup viewDidLoad");
    _filesUITable.tableData = (IOSObjectArray *) [WCUi getFilesWithExt: EXT];
    [_filesUITable setDelegate: self];
    [_filesUITable setDataSource:_filesUITable];
    [_filesUITable setEditDelegate: self];
}


/** Interface TableViewDelegate - UI call when user selects a cell */
- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath
{
    if ([MFMailComposeViewController canSendMail])
    {
        NSString * selectedFile = [_filesUITable.tableData objectAtIndex: indexPath.row];
        NSData *fileData = [NSData dataWithContentsOfURL: [WCUi getNSURLfor:selectedFile withExt:EXT] ];
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        
        [mailer setSubject: NSLocalizedStringFromTable (@"email_msg", @"R", nil) ];
        [mailer setMessageBody: NSLocalizedStringFromTable (@"email_msg", @"R", nil) isHTML:NO];
        
        [mailer addAttachmentData:fileData mimeType:@"text/plain" fileName: [NSString stringWithFormat:@"%@.%@", selectedFile, EXT]  ];
        [self presentViewController:mailer animated:YES completion:nil];
    }
    else
    {
        [WCUi showAlertWithMessage: NSLocalizedStringFromTable (@"email_not_configured", @"R", nil) andTitle:nil];
    }
}

/** Interface MFMailComposeViewControllerDelegate - Remove file & redisplay list */
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}
    
    


#if 0
#pragma mark -
#pragma mark Delegates
#endif

/** Interface WCEditDelegate - Remove file & redisplay list */
- (void) editDeleteRequestWithName: name
{
    NSURL *delURL = [WCUi getNSURLfor:name withExt:EXT];
    [[NSFileManager defaultManager] removeItemAtURL:delURL error:nil];
    _filesUITable.tableData = (IOSObjectArray *) [WCUi getFilesWithExt: EXT];
    [_filesUITable reloadData];
    NSLog(@"Recieved callback with %@", name);
}

#if 0
#pragma mark -
#pragma mark Actions
#endif

/** Button */
- (IBAction)doneButton:(id)sender
{
    [self.delegate popupDidFinish:self];
}

/** Button - turn delete on and off*/
- (IBAction)manageButton:(id)sender
{
    [_filesUITable setEditing: !(_filesUITable.editing)];
}

@end


