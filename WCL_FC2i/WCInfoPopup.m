//
//  Contextual information/help dialog
//#import <WebKit/WebKit.h>

#import "WCInfoPopup.h"

@implementation WCInfoPopup

NSString *anchorPath;

/** Set size on startup */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) self.preferredContentSize = CGSizeMake(480.0,480.0);
    return self;
}

/** Set status label on load */
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"WCInfoPopup viewDidLoad");

    [_webView setDelegate:self];

    // Strip out anchor as apple can't read a url
    NSString *fullPath = [[S globals] getInfoContextPath];
    NSString *infoPath;
    NSRange anchorPos = [fullPath rangeOfString:@"#"];
    
    if(anchorPos.location == NSNotFound) {
        infoPath = fullPath;
        anchorPath = nil;
    } else {
        infoPath = [NSString stringWithString :[fullPath substringToIndex:anchorPos.location] ];
        anchorPath = [NSString stringWithString :[fullPath substringFromIndex:anchorPos.location + 1] ];
    }
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] resourcePath] ];
    url = [url URLByAppendingPathComponent:@"html_noedit/"];
    url = [url URLByAppendingPathComponent: infoPath];
    [_webView  loadRequest:[NSURLRequest requestWithURL:url]];
    
    NSLog(@"[%@][%@]", infoPath, anchorPath);
}

// Delegate - Scroll to anchor after page load
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *js = [NSString stringWithFormat:@"%@%@%@", @"window.location.hash='", anchorPath, @"';"];
    [webView stringByEvaluatingJavaScriptFromString: js];
}

#if 0
#pragma mark -
#pragma mark Actions
#endif

/** Button - cancel*/
- (IBAction)doneButton:(id)sender
{
     [self.delegate popupDidFinish:self];
}



@end


