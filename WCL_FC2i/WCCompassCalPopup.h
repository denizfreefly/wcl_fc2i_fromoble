#import <UIKit/UIKit.h>


@class WCCompassCalPopup;

@interface WCCompassCalPopup : WCPopup <UITableViewDelegate>

@property (weak, nonatomic) id <WCPopupDelegate> delegate;
//@property (weak) IBOutlet WCTableView *filesUITable;

- (IBAction)doneButton:(id)sender;
- (IBAction)startButton:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *textLabel;

@end
