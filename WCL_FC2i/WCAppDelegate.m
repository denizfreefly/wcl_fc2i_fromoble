#import "WCAppDelegate.h"
#import "WCMainViewController.h"

@implementation WCAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    if ([WCUi smallScreen]) {
        self.mainViewController = [[WCMainViewController alloc] initWithNibName:@"WCMainViewController_iPhone" bundle:nil];
    } else {
        self.mainViewController = [[WCMainViewController alloc] initWithNibName:@"WCMainViewController_iPad" bundle:nil];
    }
    
    [[UIView appearance] setTintColor:[WCUi appCyan]];
    
    self.window.rootViewController = self.mainViewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Hide any popups
    if ([WCUi smallScreen]) {
        [self.mainViewController dismissViewControllerAnimated:NO completion:nil];
    } else {
        [self.mainViewController.popupController dismissPopoverAnimated:NO];
    }
    
    // Close any dialogs
    [WCUi dismissDialogs];
    
    // Unregister UI items
    [S unregisterUiCallbackWithS_ChartUiCallBack:nil];
    [S unregisterUiCallbackWithS_ConfigUiCallBack:nil];
    [S unregisterUiCallbackWithS_ConfigUiCallBack:nil];
    
    // Stop all background processes (Call from background to prevent UI lockup)
    [self performSelectorInBackground:@selector (sfinishInBackground) withObject:nil];
}
- (void) sfinishInBackground {
    [S finish];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}




- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{


    // Start the WIFI connection
    [S connection];
    
    // Register (or re-register if sleeping) UI elements
    [S registerUiCallbackWithS_ConfigUiCallBack: [[self mainViewController] configViewController] ];
    [S registerUiCallbackWithS_TerminalUiCallBack: [[self mainViewController] termViewController] ];
    [S registerUiCallbackWithS_ChartUiCallBack: [[self mainViewController] chartViewController] ];
    [S registerUiMainWithS_MainUiCallBack: [self mainViewController] ];
    
    //***************************************************
//    [[[S comms] hook] simConnect5];
    //***************************************************
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/** Override ViewController - Support landscape only */
//- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
//   return UIInterfaceOrientationMaskLandscape;
//}

/** Recieve associated file types */
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    if (url){
        
        // Determine locations
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSURL *documentsURL = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        NSURL *dstUrl = [documentsURL URLByAppendingPathComponent: [url lastPathComponent] ];

        // Remove previous copy if any
        NSError *error;
        if ([dstUrl checkResourceIsReachableAndReturnError:nil ] == YES) {
           [fileManager removeItemAtURL:dstUrl error:&error];
        }
        
        // Copy file into ~/Documents (Files in inbox are readonly)
        if (!error) [fileManager moveItemAtURL:url toURL:dstUrl error: &error];
        
        // Set message for user
        NSString *msg = NSLocalizedStringFromTable (@"email_stored", @"R", nil);
        if (error) msg = [error localizedDescription];
        
        // Show message
        [WCUi showExitDialogWithMessage:msg andTitle:nil];
  
    }
    return YES;
}

@end
