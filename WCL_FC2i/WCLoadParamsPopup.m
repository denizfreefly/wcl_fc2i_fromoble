//
//  Load settings to device dialog

#import "WCLoadParamsPopup.h"

@implementation WCLoadParamsPopup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) self.preferredContentSize = CGSizeMake(480.0,640.0);
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"WCLoadParamsPopup viewDidLoad");
    _filesUITable.tableData = (IOSObjectArray *) [WCUi getFilesWithExt: EXT];
    [_filesUITable setDelegate: self];
    [_filesUITable setDataSource:_filesUITable];
    [_filesUITable setEditDelegate: self];
}



/** Interface TableViewDelegate - UI call when user selects a cell */
- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath
{
    NSString * selectedFile = [_filesUITable.tableData objectAtIndex: indexPath.row];
    NSData *fileData = [NSData dataWithContentsOfURL: [WCUi getNSURLfor:selectedFile withExt:EXT] ];
    [S globals]->slfFileBuffer_ = [[NSString alloc]initWithData:fileData encoding:NSUTF8StringEncoding];
    // Bail if corrupt data
    if(![S globals]->slfFileBuffer_) {
        [self.delegate popupDidFinish:self];
        return;
    }
    // Stop user interaction
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    // Start the transfer & wait for completion
    //    [_filesUITable setProgressIndex:indexPath];
    [[S comms] startLoadThread];
    [self performSelectorInBackground:@selector (waitLoopBackgroundThread) withObject:nil ];
}

/** Waits for completion */
- (void) waitLoopBackgroundThread
{
    while ([S globals]->slfOperation_ != Globals_SLF_IDLE) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            float p = 100.0f * ((float)[S globals]->slfProgress_ / (float)[S globals]->slfProgressMax_);
            _navBar.title = [NSString stringWithFormat:@"Loading (%1.1f%%)",p ];
        }];
        [NSThread sleepForTimeInterval:1.0];
    }
    [self performSelectorOnMainThread:@selector (finishWithMessageMainThread) withObject:nil waitUntilDone:NO];
}

/** Finish on main UI thread */
- (void)finishWithMessageMainThread
{
    // Display message
    int result =[((JavaLangInteger *) [S globals]->slfResultRID_) intValue];
    [WCUi showAlertWithMessage: [S getStringWithInt: result] andTitle:nil];
    // Close the dialog
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [self.delegate popupDidFinish:self];
    //update displayed param
    [S configFragOnStateChange];
}

#if 0
#pragma mark -
#pragma mark Delegates
#endif

/** Interface WCEditDelegate - Remove file & redisplay list */
- (void) editDeleteRequestWithName: name
{
    NSURL *delURL = [WCUi getNSURLfor:name withExt:EXT];
    [[NSFileManager defaultManager] removeItemAtURL:delURL error:nil];
    _filesUITable.tableData = (IOSObjectArray *) [WCUi getFilesWithExt: EXT];
    [_filesUITable reloadData];
    NSLog(@"Recieved callback with %@", name);
}

#if 0
#pragma mark -
#pragma mark Actions
#endif

/** Button */
- (IBAction)doneButton:(id)sender
{
    [self.delegate popupDidFinish:self];
}

/** Button - turn delete on and off*/
- (IBAction)manageButton:(id)sender
{
    [_filesUITable setEditing: !(_filesUITable.editing)];
}

@end


