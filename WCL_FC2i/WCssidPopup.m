//
//  Set or change the WIFI SSID

#import "WCssidPopup.h"

@implementation WCssidPopup

UITextField *activeField;
CGSize kbSize;

/** Set size on startup */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.preferredContentSize = CGSizeMake(480.0,320.0);
        
        // Set up for keyboard notifications
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

/** Set labels & text on load */
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"WCssidPopup viewDidLoad");
    [_ssidLabel setText:[S getStringWithInt:R_string_ssid_enter_ssid]];
    [_passwordLabel setText:[S getStringWithInt:R_string_ssid_enter_password1]];
    [_passwordConfirmLabel setText:[S getStringWithInt:R_string_ssid_enter_password2]];
    [_defaultLabel setText:[S getStringWithInt:R_string_ssid_reset ]];
    NSLog(@"ssid = %@", [WCUi getCurrentWifiHotSpotName ]);
    [_ssidText setText: [WCUi getCurrentWifiHotSpotName ]];
    
    [_ssidText setDelegate:self];
    [_passwordText setDelegate:self];
    [_passwordConfirmText setDelegate:self];
    
    //for keyboard dismiss
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [self privateSetScroller];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
    [self privateSetScroller];
}

- (void)privateSetScroller  {
    
    if(!activeField) return;
    if(![WCUi smallScreen]) return;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    CGPoint origin = activeField.frame.origin;
    origin.y -= _scrollView.contentOffset.y;
    if (!CGRectContainsPoint(aRect, origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-(aRect.size.height));
        [_scrollView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    CGPoint scrollPoint = CGPointMake(0.0, 0.0);
    [_scrollView setContentOffset:scrollPoint animated:YES];
}

/** Cycle through text fields when enter key pressed */
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField == _ssidText) {
        [_passwordText becomeFirstResponder];
    } else if(textField == _passwordText) {
        [_passwordConfirmText becomeFirstResponder];
    } else if(textField == _passwordConfirmText) {
        [_ssidText becomeFirstResponder];
    }
    return NO;
}

#if 0
#pragma mark -
#pragma mark Actions
#endif

/** Button - cancel/done */
- (IBAction)doneButton:(id)sender
{
    [self.delegate popupDidFinish:self];
}

/** Button - start */
- (IBAction)startButton:(id)sender
{
    if([_ssidText isEnabled]) {
        IOSByteArray *iba = [[S globals] generateSSIDPacketWithNSString:[_ssidText text] withNSString: [_passwordText text] ];
        if (iba.count == 0) {
            [WCUi showAlertWithMessage:[S getStringWithInt: R_string_ssid_error_entry] andTitle:nil];
        } else if (! [[_passwordText text] isEqualToString:[_passwordConfirmText text]] ) {
            [WCUi showAlertWithMessage:[S getStringWithInt: R_string_ssid_error_pw_match ] andTitle:nil];
        } else {
            [WCUi showDialogWithMessage: [S getStringWithInt:R_string_ssid_set_message] andTitle:nil andDelegate:self];
        }
    } else {
        [WCUi showDialogWithMessage: [S getStringWithInt:R_string_ssid_reset_message] andTitle:nil andDelegate:self];
    }
}

/** Send flash command (dialog delegate) */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(!buttonIndex) return;
    
//    if([_ssidText isEnabled]) {
//        [[S connection] sendConfigurationPacketWithByteArray:
//         [[S globals] generateSSIDPacketWithNSString:[_ssidText text] withNSString: [_passwordText text] ]];
//        [self.delegate popupDidFinish:self];
//    } else {
//        [[S connection] sendConfigurationPacketWithByteArray:
//         [[S globals] generateSSIDResetPacket]];
//        [self.delegate popupDidFinish:self];
//    }
    
}

/** Enable or dissable entry text fields */
- (IBAction)defaultSwitch:(id)sender {
    UISwitch *sw = (UISwitch *)sender;
    [_ssidText setEnabled: ! [sw isOn]];
    [_passwordText setEnabled: ! [sw isOn]];
    [_passwordConfirmText setEnabled: ! [sw isOn]];
}

/** Validate the entered ssid Name */
- (IBAction)ssidTextEditingChanged:(id)sender {
    NSString *st = [[S globals] generateSSIDValidWithNSString:[_ssidText text]];
    if ( ! [st isEqualToString: [_ssidText text] ] ) {
        [_ssidText setText:st];
    }
}
@end


