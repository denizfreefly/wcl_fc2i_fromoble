#import <UIKit/UIKit.h>


@class WCAutotunePopup;

@interface WCAutotunePopup : WCPopup <UITableViewDelegate>

@property (weak, nonatomic) id <WCPopupDelegate> delegate;
//@property (weak) IBOutlet WCTableView *filesUITable;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *startButtonOutlet1;
- (IBAction)doneButton:(id)sender;
- (IBAction)startButton:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *textLabel;

@end
