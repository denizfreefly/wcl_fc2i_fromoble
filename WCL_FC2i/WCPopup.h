
@class WCPopup;

@protocol WCPopupDelegate
- (void)popupDidFinish:(UIViewController *)controller;
@end

@interface WCPopup : UIViewController

@property (weak, nonatomic) id <WCPopupDelegate> delegate;

@end
