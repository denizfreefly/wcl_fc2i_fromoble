//
//  WCChartViewScale.m
//  WCL_FC2i
//
//  Created by Lorne Kelly on 13-08-15.
//  Copyright (c) 2014 Lorne Kelly. All rights reserved.
//



@implementation WCChartViewScale

float textHeight;

/** Override NSView - draw the scale to the left of the charts */
- (void)drawRect:(CGRect)r
{
    textHeight = [WCUi smallScreen] ? 13 : 15;
    
    [[S charts] setScalingWithFloat:r.size.height withFloat: r.size.width];
    
    // Get raw listis for upper and lower charts
    IOSObjectArray * tsUpper = [[S charts] getTraceListSplitUpper];
    IOSObjectArray * tsLower = [[S charts] getTraceListSplitLower];
    
    // Display a single chart scale
    if (tsLower == nil) {
        [self drawValsBottom: r withList: [[S charts] getChartSetListWithTraceStructureArray: tsUpper]];
        [self drawValsTop: r withList: [[S charts] getChartSetListWithTraceStructureArray: tsUpper]];
    }
    
    // Display a split chart scale
    else {
        [self drawValsBottom: r withList: [[S charts] getChartSetListWithTraceStructureArray: tsUpper]];
        [self drawValsTop: r withList: [[S charts] getChartSetListWithTraceStructureArray: tsUpper]];
        [self drawValsBottom: r withList: [[S charts] getChartSetListWithTraceStructureArray: tsLower]];
        [self drawValsTop: r withList: [[S charts] getChartSetListWithTraceStructureArray: tsLower]];
    }
}

/** Private - Draw upper chart scale values.  Colapse list if all traces have the same upper value */
- (void) drawValsTop: (CGRect) r withList: (IOSObjectArray*) tsList {
    
    if (tsList == nil) return;
    
    TraceStructure *ts = (TraceStructure *) [tsList objectAtIndex:0];
    float yStart = ts->chartTop_;
    float yCursor = yStart + textHeight;
    UIColor *txtColor;
    
    for (int i = 0; i < [tsList count]; i++) {
        ts = (TraceStructure *) [tsList objectAtIndex:i];
        if ([tsList count] == 1)
            txtColor = [UIColor blackColor];
        else
            txtColor = [WCUi getColorForTrace:ts];
        [self drawTextRight: r message: [ts getChartLabelMax] X: r.size.width -12 Y: yCursor Color:txtColor];
        yCursor += textHeight;
    }
    
    yCursor -= textHeight;
    [[UIColor blackColor] setStroke];
    [WCUi lineOnRect: r X1: r.size.width -5  Y1: yStart X2: r.size.width -2 Y2: yStart ];
    [WCUi lineOnRect: r X1: r.size.width -5  Y1: yStart X2: r.size.width -5 Y2: yCursor ];
    [WCUi lineOnRect: r X1: r.size.width -8  Y1: yCursor X2: r.size.width -5 Y2: yCursor ];
}

/** Private - Draw lower chart scale values.  Colapse list if all traces have the same lower value */
- (void) drawValsBottom: (CGRect) r withList: (IOSObjectArray*) tsList {
    
    if (tsList == nil) return;
    
    TraceStructure *ts = (TraceStructure *) [tsList objectAtIndex:0];
    float yStart = ts->chartBottom_;
    float yCursor = yStart;
    UIColor *txtColor;
    
    for (int i = 0; i < [tsList count]; i++) {
        TraceStructure *ts = (TraceStructure *) [tsList objectAtIndex:i];
        if ([tsList count] == 1)
            txtColor = [UIColor blackColor];
        else
            txtColor = [WCUi getColorForTrace:ts];
        [self drawTextRight: r message: [ts getChartLableMin] X: r.size.width -12 Y: yCursor Color:txtColor];
        yCursor -= textHeight;
    }
    
    [[UIColor blackColor] setStroke];
    [WCUi lineOnRect: r X1: r.size.width -5  Y1: yStart X2: r.size.width -2 Y2: yStart ];
    [WCUi lineOnRect: r X1: r.size.width -5  Y1: yStart X2: r.size.width -5 Y2: yCursor ];
    [WCUi lineOnRect: r X1: r.size.width -8  Y1: yCursor X2: r.size.width -5 Y2: yCursor ];
}

/** Private - Draw right aligned text with the color and cooridnates specified */
- (void) drawTextRight: (CGRect) r message: (NSString*) str X: (float) x Y: (float) y Color: (UIColor*) uiColor {

    CGRect strFrame = { { 0, y - textHeight }, { x, y  } };
    
    NSMutableParagraphStyle *textStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    textStyle.alignment = NSTextAlignmentRight;
    UIFont *textFont = [UIFont systemFontOfSize:textHeight];
    [str drawInRect:strFrame withAttributes:@{NSFontAttributeName:textFont, NSParagraphStyleAttributeName:textStyle, NSForegroundColorAttributeName: uiColor }];
    
    
}


@end
