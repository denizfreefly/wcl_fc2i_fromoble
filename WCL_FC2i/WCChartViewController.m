//
//  WCConfigView.m
//  WCL_FC2i
//
//  Created by Lorne Kelly on 13-08-15.
//  Copyright (c) 2014 Lorne Kelly. All rights reserved.
//
#include "Charts.h"

@implementation WCChartViewController

- (void) lateInit
{
    
    // Add text to labels
    [_streamLabel setText: [S getStringWithInt:R_string_chart_button_stream]];
    
    [_resetLabel setText: [S getStringWithInt:R_string_chart_button_reset]];
    [_visibleLabel setText: [S getStringWithInt:R_string_chart_button_visible]];
    
    // Configure tables
    [_chartsetTable setDelegate: self];
    [_chartsetTable setDataSource:_chartsetTable];
    if (![WCUi smallScreen]) _chartsetTable.tableTitle = NSLocalizedStringFromTable (@"chart_set", @"R", nil);
    
    [_traceTable setDelegate: self];
    [_traceTable setDataSource:_traceTable];
    if (![WCUi smallScreen]) _traceTable.tableTitle = NSLocalizedStringFromTable (@"chart_trace", @"R", nil);
    
    // Add pinch and zoom gesture recognizers
    _chartView.userInteractionEnabled = true ;
    _chartView.multipleTouchEnabled = true;
    
    UIPanGestureRecognizer * recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    recognizer.delegate = self;
    [_chartView addGestureRecognizer:recognizer];
    
    UIPinchGestureRecognizer * recogniz = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    recogniz.delegate = self;
    [_chartView addGestureRecognizer:recogniz];
    
    // Set up stream rate button
    [self setStreamSelectionWithInt: 6];
    [_streamRateButton setBackgroundColor:[WCUi appGrey]];
    
    if ([WCUi smallScreen]) [_ssDoneButtonOutlet setBackgroundColor:[WCUi appGrey]];
    
    // Set selections (populates tables)
    [self tableView:nil didSelectRowAtIndexPath:nil];
    
    // Register for events
    [S registerUiCallbackWithS_ChartUiCallBack: self];

}

#if 0
#pragma mark -
#pragma mark Table delegate stuff
#endif

/** Interface UITableViewDelegate - UI call when user selects a cell */
- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath {
    
    // If notification is nil this is an initialization (Firmware may have changed)
    if (tableView == nil) {
        _chartsetTable.tableData = (IOSObjectArray *) [[S charts] getChartSetNames];
        [_chartsetTable reloadData];
        [_chartsetTable selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:0];
    }
    
    // If call from chart set table, or a refresh request
    if (tableView == _chartsetTable || tableView == nil) {
        
        // update S charts data
        NSIndexPath *selectedIndexPath = [_chartsetTable indexPathForSelectedRow];
        [[S charts] setSelectedChartSetWithInt:(int)selectedIndexPath.row];
        
        // populate the trace list
        int setSize = [[S charts] getTraceListLength];
        //TODO
//        IOSObjectArray *newList = [IOSObjectArray arrayWithLength:setSize type:[IOSClass classWithClass:[NSString class]]];
//        IOSObjectArray *newColor = [IOSObjectArray arrayWithLength:setSize type:[IOSClass classWithClass:[UIColor class]]];
//        for (int i = 0; i < (int) [newList count]; i++) {
//            TraceStructure *ts = [[S charts] getTraceAtIndexWithInt:i];
//            [newList replaceObjectAtIndex:i withObject: [ts getLabel] ];
//            [newColor replaceObjectAtIndex:i withObject: [WCUi getColorForTrace: ts] ];
//        }
//        _traceTable.tableData = newList;
//        _traceTable.colorData = newColor;
        [_traceTable reloadData];
        
        // start streaming
        [self startStreaming];
        
    }
    
    [self setTrace ];
}


#if 0
#pragma mark -
#pragma mark Private selectors
#endif

/** Private - set the specified trace or -1 for no selection
    Update visibility switch */
- (void)setTrace {
    
    NSIndexPath *selectedIndexPath = [_traceTable indexPathForSelectedRow];
    int selRow = (selectedIndexPath) ? (int)selectedIndexPath.row: -1;
    [[S charts] setSelectedTraceWithInt: selRow];
    TraceStructure *ts = [[S charts] getSelectedTrace];
    
    if (ts == nil) {
        [_visibleSwitch setEnabled: false];
        [_visibleSwitch setOn:false];
        NSLog(@"setEnabled false");
    } else {
        [_visibleSwitch setEnabled:true];
        [_visibleSwitch setOn: ts->isEnabled_];
        NSLog(@"setEnabled true");
    }
}

/** Private - Sets the displayed stream rate and records selection in button tag */
- (void)setStreamSelectionWithInt:(int) sel {
    NSString *srFormat = [S getStringWithInt:R_string_chart_text_stream_rate];
    srFormat = [srFormat replaceAll:@"%s" withReplacement:@"%@"];//Java vs C
    NSString *comboItem = [NSString stringWithFormat: srFormat, IOSObjectArray_Get(Charts_get_STREAM_OPTIONS_(), sel) ];
    [_streamRateButton setTitle:comboItem forState:UIControlStateNormal];
    [_streamRateButton setTag: sel];
    
    NSLog(@"rate is %@" , comboItem);
    
}


/** Private - configure to start streaming */
- (void)startStreaming {
    
    // Ensure chart set slection is up to date
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[[S charts] getSelectedChartSetIndex ] inSection:0];
    [_chartsetTable selectRowAtIndexPath:indexPath animated:false scrollPosition:UITableViewScrollPositionNone];
    
    // Ensure chart speed is correct
    long sel = [_streamRateButton tag];
    //TODO
//    [S charts]->secondsPerDivision_ = IOSObjectArray_Get(Charts_get_STREAM_OPTIONS_(), sel);
    
    
    
    // Clear trace selection
    [[S charts] setSelectedTraceWithInt: -1 ];
    NSIndexPath *selectedIndexPath = [_traceTable indexPathForSelectedRow];
    [_traceTable deselectRowAtIndexPath:  selectedIndexPath animated:NO];
    
    // Ensure everything gets re-drawn
    [_chartView setNeedsDisplay];
    [_chartScale setNeedsDisplay];
    
    // Clear previous stream and start new stream
    [S globals]->streaming_ = false;
    [[S comms] setupStream];
    [S globals]->streaming_ = true;
    [[S comms] setupStream];
    
    [_streamSwitch setSelected:false ];
}

#if 0
#pragma mark -
#pragma mark Interface S_ChartUiCallBack implementation
#endif

/** Callback - New data available to chart */
- (void)onAddValues {
    
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread: @selector(onAddValues) withObject: nil waitUntilDone:true  ];
        return;
    }
    
    [_chartView setNeedsDisplay];
}

/** Not used in this version */
- (void)onUpdateSpektrum {
}

/** Not used in this version */
- (void)onUpdateGPS {
}

#if 0
#pragma mark -
#pragma mark Respond to UI controls
#endif

/** Button - Streaming on or off  */
- (IBAction)streamSwitchAction:(id)sender {
    if ([_streamSwitch isOn]) {
        [self startStreaming];
    } else {
        [S globals]->streaming_ = false;
        [[S comms] setupStream ];
    }
}

/** Button - Restore traces to default */
- (IBAction)resetSwitchAction:(id)sender {
    [[S charts] resetTraces];
    [self startStreaming];
    [_chartScale setNeedsDisplay];
    [_chartView setNeedsDisplay];
    
    [self performSelector:@selector(resetSwitchSpringback) withObject:nil afterDelay: 0.2 ];
}
- (void) resetSwitchSpringback {
    [_resetSwitch setOn:false animated:true];
}


/** Button - Trace visibility */
- (IBAction)visibleSwitchAction:(id)sender {
    TraceStructure *ts = [[S charts] getSelectedTrace];
    if (ts != nil) ts->isEnabled_ = [_visibleSwitch isOn];
    [_chartView setNeedsDisplay];
    [_chartScale setNeedsDisplay];
}

/** Button - Set stream speed
 * WARNING: Selections are in order
 */
- (IBAction)streamRateButtonAction:(id)sender {
    
    NSLog(@"streamRateButtonAction");
    
    NSString *srFormat = [S getStringWithInt:R_string_chart_text_stream_rate];
    srFormat = [srFormat replaceAll:@"%s" withReplacement:@"%@"];//Java vs C
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:[S getStringWithInt:R_string_chart_button_stream]
                                  delegate:self
                                  cancelButtonTitle:nil
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:[NSString stringWithFormat: srFormat, IOSObjectArray_Get(Charts_get_STREAM_OPTIONS_(), 1) ],
                                  [NSString stringWithFormat: srFormat, IOSObjectArray_Get( Charts_get_STREAM_OPTIONS_(), 2 ) ],
                                  [NSString stringWithFormat: srFormat, IOSObjectArray_Get(nil_chk(Charts_get_STREAM_OPTIONS_()), 3) ],
                                  [NSString stringWithFormat: srFormat, IOSObjectArray_Get(nil_chk(Charts_get_STREAM_OPTIONS_()), 4)],
                                  [NSString stringWithFormat: srFormat, IOSObjectArray_Get(nil_chk(Charts_get_STREAM_OPTIONS_()), 5) ],
                                  [NSString stringWithFormat: srFormat, IOSObjectArray_Get(nil_chk(Charts_get_STREAM_OPTIONS_()), 6) ],
                                  [NSString stringWithFormat: srFormat, IOSObjectArray_Get(nil_chk(Charts_get_STREAM_OPTIONS_()), 7) ],
                                  [NSString stringWithFormat: srFormat, IOSObjectArray_Get(nil_chk(Charts_get_STREAM_OPTIONS_()), 8) ],
                                  [NSString stringWithFormat: srFormat, IOSObjectArray_Get(nil_chk(Charts_get_STREAM_OPTIONS_()), 9) ],
                                  nil];

    [actionSheet showInView:self.view];
    
//    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

/** Interface UIActionSheetDelegate - Respond to actions sheet selection (send Flash command to unit)
 WARNING: Selections are in order
 */
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == -1) return;
    
    [self setStreamSelectionWithInt: buttonIndex +1];
    
    [_streamSwitch setTag: buttonIndex ];
    //TODO
//    [S charts]->secondsPerDivision_ = IOSObjectArray_Get(nil_chk(Charts_get_STREAM_OPTIONS_()), buttonIndex);
    [[S comms] setupStream];
}

#if 0
#pragma mark -
#pragma mark Respond to gestures
#endif

/** UIGestureRecognizerDelegate - set trace scale */
- (IBAction)handlePinch:(UIPinchGestureRecognizer *)recognizer {
    
    static float scale = 0;
    TraceStructure *ts = [[S charts] getSelectedTrace];
    if (ts != nil) {
        [_chartScale setNeedsDisplay];
        [_chartView setNeedsDisplay];
        
        if(recognizer.state != UIGestureRecognizerStateBegan)
            [ts adjustTraceScaleWithFloat: (recognizer.scale - scale) * 100 ];
        
        scale = recognizer.scale;
    }
}

/** UIGestureRecognizerDelegate - set trace pan */
- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer {
    
    static float offset = 0;
    TraceStructure *ts = [[S charts] getSelectedTrace];
    if (ts != nil) {
        [_chartScale setNeedsDisplay];
        [_chartView setNeedsDisplay];
        
        CGPoint translation = [recognizer translationInView:self.view];
        if(recognizer.state != UIGestureRecognizerStateBegan)
            [ts adjustTraceOffsetWithFloat: (translation.y - offset) * -1 ];
        offset = translation.y;
    }
}

/** Small screen done button hides additional controls */
- (IBAction)ssDoneButton:(id)sender {
    [UIView
     animateWithDuration:0.5
     delay:0.0
     options:UIViewAnimationOptionAllowUserInteraction
     animations:^{ _ssControlsView.alpha = 0.0; }
     completion:nil];
}

/** Small screen gear button shows additional controls */
- (IBAction)ssGearButton:(id)sender {
    [UIView
     animateWithDuration:0.5
     delay:0.0
     options:UIViewAnimationOptionAllowUserInteraction
     animations:^{ _ssControlsView.alpha = 1.0; }
     completion:nil];
}
@end
