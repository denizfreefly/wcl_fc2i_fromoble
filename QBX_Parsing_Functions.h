/*-----------------------------------------------------------------
    Copyright by Freefly Systems Inc 2015
		
    Contains proprietary algorithms and source code
    Copyright by SM Webb and JG Ellison 2010-2012
		
    Filename: "QBX_Parsing_Functions.h"
-----------------------------------------------------------------*/

#ifndef QBX_PARSING_FUNCTIONS_H
#define QBX_PARSING_FUNCTIONS_H

//****************************************************************************
// Headers
//****************************************************************************
#include <stdlib.h>		// for Standard Data Types
#include <stdint.h>		// for Standard Data Types

//****************************************************************************
// Defines
//****************************************************************************
// Parser Direction Defines
#define READ				0
#define WRITEDELTA	1 
#define WRITEABS		2

//****************************************************************************
// Defines
//****************************************************************************
extern uint8_t rw;

//****************************************************************************
// Public Function Prototypes
//****************************************************************************
void QBX_Parser_SetMessagePointer(uint8_t *p);
uint8_t *QBX_Parser_GetMessagePointer(void);
void QBX_Parser_SetDir_Read(void);
void QBX_Parser_SetDir_WriteRel(void);
void QBX_Parser_SetDir_WriteAbs(void);

//****************************************************************************
// Private Function Prototypes
//****************************************************************************
void AddFloatAsSignedLong(float *v, uint32_t n, float scaleto);
void AddFloatAsSignedShort(float *v, uint32_t n, float scaleto);
void AddFloatAsSignedChar(float *v, uint32_t n, float scaleto);
void AddFloatAsUnsignedChar(float *v, uint32_t n, float scaleto);
void GetFloatAsSignedLong(float *v, uint32_t n, float max, float min, float scalefrom);
void GetFloatAsSignedShort(float *v, uint32_t n, float max, float min, float scalefrom);
void GetFloatAsSignedChar(float *v, uint32_t n, float max, float min, float scalefrom);
void GetFloatAsUnsignedChar(float *v, uint32_t n, float max, float min, float scalefrom);
void AddSignedLongAsSignedLong(int32_t *v, uint32_t n);
void AddSignedLongAsSignedShort(int32_t *v, uint32_t n);
void AddSignedLongAsSignedChar(int32_t *v, uint32_t n);
void AddSignedLongAsUnsignedChar(int32_t *v, uint32_t n);
void GetSignedLongAsSignedLong(int32_t *v, uint32_t n, int32_t max, int32_t min);
void GetSignedLongAsSignedShort(int32_t *v, uint32_t n, int32_t max, int32_t min);
void GetSignedLongAsSignedChar(int32_t *v, uint32_t n, int32_t max, int32_t min);
void GetSignedLongAsUnsignedChar(int32_t *v, uint32_t n, int32_t max, int32_t min);
void AddSignedShortAsSignedShort(int16_t *v, uint32_t n);
void AddSignedShortAsSignedChar(int16_t *v, uint32_t n);
void AddSignedShortAsUnsignedChar(int16_t *v, uint32_t n);
void GetSignedShortAsSignedShort(int16_t *v, uint32_t n, float max, float min);
void GetSignedShortAsSignedChar(int16_t *v, uint32_t n, float max, float min);
void GetSignedShortAsUnsignedChar(int16_t *v, uint32_t n, int16_t max, int16_t min);
void AddSignedCharAsSignedChar(int8_t *v, uint32_t n);
void GetSignedCharAsSignedChar(int8_t *v, uint32_t n, int8_t max, int8_t min);
void AddUnsignedCharAsUnsignedChar(uint8_t *v, uint32_t n);
void GetUnsignedCharAsUnsignedChar(uint8_t *v, uint32_t n, uint8_t max, uint8_t min);


//****************************************************************************
// Public Function Like Macros for 
//****************************************************************************

#define PARSE_FL_AS_SL(value, len, max, min, scale)\
    if(rw == READ) { AddFloatAsSignedLong(value, len, scale); } else { GetFloatAsSignedLong(value, len, max, min, 1.0f / scale); }

#define PARSE_FL_AS_SS(value, len, max, min, scale)\
    if(rw == READ) { AddFloatAsSignedShort(value, len, scale); } else { GetFloatAsSignedShort(value, len, max, min, 1.0f / scale); }

#define PARSE_FL_AS_SC(value, len, max, min, scale)\
    if(rw == READ) { AddFloatAsSignedChar(value, len, scale); } else { GetFloatAsSignedChar(value, len, max, min, 1.0f / scale); }

#define PARSE_FL_AS_UC(value, len, max, min, scale)\
    if(rw == READ) { AddFloatAsUnsignedChar(value, len, scale); } else { GetFloatAsUnsignedChar(value, len, max, min, 1.0f / scale); }

#define PARSE_SL_AS_SL(value, len, max, min)\
    if(rw == READ) { AddSignedLongAsSignedLong((int32_t *)value, len); } else { GetSignedLongAsSignedLong((int32_t *)value, len, max, min); }

#define PARSE_SL_AS_SS(value, len, max, min)\
    if(rw == READ) { AddSignedLongAsSignedShort((int32_t *)value, len); } else { GetSignedLongAsSignedShort((int32_t *)value, len, max, min); }

#define PARSE_SL_AS_SC(value, len, max, min)\
    if(rw == READ) { AddSignedLongAsSignedChar((int32_t *)value, len); } else { GetSignedLongAsSignedChar((int32_t *)value, len, max, min); }

#define PARSE_SL_AS_UC(value, len, max, min)\
    if(rw == READ) { AddSignedLongAsUnsignedChar((int32_t *)value, len); } else { GetSignedLongAsUnsignedChar((int32_t *)value, len, max, min); }

#define PARSE_SS_AS_SS(value, len, max, min)\
    if(rw == READ) { AddSignedShortAsSignedShort((int16_t *)value, len); } else { GetSignedShortAsSignedShort((int16_t *)value, len, max, min); }

#define PARSE_SS_AS_SC(value, len, max, min)\
    if(rw == READ) { AddSignedShortAsSignedChar((int16_t *)value, len); } else { GetSignedShortAsSignedChar((int16_t *)value, len, max, min); }

#define PARSE_SS_AS_UC(value, len, max, min)\
    if(rw == READ) { AddSignedShortAsUnsignedChar((int16_t *)value, len); } else { GetSignedShortAsUnsignedChar((int16_t *)value, len, max, min); }

#define PARSE_SC_AS_SC(value, len, max, min)\
    if(rw == READ) { AddSignedCharAsSignedChar(value, len); } else { GetSignedCharAsSignedChar(value, len, max, min); }

#define PARSE_UC_AS_UC(value, len, max, min)\
    if(rw == READ) { AddUnsignedCharAsUnsignedChar(value, len); } else { GetUnsignedCharAsUnsignedChar(value, len, max, min); }


#endif